FCKConfig.ToolbarSets["HAN-Toolbar"] = [
['Source','-','NewPage','Preview'],
['Cut','Copy','Paste','PasteText','PasteWord'],
['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
'/',
['Bold','Italic','Underline','StrikeThrough','-','Subscript','Superscript'],
['OrderedList','UnorderedList','-','Outdent','Indent','Blockquote'],
['JustifyLeft','JustifyCenter','JustifyRight','JustifyFull'],
['Link','Unlink','Anchor'],
['Image','Flash','Table','Rule','SpecialChar','PageBreak'],
'/',
['Style','FontFormat','FontName','FontSize'],
['TextColor','BGColor'],
['FitWindow','ShowBlocks','-','About'] // No comma for the last row.
] ;

FCKConfig.ToolbarSets["HANBasic"] = [
['Bold','Italic','Underline','-','OrderedList','UnorderedList','-','Subscript','Superscript','-','Link','Unlink'],
'/',
['Paste','PasteText','PasteWord','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyFull','Table','SpecialChar','SpellCheck'],
'/',
['FitWindow','-','FontName','FontSize','TextColor']
] ;
