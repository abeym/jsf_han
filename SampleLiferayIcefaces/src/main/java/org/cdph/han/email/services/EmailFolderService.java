package org.cdph.han.email.services;

import javax.mail.MessagingException;

import org.cdph.han.email.dto.FolderDetailTO;

/**
 * This interface defines the common operations to be performed over email folders
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface EmailFolderService {

	/**
	 * The implementation of this method must create a new folder in the EMail server
	 * @param userId of the user owning the new folder
	 * @param folderName the name for the new folder
	 * @return the outcome of the folder creation
	 */
	EmailOperationOutcome createFolder (final long userId, final String folderName);

	/**
	 * The implementation of this method must delete a folder if empty, or return an error if not empty
	 * @param userId of the owning user
	 * @param folderName the name of the folder to delete
	 * @return the outcome of the folder removal
	 */
	EmailOperationOutcome deleteFolder (final long userId, final String folderName);

	/**
	 * The implementation of this method must rename a folder
	 * @param userId of the user owning the folder
	 * @param folderName the name of the folder to be renamed
	 * @param newFolderName the new name of the folder
	 * @return the outcome of the folder rename
	 */
	EmailOperationOutcome renameFolder (final long userId, final String folderName, final String newFolderName);

	/**
	 * The implementation of this method must return all the folders available for the current user
	 * @param userId of the user to retrieve the folder names
	 * @return list of names
	 */
	FolderDetailTO[] listFolders (final long userId);

	/**
	 * The implementation of this method must empty the trash folder of the provided user
	 * @param userId of the user owning the trash folder
	 */
	void emptyTrash (final long userId) throws MessagingException;

}
