package org.cdph.han.email.component;

import java.util.Comparator;

import org.cdph.han.email.dto.EmailSummaryTO;

/**
 * This comparator will allow sorting a collection of EmailSummaryTO objects by the given column
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EmailSummaryComparator implements Comparator<EmailSummaryTO> {
	/** The column to use in sorting */
	private EmailSummaryTO.Column sortColumn;

	/**
	 * Constructor to set the column to sort by
	 * @param sortColumn to use in sorting
	 */
	public EmailSummaryComparator (final EmailSummaryTO.Column sortColumn) {
		this.sortColumn = sortColumn;
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare (final EmailSummaryTO summary1, final EmailSummaryTO summary2) {
		int compare = 0;
		if (EmailSummaryTO.Column.ATTACHMENTS.equals (sortColumn)) {
			if (summary1.isAttachments ()) {
				compare = summary2.isAttachments () ? 0 : 1;
			} else {
				compare = summary2.isAttachments () ? -1 : 0;
			}
		} else if (EmailSummaryTO.Column.DATE.equals (sortColumn)) {
			if (summary1.getDate () != null && summary2.getDate () != null) {
				compare = summary1.getDate ().compareTo (summary2.getDate ());
			} else if (summary1.getDate () != null) {
				compare = 1;
			} else if (summary2.getDate () != null) {
				compare = -1;
			} else {
				compare = 0;
			}
		} else if (EmailSummaryTO.Column.FROM.equals (sortColumn)) {
			compare = summary1.getFrom ().getName ().compareTo (summary2.getFrom ().getName ());
		} else if (EmailSummaryTO.Column.NUMBER.equals (sortColumn)) {
			compare = summary1.getNumber () - summary2.getNumber ();
		} else if (EmailSummaryTO.Column.SIZE.equals (sortColumn)) {
			compare = summary1.getSize () - summary2.getSize ();
		} else if (EmailSummaryTO.Column.SUBJECT.equals (sortColumn)) {
			compare = summary1.getSubject ().compareTo (summary2.getSubject ());
		}
		return compare;
	}

}
