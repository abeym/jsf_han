package org.cdph.han.email.services;

import java.util.List;

import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.email.dto.EmailAddressTO;

/**
 * This interface defines the services available to retrieve the users id from a personal group collection
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface EmailUsersService {

	/**
	 * The implementation of this method must return all the users in the given filter
	 * @param filter containing the groups, users and other selections
	 * @return the users in the given filter
	 */
	long[] getUserIds (final List<RecipientTO> filter);

	/**
	 * The implementation of this method must return a translated list of recipient transfer objects from the given
	 * addresses
	 * @param addresses to translate
	 * @return recipient list
	 */
	List<RecipientTO> getRecipients (final EmailAddressTO[] addresses);

}
