package org.cdph.han.email.dto;

/**
 * Transfer object for email addresses
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EmailAddressTO {
	/** Name of the sender or recipient */
	public String name;
	/** Email address of the sender or recipient */
	public String address;

	/**
	 * Constructor of the transfer object
	 * @param name of the sender/recipient
	 * @param address of the sender/recipient
	 */
	public EmailAddressTO (final String name, final String address) {
		this.name = name;
		this.address = address;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString () {
		return name;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @return the address
	 */
	public String getAddress () {
		return address;
	}

}
