package org.cdph.han.email.services;

import javax.mail.MessagingException;

import org.cdph.han.email.dto.QuotaTO;

/**
 * Service that retrieves the available quota for the current user
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface QuotaService {

	/**
	 * Retrieves the quota for the given user
	 * @param userId to retrive quota
	 * @return the available quota for the current user
	 * @throws MessagingException in case of errors
	 */
	QuotaTO getUserQuota (final long userId) throws MessagingException;

}
