package org.cdph.han.email.services;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.Transport;
import javax.mail.Flags.Flag;
import javax.mail.Message.RecipientType;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.services.HanPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Properties;

/**
 * Implementation of the service to send emails
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Service ("emailSenderService")
public class EmailSenderServiceImpl implements EmailSenderService {
	/** Class log */
	private static final Log log = LogFactory.getLog (EmailSenderServiceImpl.class);
	/** Property key for the admin emails from address */
	private static final String HAN_EMAIL_FROM_ADDRESS_KEY = "HAN_EMAIL_FROM_ADDRESS";
	/** Property key for the admin emails sender name */
	private static final String HAN_EMAIL_SENDER_NAME_KEY = "HAN_EMAIL_SENDER_NAME";
	/** Property key for the admin email user name */
	private static final String HAN_EMAIL_USER_NAME_KEY = "HAN_EMAIL_USER_NAME";
	/** Property key for the admin email password */
	private static final String HAN_EMAIL_PASSWORD_KEY = "HAN_EMAIL_PASSWORD";
	/** Domain */
	private static final String HAN_EMAIL_DOMAIN_KEY = "HAN_EMAIL_DOMAIN";
	/** Email properties */
	private final Properties props = new Properties ();
	/** Service used to retrieve han properties */
	//abey...
	//@Autowired
	private HanPropertiesService hanPropertiesService;
	/** Service to retrieve user information */
	//abey...
	//@Autowired
	private UserDataService userDataService;

	/**
	 * Default constructor
	 */
	public EmailSenderServiceImpl () {
		try {
			props.load (this.getClass ().getResourceAsStream ("email.properties"));
		} catch (Exception ex) {
			log.fatal ("Error loading email properties.", ex);
		}
	}

	/**
	 * Create a set of addresses with the given string array
	 * @param addresses to create
	 * @return addresses created
	 * @throws AddressException in case an address is not in the correct format
	 * @throws UnsupportedEncodingException in case of using an unsupported encoding in the character set
	 */
	private Address[] createAddresses (final long[] userIds) throws AddressException, UnsupportedEncodingException {
		Address[] inetAddresses = new Address[userIds.length];
		UserData userData;
		for (int i = 0; i < userIds.length; i++) {
			userData = userDataService.getUser (userIds[i]);
			inetAddresses[i] = new InternetAddress (userData.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY),
					userData.getContact ().getFirstName () + " " + userData.getContact ().getLastName ());
		}
		return inetAddresses;
	}

	/**
	 * Creates a set of addresses using the email provided by the user
	 * @param addresses to create
	 * @return addresses created
	 * @throws AddressException in case an address is not in the correct format
	 * @throws UnsupportedEncodingException in case of using an unsupported encoding in the character set
	 */
	private Address[] createExternalAddresses (final long[] userIds) throws AddressException,
	UnsupportedEncodingException {
		Address[] inetAddresses = new Address[userIds.length];
		UserData userData;
		for (int i = 0; i < userIds.length; i++) {
			userData = userDataService.getUser (userIds[i]);
			inetAddresses[i] = new InternetAddress (userData.getEmail (), userData.getContact ().getFirstName ()
					+ " " + userData.getContact ().getLastName ());
		}
		return inetAddresses;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailSenderService#sendAdminEmail(long[], long[], java.lang.String, java.lang.String)
	 */
	@Override
	public void sendAdminEmail (final long[] to, final long[] cc, final String message, final String subject,
			final List<FileTO> attachments) {
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				hanPropertiesService.getValue (HAN_EMAIL_USER_NAME_KEY),
				hanPropertiesService.getValue (HAN_EMAIL_PASSWORD_KEY)));
		MimeMessage mimeMessage = new MimeMessage (session);
		try {
			if (attachments == null || attachments.isEmpty ()) {
				mimeMessage.setContent (message, "text/html");
			} else {
				Multipart multipart = new MimeMultipart ();
				BodyPart bodyPart = new MimeBodyPart ();
				bodyPart.setContent (message, "text/html");
				multipart.addBodyPart (bodyPart);
				DataSource dataSource;
				for (FileTO attachment : attachments) {
					dataSource = new FileDataSource (attachment.getFile ());
					bodyPart = new MimeBodyPart ();
					bodyPart.setDataHandler (new DataHandler (dataSource));
					bodyPart.setFileName (attachment.getFileInfo().getFileName ());
					multipart.addBodyPart (bodyPart);
				}
				mimeMessage.setContent (multipart);
			}
			mimeMessage.setSubject (subject);
			mimeMessage.setRecipients (RecipientType.TO, createExternalAddresses (to));
			if (cc != null && cc.length > 0) {
				mimeMessage.setRecipients (RecipientType.CC, createExternalAddresses (cc));
			}
			mimeMessage.setFrom (new InternetAddress (hanPropertiesService.getValue (HAN_EMAIL_FROM_ADDRESS_KEY),
					hanPropertiesService.getValue (HAN_EMAIL_SENDER_NAME_KEY)));
			final Transport transport = session.getTransport ();
			transport.connect ();
			mimeMessage.saveChanges ();
			transport.sendMessage (mimeMessage, mimeMessage.getAllRecipients ());
			mimeMessage.saveChanges ();
			transport.close ();
		} catch (AddressException ex) {
			log.error ("Error creating addresses", ex);
		} catch (MessagingException ex) {
			log.error ("Error creating email message.", ex);
		} catch (UnsupportedEncodingException ex) {
			log.error ("Unsupported encoding found while creating addresses.", ex);
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailSenderService#sendEmail(long, long[], long[], java.lang.String, java.lang.String)
	 */
	@Override
	public int sendEmail (final long from, final long[] to, final long[] cc, final String message,
			final String subject, final List<FileTO> attachments, final boolean saveSent) {
		int messageSize = 0;
		final UserData userData = userDataService.getUser (from);
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				userData.getScreenName (), userData.getEmailPassword ()));
		final MimeMessage mimeMessage = new MimeMessage (session);
		try {
			if (attachments == null || attachments.isEmpty ()) {
				mimeMessage.setContent (message, "text/html");
			} else {
				Multipart multipart = new MimeMultipart ();
				BodyPart bodyPart = new MimeBodyPart ();
				bodyPart.setContent (message, "text/html");
				multipart.addBodyPart (bodyPart);
				DataSource dataSource;
				for (FileTO attachment : attachments) {
					dataSource = new FileDataSource (attachment.getFile ());
					bodyPart = new MimeBodyPart ();
					bodyPart.setDataHandler (new DataHandler (dataSource));
					bodyPart.setFileName (attachment.getFileInfo ().getFileName ());
					multipart.addBodyPart (bodyPart);
				}
				mimeMessage.setContent (multipart);
			}
			mimeMessage.setSubject (subject);
			mimeMessage.setRecipients (RecipientType.TO, createAddresses (to));
			if (cc != null && cc.length > 0) {
				mimeMessage.setRecipients (RecipientType.CC, createAddresses (cc));
			}
			mimeMessage.setFrom (new InternetAddress (userData.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY),
					userData.getContact ().getFirstName () + " " + userData.getContact ().getLastName ()));
			final Transport transport = session.getTransport ();
			transport.connect ();
			mimeMessage.setSentDate (new Date ());
			transport.sendMessage (mimeMessage, mimeMessage.getAllRecipients ());
			transport.close ();
			if (saveSent) {
				final Store store = session.getStore ("imap");
				store.connect ();
				mimeMessage.setFlag (Flag.SEEN, true);
				mimeMessage.setSentDate (new Date ());
				final Folder sentFolder = store.getDefaultFolder ().getFolder ("INBOX").getFolder ("Sent");
				sentFolder.open (Folder.READ_WRITE);
				sentFolder.appendMessages (new Message[]{mimeMessage});
				sentFolder.close (false);
			}
			messageSize = mimeMessage.getSize ();
		} catch (AddressException ex) {
			log.error ("Error creating addresses", ex);
		} catch (MessagingException ex) {
			log.error ("Error creating email message.", ex);
		} catch (UnsupportedEncodingException ex) {
			log.error ("Unsupported encoding found while creating addresses.", ex);
		}
		return messageSize;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailSenderService#saveDraft(long, long[], long[], java.lang.String, java.lang.String, java.util.List)
	 */
	@Override
	public int saveDraft (final long from, final long[] to, final long[] cc, final String message,
			final String subject, final List<FileTO> attachments, final Integer draftNumber) {
		final UserData userData = userDataService.getUser (from);
		final Session session = Session.getInstance (props, new EmailAuthenticator (
				userData.getScreenName (), userData.getEmailPassword ()));
		try {
			final Store store = session.getStore ();
			store.connect ();
			final Folder drafts = store.getDefaultFolder ().getFolder ("INBOX").getFolder ("Drafts");
			if (draftNumber != null) {
				drafts.open (Folder.READ_WRITE);
				drafts.getMessage (draftNumber).setFlag (Flag.DELETED, true);
				drafts.close (true);
			}
			final MimeMessage mimeMessage = new MimeMessage (session);
			if (attachments == null || attachments.isEmpty ()) {
				mimeMessage.setContent (message, "text/html");
			} else {
				Multipart multipart = new MimeMultipart ();
				BodyPart bodyPart = new MimeBodyPart ();
				bodyPart.setContent (message, "text/html");
				multipart.addBodyPart (bodyPart);
				DataSource dataSource;
				for (FileTO attachment : attachments) {
					dataSource = new FileDataSource (attachment.getFile ());
					bodyPart = new MimeBodyPart ();
					bodyPart.setDataHandler (new DataHandler (dataSource));
					bodyPart.setFileName (attachment.getFileInfo ().getFileName ());
					multipart.addBodyPart (bodyPart);
				}
				mimeMessage.setContent (multipart);
			}
			mimeMessage.setSubject (subject);
			mimeMessage.setRecipients (RecipientType.TO, createAddresses (to));
			if (cc != null && cc.length > 0) {
				mimeMessage.setRecipients (RecipientType.CC, createAddresses (cc));
			}
			mimeMessage.setFrom (new InternetAddress (userData.getScreenName () + '@'
					+ hanPropertiesService.getValue (HAN_EMAIL_DOMAIN_KEY),
					userData.getContact ().getFirstName () + " " + userData.getContact ().getLastName ()));
			mimeMessage.setFlag (Flag.DRAFT, true);
			drafts.appendMessages (new Message[]{mimeMessage});
			drafts.open (Folder.READ_ONLY);
			final int messageNumber = drafts.getMessageCount ();
			drafts.close (true);
			return messageNumber;
		} catch (AddressException ex) {
			log.error ("Error creating addresses", ex);
		} catch (MessagingException ex) {
			log.error ("Error creating email message.", ex);
		} catch (UnsupportedEncodingException ex) {
			log.error ("Unsupported encoding found while creating addresses.", ex);
		}
		return -1;
	}

}
