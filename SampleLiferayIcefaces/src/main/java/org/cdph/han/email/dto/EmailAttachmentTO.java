package org.cdph.han.email.dto;

/*import com.icesoft.faces.context.ByteArrayResource;
import com.icesoft.faces.context.Resource;
*/
/**
 * Data transfer objects for email attachments
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EmailAttachmentTO {
	/** The original file name */
	private String fileName;
	/** The mime type of the file */
	private String mimeType;
	/** The actual file */
	private byte[] content;

	/**
	 * Creates a new attachment object using the provided information
	 * @param fileName original filename of the attachment
	 * @param mimeType the mime type of the attachment
	 * @param content of the attachment
	 */
	public EmailAttachmentTO (final String fileName, final String mimeType, final byte[] content) {
		this.fileName = fileName;
		this.mimeType = mimeType;
		this.content = content;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName () {
		return fileName;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType () {
		return mimeType;
	}

	/**
	 * @return the content
	 */
	public byte[] getContent () {
		return content;
	}

	/**
	 * @return the formattedSize
	 */
	public String getFormattedSize () {
		String size;
		if (content.length < 1024) {
			size = content.length + " B";
		} else if (content.length < (1024 * 1024)) {
			size = (content.length / 1024) + " KB";
		} else {
			size = (content.length / (1024 * 1024)) + " MB";
		}
		return size;
	}

	/**
	 * @return the fileContent
	 */
	/*public Resource getFileContent () {
		return new ByteArrayResource (content);
	}*/

}
