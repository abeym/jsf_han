package org.cdph.han.email.jobs;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.FileTO;
import org.cdph.han.alerts.services.MessagesService;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.email.dto.QuotaTO;
import org.cdph.han.email.services.EmailSenderService;
import org.cdph.han.email.services.QuotaService;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.services.HanPropertiesService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class QuotaWarningJob extends QuartzJobBean {
	/** Job log */
	private static final Log log = LogFactory.getLog (QuotaWarningJob.class);
	/** Key to retrieve the quota limit */
	private static final String QUOTA_WARNING_LIMIT_KEY = "HAN_QUOTA_WARNING_LIMIT";
	/** Key to retrieve the quota limit warning message subject */
	private static final String QUOTA_WARNING_SUBJECT_KEY = "email.inbox.quota.warning.subject";
	/** Key to retrieve the quota limit warning message content */
	private static final String QUOTA_WARNING_CONTENT_KEY = "email.inbox.quota.warning.content";

	/* (non-Javadoc)
	 * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
	 */
	@Override
	protected void executeInternal (final JobExecutionContext jobContext)
			throws JobExecutionException {
		log.info ("Scanning for users with quota close to limit.");
		try {
			final ApplicationContext appCtx =
				(ApplicationContext) jobContext.getScheduler ().getContext ().get ("applicationContext");
			final UserDataService userDataService = (UserDataService) appCtx.getBean ("userService");
			final QuotaService quotaService = (QuotaService) appCtx.getBean ("quotaService");
			final HanPropertiesService hanPropertiesService =
				(HanPropertiesService) appCtx.getBean ("hanPropertiesService");
			final MessagesService messagesService = (MessagesService) appCtx.getBean ("messagesService");
			final EmailSenderService emailSenderService =
				(EmailSenderService) appCtx.getBean ("emailSenderService");
			final String subject = messagesService.getMessage (QUOTA_WARNING_SUBJECT_KEY);
			final String content = messagesService.getMessage (QUOTA_WARNING_CONTENT_KEY);
			final int quotaWarning = Integer.parseInt (hanPropertiesService.getValue (QUOTA_WARNING_LIMIT_KEY));
			final List<UserData> users = userDataService.getAllUsers ();
			final List<FileTO> attachments = new ArrayList<FileTO> ();
			QuotaTO quota;
			for (UserData user : users) {
				if (user.isActive ()) {
					try {
						quota = quotaService.getUserQuota (user.getUserId ());
					} catch (Exception ex) {
						log.warn ("Couldn't retrieve quota for user: " + user.getEmail ());
						continue;
					}
					if (quota.getUsed () >= quotaWarning) {
						if (!user.isQuotaWarned ()) {
							emailSenderService.sendAdminEmail (new long[]{user.getUserId ()}, new long[]{},
									content, subject, attachments);
							userDataService.toggleWarnedUser (user.getUserId (), true);
						}
					} else {
						if (user.isQuotaWarned ()) {
							userDataService.toggleWarnedUser (user.getUserId (), false);
						}
					}
				}
			}
		} catch (Exception ex) {
			log.error ("Error scanning for users with quota close to limit.", ex);
		}
	}

}
