package org.cdph.han.email.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.custgroups.dto.FilterCriteriaTO;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.custgroups.services.OrganizationalSearchService;
import org.cdph.han.email.dto.EmailAddressTO;
import org.cdph.han.persistence.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the service used to recover all the users available in the given filter
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Service ("emailUsersService")
public class EmailUsersServiceImpl implements EmailUsersService {
	/** Class log */
	private static final Log log = LogFactory.getLog (EmailUsersServiceImpl.class);
	/** Query for searching users */
	private static final String SCREEN_NAME_QUERY = "select u from UserData u where u.screenName = :screenName";
	/** Service for searching users */
	//abey...
	//@Autowired
	private OrganizationalSearchService organizationalSearchService;
	/** Persistence context */
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailUsersService#getUserIds(java.util.List)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public long[] getUserIds (final List<RecipientTO> filter) {
		final List<Long> userIds = new ArrayList<Long> ();
		List<UserDisplayTO> foundUsers;
		for (RecipientTO user : filter) {
			if (RecipientType.CRITERIA.equals (user.getType ())) {
				for (FilterCriteriaTO filterCriteria : user.getCriteria ()) {
					if (filterCriteria.getOrganizationId () == null && filterCriteria.getAreaFieldId () == null
							&& filterCriteria.getRoleId () == null) {
						foundUsers = organizationalSearchService.getUsersByOrganizationType (
								filterCriteria.getOrganizationTypeId ());
					} else {
						foundUsers = organizationalSearchService.filterUsers (
								filterCriteria.getOrganizationId () == null ? new long[]{}
								: new long[]{filterCriteria.getOrganizationId ()},
								filterCriteria.getAreaFieldId () == null ? new long[]{} 
								: new long[]{filterCriteria.getAreaFieldId ()},
								filterCriteria.getRoleId () == null ? new long[]{}
								: new long[]{filterCriteria.getRoleId ()});
					}
					for (UserDisplayTO foundUser : foundUsers) {
						if (!userIds.contains (foundUser.getId ())) {
							userIds.add (foundUser.getId ());
						}
					}
				}
			} else if (RecipientType.USER.equals (user.getType ())) {
				if (!userIds.contains (user.getId ())) {
					userIds.add (user.getId ());
				}
			}
		}
		int i = 0;
		long[] temp = new long[userIds.size ()];
		for (Long userId : userIds) {
			temp[i++] = userId;
		}
		return temp;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.email.services.EmailUsersService#getRecipients(java.lang.String[])
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public List<RecipientTO> getRecipients (final EmailAddressTO[] addresses) {
		final List<RecipientTO> recipients = new ArrayList<RecipientTO> ();
		String screenName;
		if (addresses != null && addresses.length > 0) {
			final Query query = entityManager.createQuery (SCREEN_NAME_QUERY);
			UserData user;
			for (EmailAddressTO address : addresses) {
				screenName = address.getAddress ().substring (0, address.getAddress ().lastIndexOf ('@'));
				query.setParameter ("screenName", screenName);
				user = (UserData) query.getSingleResult ();
				recipients.add (new RecipientTO (user.getUserId (), user.getContact ().getFirstName () + " "
						+ user.getContact ().getLastName (), RecipientType.USER));
			}
		}
		return recipients;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
