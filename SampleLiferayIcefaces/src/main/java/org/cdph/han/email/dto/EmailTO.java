package org.cdph.han.email.dto;

import java.util.Date;

/**
 * Transfer object containing all the email message details
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EmailTO {
	/** Recipients in the To field */
	private EmailAddressTO[] to;
	/** Recipients in the CC field */
	private EmailAddressTO[] cc;
	/** User that sent the message */
	private EmailAddressTO from;
	/** Attachments in the message */
	private EmailAttachmentTO[] attachments;
	/** Content of the message */
	private String content;
	/** Message number */
	private int messageNumber;
	/** Folder name */
	private String folderName;
	/** Email sent date */
	private Date sent;
	/** Email subject */
	private String subject;
	/** If the message is html or plain text */
	private boolean html;

	/**
	 * Creates a new EmailTO with the provided parameters
	 * @param to recipients
	 * @param cc recipients
	 * @param from user
	 * @param attachments in the message
	 * @param content of the message
	 */
	public EmailTO (final EmailAddressTO[] to, final EmailAddressTO[] cc, final EmailAddressTO from,
			final EmailAttachmentTO[] attachments, final String content, final String folderName,
			final Date sent, final String subject, final int messageNumber, final boolean html) {
		this.to = to;
		this.cc = cc;
		this.from = from;
		this.attachments = attachments;
		this.content = content;
		this.folderName = folderName;
		this.sent = sent;
		this.subject = subject;
		this.messageNumber = messageNumber;
		this.html = html;
	}

	/**
	 * @return the to
	 */
	public EmailAddressTO[] getTo () {
		return to;
	}

	/**
	 * @return the formattedTo
	 */
	public String getFormattedTo () {
		final StringBuilder builder = new StringBuilder ();
		for (EmailAddressTO to : this.to) {
			builder.append (to).append (", ");
		}
		return builder.length () > 0 ? builder.substring (0, builder.length () - 2) : "";
	}

	/**
	 * @return the cc
	 */
	public EmailAddressTO[] getCc () {
		return cc;
	}

	/**
	 * @return the formattedCc
	 */
	public String getFormattedCc () {
		final StringBuilder builder = new StringBuilder ();
		for (EmailAddressTO cc : this.cc) {
			builder.append (cc).append (", ");
		}
		return builder.length () > 0 ? builder.substring (0, builder.length () - 2) : "";
	}

	/**
	 * @return the from
	 */
	public EmailAddressTO getFrom () {
		return from;
	}

	/**
	 * @return the attachments
	 */
	public EmailAttachmentTO[] getAttachments () {
		return attachments;
	}

	/**
	 * @return the content
	 */
	public String getContent () {
		return content;
	}

	/**
	 * @return the messageNumber
	 */
	public int getMessageNumber () {
		return messageNumber;
	}

	/**
	 * @param messageNumber the messageNumber to set
	 */
	public void setMessageNumber (int messageNumber) {
		this.messageNumber = messageNumber;
	}

	/**
	 * @return the folderName
	 */
	public String getFolderName () {
		return folderName;
	}

	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName (String folderName) {
		this.folderName = folderName;
	}

	/**
	 * @return the sent
	 */
	public Date getSent () {
		return sent;
	}

	/**
	 * @param sent the sent to set
	 */
	public void setSent (Date sent) {
		this.sent = sent;
	}

	/**
	 * @return the subject
	 */
	public String getSubject () {
		return subject;
	}

	/**
	 * @param subject the subject to set
	 */
	public void setSubject (String subject) {
		this.subject = subject;
	}

	/**
	 * @return the html
	 */
	public boolean isHtml () {
		return html;
	}

}
