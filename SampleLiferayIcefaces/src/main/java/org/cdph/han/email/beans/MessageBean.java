package org.cdph.han.email.beans;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.AbortProcessingException;
import javax.faces.event.ActionEvent;
import javax.faces.event.ActionListener;
import javax.mail.MessagingException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.MessagesService;
import org.cdph.han.email.component.EmailSummaryComparator;
import org.cdph.han.email.dto.EmailSummaryTO;
import org.cdph.han.email.dto.EmailTO;
import org.cdph.han.email.dto.EmailSummaryTO.Column;
import org.cdph.han.email.services.EmailMessageService;
import org.cdph.han.email.services.EmailOperationOutcome;
import org.cdph.han.email.util.EmailUtil;

import com.icesoft.faces.component.ext.RowSelectorEvent;

/**
 * Backed bean used to manipulate email messages
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class MessageBean implements ActionListener {
	/** Class log */
	private static final Log log = LogFactory.getLog (MessageBean.class);
	/** Service for retrieving messages */
	private EmailMessageService emailMessageService;
	/** Service for retrieving messages from the resource bundle */
	private MessagesService messagesService;
	/** List of messages */
	private EmailSummaryTO[] messages;
	/** Selected folder */
	private String folderName;
	/** Column to use for sorting */
	private String sortColumn = "date";
	/** If using ascending order */
	private boolean ascending;
	/** Field to implement memento pattern */
	private String oldSortColumn;
	/** Field to implement memento pattern */
	private boolean oldAscending;
	/** Selected value to apply to all messages */
	private boolean selectAll;
	/** Message retrieved from the email server for reading */
	private EmailTO email;
	/** Flag to display a message */
	private boolean showMessage;
	/** Flag to display the move messages popup */
	private boolean moveVisible;
	/** Folder to move the messages */
	private String toMoveFolder;
	/** Flag to display the delete messages popup */
	private boolean deleteVisible;
	/** Flag to move the messages to the trash folder */
	private boolean moveToTrash;

	/**
	 * This method retrieves all the available messages from the current folder
	 */
	private void retrieveMessages () {
		try {
			messages = emailMessageService.listMessagesInFolder (EmailUtil.getInstance ().getUserId (), folderName);
			sort ();
		} catch (MessagingException ex) {
			log.error ("Error retrieving messages in folder: " + folderName, ex);
			FacesContext.getCurrentInstance ().addMessage (null, messagesService.getMessage (
					"email.inbox.folder.messages.error", "Error retrieving messages, please try again later",
					FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * Sorts the given array using a custom comparator using the provided sort column
	 */
	private void sort () {
		EmailSummaryComparator comparator;
		EmailSummaryTO[] sorted = messages;
		if ("date".equals (sortColumn)) {
			comparator = new EmailSummaryComparator (Column.DATE);
		} else if ("from".equals (sortColumn)) {
			comparator = new EmailSummaryComparator (Column.FROM);
		} else if ("subject".equals (sortColumn)) {
			comparator = new EmailSummaryComparator (Column.SUBJECT);
		} else if ("size".equals (sortColumn)) {
			comparator = new EmailSummaryComparator (Column.SIZE);
		} else if ("attachments".equals (sortColumn)) {
			comparator = new EmailSummaryComparator (Column.ATTACHMENTS);
		} else {
			comparator = new EmailSummaryComparator (Column.NUMBER);
		}
		Arrays.sort (sorted, comparator);
		if (ascending) {
			EmailSummaryTO[] temporal = new EmailSummaryTO[sorted.length];
			int i = temporal.length - 1;
			for (EmailSummaryTO summary : sorted) {
				temporal[i--] = summary;
			}
			sorted = temporal;
		}
		for (EmailSummaryTO header : sorted) {
			log.debug ("Email: " + header.getFrom () + " " + header.getSubject () + " " + header.getDate ());
		}
		messages = sorted;
	}

	/**
	 * This method initializes the bean after the email message service has been set
	 */
	private void init () {
		folderName = "INBOX";
		sortColumn = "date";
		ascending = true;
		retrieveMessages ();
	}

	/**
	 * Resort the messages if needed
	 */
	private void reSort () {
		// Check if resorting is needed
		if (ascending != oldAscending || !sortColumn.equals (oldSortColumn)) {
			retrieveMessages ();
		}
	}

	/**
	 * Sets all the messages selected box to the new value
	 * @param event
	 */
	public void toggleSelectAll (final ActionEvent event) {
		selectAll = !selectAll;
		for (EmailSummaryTO summary : messages) {
			summary.setSelected (selectAll);
		}
	}

	/**
	 * Action used to retrieve the messages from the provided folder
	 * @return outcome of the operation
	 */
	public String loadMessages () {
		String outcome = "success";
		sortColumn = oldSortColumn = "date";
		ascending = oldAscending = true;
		log.debug ("Folder name: " + folderName);
		retrieveMessages ();
		showMessage = false;
		return outcome;
	}

	/**
	 * Selector listener for changes in the message selected
	 * @param event fired after the selection of a message
	 */
	public void messageSelected (final RowSelectorEvent event) {
		log.debug ("Selected row: " + event.getRow ());
		try {
			if (messages[event.getRow ()].isDisplay ()) {
				final long userId = EmailUtil.getInstance ().getUserId ();
				email = emailMessageService.retrieveMessage (userId, folderName,
						messages[event.getRow ()].getNumber ());
				emailMessageService.markAsSeen (userId, folderName,
						new int[]{messages[event.getRow ()].getNumber ()});
				messages[event.getRow ()].setNewMessage (false);
				showMessage = true;
			} else {
				showMessage = false;
			}
		} catch (Exception ex) {
			log.error ("Error retrieving email message", ex);
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.compose.unavailable.email",
							"The email you selected is not available, please try again later",
							FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * Action lister to show the delete messages popup
	 * @param event ActionEvent
	 */
	public void toggleDeleteMessages (final ActionEvent event) {
		final String stringMoveToTrash = FacesContext.getCurrentInstance ().getExternalContext (
				).getRequestParameterMap ().get ("moveToTrash");
		moveToTrash = stringMoveToTrash != null ? Boolean.parseBoolean (stringMoveToTrash) : true;
		deleteVisible = !deleteVisible;
	}

	/**
	 * The action listener move the messages to the trash folder and flags them as deleted
	 * @param event action event
	 */
	public void deleteMessages (final ActionEvent event) {
		deleteVisible = false;
		selectAll = false;
		try {
			final int[] selected = getSelectedMessages ();
			if (selected.length > 0) {
				emailMessageService.deleteMessages (EmailUtil.getInstance ().getUserId (), folderName,
						selected, moveToTrash);
				loadMessages ();
			} else {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.delete.no.selection",
								"There are no messages selected to be deleted",
								FacesMessage.SEVERITY_ERROR));
			}
		} catch (Exception ex) {
			log.error ("Error deleting messages", ex);
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.delete.error",
							"Error deleting message(s), try again later",
							FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * Returns all the selected messages
	 * @return the selected messages
	 */
	private int[] getSelectedMessages () {
		final List<Integer> selected = new ArrayList<Integer> ();
		for (EmailSummaryTO summary : messages) {
			if (summary.isSelected ()) {
				selected.add (summary.getNumber ());
			}
		}
		final int[] selectedArray = new int[selected.size ()];
		int i = 0;
		for (Integer messageNumber : selected) {
			selectedArray[i++] = messageNumber;
		}
		return selectedArray;
	}

	/**
	 * Hide the message view
	 * @return
	 */
	public String hideMessage () {
		showMessage = false;
		return "success";
	}

	/**
	 * Toggles the move popup visibility
	 * @param event
	 */
	public void toggleMove (final ActionEvent event) {
		if (!moveVisible) {
			toMoveFolder = null;
		}
		moveVisible = !moveVisible;
	}

	/**
	 * Action listener that moves the selected messages to the selected folder
	 * @param event
	 */
	public void moveMessages (final ActionEvent event) {
		moveVisible = false;
		if (folderName != null && folderName.endsWith ("Drafts")) {
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.move.draft",
							"You cannot move draft messages",
							FacesMessage.SEVERITY_ERROR));
		}else if (toMoveFolder == null) {
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.move.no.selection",
							"You must select a destination folder",
							FacesMessage.SEVERITY_ERROR));
		} else if (toMoveFolder.equals (folderName)) {
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("email.inbox.folder.move.same.folder",
							"The destination folder must be different to the source folder",
							FacesMessage.SEVERITY_ERROR));
		} else {
			try {
				final int[] selected = getSelectedMessages ();
				if (selected.length == 0) {
					FacesContext.getCurrentInstance ().addMessage (null,
							messagesService.getMessage ("email.inbox.folder.move.no.messages",
									"You must select the messages to be moved",
									FacesMessage.SEVERITY_ERROR));
				} else {
					EmailOperationOutcome outcome = emailMessageService.moveMessageToFolder (
							EmailUtil.getInstance ().getUserId (), selected,
							folderName, toMoveFolder);
					if (EmailOperationOutcome.MESSAGE_MOVED.equals (outcome)) {
						FacesContext.getCurrentInstance ().addMessage (null,
								messagesService.getMessage ("email.inbox.folder.move.success",
										"The messages were moved to the selected folder",
										FacesMessage.SEVERITY_INFO));
					} else {
						FacesContext.getCurrentInstance ().addMessage (null,
								messagesService.getMessage ("email.inbox.folder.move.error",
										"Error moving messages, please try again later",
										FacesMessage.SEVERITY_ERROR));
					}
				}
			} catch (Exception ex) {
				FacesContext.getCurrentInstance ().addMessage (null,
						messagesService.getMessage ("email.inbox.folder.move.error",
								"Error moving messages, please try again later",
								FacesMessage.SEVERITY_ERROR));
			}
		}
	}

	/* (non-Javadoc)
	 * @see javax.faces.event.ActionListener#processAction(javax.faces.event.ActionEvent)
	 */
	@Override
	public void processAction (final ActionEvent arg0) throws AbortProcessingException {
		retrieveMessages ();
	}

	/**
	 * @return the messages
	 */
	public EmailSummaryTO[] getMessages () {
		return messages;
	}

	/**
	 * @param messages the messages to set
	 */
	public void setMessages (EmailSummaryTO[] messages) {
		this.messages = messages;
	}

	/**
	 * @return the folderName
	 */
	public String getFolderName () {
		return folderName;
	}

	/**
	 * @param folderName the folderName to set
	 */
	public void setFolderName (String folderName) {
		this.folderName = folderName;
	}

	/**
	 * @param emailMessageService the emailMessageService to set
	 */
	public void setEmailMessageService (EmailMessageService emailMessageService) {
		this.emailMessageService = emailMessageService;
		init ();
	}

	/**
	 * @param messagesService the messagesService to set
	 */
	public void setMessagesService (MessagesService messagesService) {
		this.messagesService = messagesService;
	}

	/**
	 * @return the sortColumn
	 */
	public String getSortColumn () {
		return sortColumn;
	}

	/**
	 * @param sortColumn the sortColumn to set
	 */
	public void setSortColumn (String sortColumn) {
		oldSortColumn = this.sortColumn;
		this.sortColumn = sortColumn;
		reSort ();
	}

	/**
	 * @return the ascending
	 */
	public boolean isAscending () {
		return ascending;
	}

	/**
	 * @param ascending the ascending to set
	 */
	public void setAscending (boolean ascending) {
		oldAscending = this.ascending;
		this.ascending = ascending;
		reSort ();
	}

	/**
	 * @return the selectAll
	 */
	public boolean isSelectAll () {
		return selectAll;
	}

	/**
	 * @param selectAll the selectAll to set
	 */
	public void setSelectAll (boolean selectAll) {
		this.selectAll = selectAll;
	}

	/**
	 * @return the select all caption 
	 */
	public String getSelectAllCaption () {
		String caption;
		if (!selectAll) {
			caption = "Select All";
		} else {
			caption = "Clear All";
		}
		return caption;
	}

	/**
	 * @return the email
	 */
	public EmailTO getEmail () {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail (EmailTO email) {
		this.email = email;
	}

	/**
	 * @return the showMessage
	 */
	public boolean isShowMessage () {
		return showMessage;
	}

	/**
	 * @param showMessage the showMessage to set
	 */
	public void setShowMessage (boolean showMessage) {
		this.showMessage = showMessage;
	}

	/**
	 * @return the moveVisible
	 */
	public boolean isMoveVisible () {
		return moveVisible;
	}

	/**
	 * @param moveVisible the moveVisible to set
	 */
	public void setMoveVisible (boolean moveVisible) {
		this.moveVisible = moveVisible;
	}

	/**
	 * @return the toMoveFolder
	 */
	public String getToMoveFolder () {
		return toMoveFolder;
	}

	/**
	 * @param toMoveFolder the toMoveFolder to set
	 */
	public void setToMoveFolder (String toMoveFolder) {
		this.toMoveFolder = toMoveFolder;
	}

	/**
	 * @return if the message is a draft
	 */
	public boolean isDraft () {
		return folderName == null ? false : folderName.endsWith ("Drafts");
	}

	/**
	 * @return the deleteVisible
	 */
	public boolean isDeleteVisible () {
		return deleteVisible;
	}

	/**
	 * @param deleteVisible the deleteVisible to set
	 */
	public void setDeleteVisible (boolean deleteVisible) {
		this.deleteVisible = deleteVisible;
	}

	/**
	 * @return the moveToTrash
	 */
	public boolean isMoveToTrash () {
		return moveToTrash;
	}

	/**
	 * @param moveToTrash the moveToTrash to set
	 */
	public void setMoveToTrash (boolean moveToTrash) {
		this.moveToTrash = moveToTrash;
	}

}
