package org.cdph.han.email.dto;

import java.util.Date;

/**
 * This transfer object contains the summary information of an email
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class EmailSummaryTO {
	/** The email subject */
	private String subject;
	/** Date the email was sent */
	private Date date;
	/** Size of the email */
	private int size;
	/** If the email contains attachments */
	private boolean attachments;
	/** Name of the sender */
	private EmailAddressTO from;
	/** Number of the message in the current folder */
	private int number;
	/** If the email was selected or not */
	private boolean selected;
	/** If the mail is selected to display its contents */
	private boolean display;
	/** If the mail is new */
	private boolean newMessage;

	/**
	 * Constructor for the immutable class
	 * @param from the name of the sender
	 * @param subject the subject of the email
	 * @param date the date the message was sent
	 * @param size the size of the message
	 * @param attachments if the message has any attachment
	 * @param number the number of the message in the current folder
	 */
	public EmailSummaryTO (final EmailAddressTO from, final String subject, final Date date, final int size,
			final boolean attachments, final int number, final boolean newMessage) {
		this.from = from;
		this.subject = subject;
		this.date = date;
		this.size = size;
		this.attachments = attachments;
		this.number = number;
		this.newMessage = newMessage;
	}

	/**
	 * @return the subject
	 */
	public String getSubject () {
		return subject;
	}

	/**
	 * @return the date
	 */
	public Date getDate () {
		return date;
	}

	/**
	 * @return the size
	 */
	public int getSize () {
		return size;
	}

	/**
	 * @return the attachments
	 */
	public boolean isAttachments () {
		return attachments;
	}

	/**
	 * @return the from
	 */
	public EmailAddressTO getFrom () {
		return from;
	}

	/**
	 * @return the number
	 */
	public int getNumber () {
		return number;
	}

	public enum Column {
		FROM, SUBJECT, DATE, SIZE, ATTACHMENTS, NUMBER
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected () {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected (boolean selected) {
		this.selected = selected;
	}

	/**
	 * @return formatted size
	 */
	public String getFormattedSize () {
		String formatted;
		if (size > 1024) {
			formatted = size / 1024 + " kB";
		} else if (size > (1024 * 1024)) {
			formatted = size / (1024 * 1024) + " MB";
		} else {
			formatted = size + " B";
		}
		return formatted;
	}

	/**
	 * @return the display
	 */
	public boolean isDisplay () {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay (boolean display) {
		this.display = display;
	}

	/**
	 * @return the newMessage
	 */
	public boolean isNewMessage () {
		return newMessage;
	}

	/**
	 * @param newMessage the newMessage to set
	 */
	public void setNewMessage (boolean newMessage) {
		this.newMessage = newMessage;
	}

	/**
	 * @return the style
	 */
	public String getStyle () {
		String style;
		if (newMessage) {
			style = "font-weight: bold;";
		} else {
			style = "font-weight: normal;";
		}
		return style;
	}

}
