package org.cdph.han.registration.jms;

import java.util.HashMap;
import java.util.Map;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.velocity.app.VelocityEngine;
import org.cdph.han.persistence.RegistrationRequestStatus;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.registration.service.RegistrationService;
import org.cdph.han.util.services.HanPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineUtils;

public class HanEmailQueueListener implements MessageListener {

	/** Class logger */
	private static Log log = LogFactory.getLog(HanEmailQueueListener.class);
	
	private String EMAIL_FROM_ADDRESS = "han_admin@staging.chicagohan.org";
	private String EMAIL_FROM_NAME = "HAN Administrator";

	//abey...
	//@Autowired
	private RegistrationService service;

	//abey...
	//@Autowired
	private JavaMailSender mailSender;

	//abey...
	//@Autowired
	private VelocityEngine velocityEngine;

	/** Service used to retrieve the email address and mail sender name */
	//abey...
	//@Autowired
	private HanPropertiesService hanPropertiesService;
	/** DB Key for the email address */
	private static final String HAN_EMAIL_FROM_ADDRESS_KEY = "HAN_EMAIL_FROM_ADDRESS";
	/** DB Key for the sender name */
	private static final String HAN_EMAIL_SENDER_NAME_KEY = "HAN_EMAIL_SENDER_NAME";

	//abey ...@Transactional
	public void onMessage(Message message) {
		log.debug("onMessage...");
		log.debug(message);

		if (message instanceof ObjectMessage) {
			log.debug("OBJECT MESSAGE");
			try {
				Long requestFormId = (Long) ((ObjectMessage) message)
						.getObject();
				if (log.isDebugEnabled()) {
					log.debug("Request ID: " + requestFormId);
				}

				RequestForm requestForm = service
						.getRegistrationRequest(requestFormId);
				
				if (log.isDebugEnabled()) {
					log.debug("After querying the DB Request ID: " + requestForm.getId());
				}

				if (requestForm.getStatus() == RegistrationRequestStatus.SUBMITTED) {
					log
							.info("Sending registration request notification email...");
					sendRegistrationEmail(requestForm);
				} else if (requestForm.getStatus() == RegistrationRequestStatus.REJECTED) {
					log
							.info("Sending registration rejection notification email...");
					sendRejectionNotificaionEmail(requestForm);
				} else {
					log.warn("Registration request doesn't have a status that"
							+ "triggers a notication email: "
							+ requestForm.getStatus());
				}

			} catch (Exception jex) {
				log.error(jex.getMessage(), jex);
				throw new RuntimeException(jex);
			}

		} else {
			IllegalArgumentException iaex = new IllegalArgumentException(
					"Message must be of type ObjectMessage");
			log.error(iaex.getMessage(), iaex);
			throw iaex;
		}
	}

	private void sendRegistrationEmail(final RequestForm form) {
		log.debug("In sendRegistrationEmail(RequestForm form)...");
		if (form == null) {
			log.warn("Request passed as parameter is null. Exiting method...");
			return;
		}

		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				message.setTo(form.getEmail());
				message.setFrom(new InternetAddress(hanPropertiesService.getValue (HAN_EMAIL_FROM_ADDRESS_KEY),
						hanPropertiesService.getValue (HAN_EMAIL_SENDER_NAME_KEY)));
				message.setSubject("CDPH HAN Registration Request Confirmation of Receipt");
				Map<String, Object> model = new HashMap<String, Object>();
				model.put("registrationReq", form);
				String text = VelocityEngineUtils
						.mergeTemplateIntoString(
								velocityEngine,
								"org/cdph/han/registration/jms/registration-confirmation.vm",
								model);
				message.setText(text, true);
			}
		};

		mailSender.send(preparator);
	}

	private void sendRejectionNotificaionEmail(final RequestForm form) {
		log.debug("In sendRejectionNotificaionEmail(RequestForm form)...");
		if (form == null) {
			log.warn("Request passed as parameter is null. Exiting method...");
			return;
		}
		
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) throws Exception {
				MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
				message.setTo(form.getEmail());
				message.setFrom(new InternetAddress(hanPropertiesService.getValue (HAN_EMAIL_FROM_ADDRESS_KEY),
						hanPropertiesService.getValue (HAN_EMAIL_SENDER_NAME_KEY)));
				message.setSubject("HAN registration rejected");
				Map<String, Object> model = new HashMap<String, Object>();
				model.put("registrationReq", form);
				String text = VelocityEngineUtils
						.mergeTemplateIntoString(
								velocityEngine,
								"org/cdph/han/registration/jms/registration-rejection.vm",
								model);
				message.setText(text, true);
			}
		};

		mailSender.send(preparator);
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	public void setVelocityEngine(VelocityEngine velocityEngine) {
		this.velocityEngine = velocityEngine;
	}

	public void setEmailFromAddress(String email_from_address) {
		EMAIL_FROM_ADDRESS = email_from_address;
	}

	public void setEmailFromName(String email_from_name) {
		EMAIL_FROM_NAME = email_from_name;
	}
}
