package org.cdph.han.registration.dto;

import java.util.List;

import org.cdph.han.dto.LazyDataModel;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.registration.service.RegistrationService;

public class RequestFormDTO extends LazyDataModel<RequestForm>{
	
	private RegistrationService service;
	
	public RequestFormDTO(RegistrationService service) {
		this.service = service;
	}

	@Override
	public int countRows() {
		return service.getPendingRegistrationRequestsCount();
	}

	@Override
	public List<RequestForm> findRows(int startRow, int finishRow) {
		
		return service.getPendingRegistrationRequests(startRow, (finishRow - startRow)+1);
	}

	public void setService(RegistrationService service) {
		this.service = service;
	}

}
