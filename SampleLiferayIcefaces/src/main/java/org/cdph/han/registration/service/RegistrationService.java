package org.cdph.han.registration.service;

import java.util.List;

import org.cdph.han.persistence.AreaField;
import org.cdph.han.persistence.Organization;
import org.cdph.han.persistence.RequestForm;
import org.cdph.han.persistence.Role;
import org.cdph.han.registration.exception.EmailAlreadyExistsException;

import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.DuplicateUserScreenNameException;

public interface RegistrationService {

	public static final String REGULAR_ROLE = "regular";
	public static final String COMMUNITY_ROLE = "community";
	public static final String ORGANIZATIONAL_ROLE = "organization";

	public Long registerRequest(RequestForm form) throws EmailAlreadyExistsException;
	
	public int getPendingRegistrationRequestsCount();

	public List<RequestForm> getPendingRegistrationRequests();
	
	public List<RequestForm> getPendingRegistrationRequests(int startIndex, int maxResults);

	public RequestForm getRegistrationRequest(Long id);

	public List<Organization> getOrganizationTypes();

	public List<Organization> getSuborganizations(Long parentOrgId);

	public List<Role> getOrganizationalRoles(Long orgTypeId);

	public List<AreaField> getAreaFields(Long oganizationType);

	public boolean usernameExist(String username);

	public boolean emailExist(String email);

	public void approveRequest(RequestForm form)
			throws DuplicateUserEmailAddressException,
			DuplicateUserScreenNameException;

	public void rejectRequest(RequestForm request);

	public Organization getOrganization(Long id);

	public AreaField getAreaField(Integer id);

	public Role getRole(Long id);
	
	public void enqueueNotificationEmail(Long registrationReqId);
}
