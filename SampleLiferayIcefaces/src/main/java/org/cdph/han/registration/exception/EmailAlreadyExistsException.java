package org.cdph.han.registration.exception;

public class EmailAlreadyExistsException extends Exception {
	
	public EmailAlreadyExistsException() {
		super();
	}

	public EmailAlreadyExistsException(String msg) {
		super(msg);
	}

	public EmailAlreadyExistsException(String msg, Throwable cause) {
		super(msg, cause);
	}

	public EmailAlreadyExistsException(Throwable cause) {
		super(cause);
	}
	
}
