package org.cdph.han.batch.jms;


import java.util.Date;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.batch.service.BatchService;
import org.cdph.han.persistence.BatchStatus;
import org.cdph.han.persistence.NonHanUserGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

public class UploadQueueListener implements MessageListener {

	/** Class logger */
	private static Log log = LogFactory.getLog(UploadQueueListener.class);
	
	//abey...
	//@Autowired
	private BatchService service;

	//abey ...@Transactional
	public void onMessage(Message message) {
		log.debug("onMessage...");
		log.debug(message);
		
		NonHanUserGroup group = null;
		
		if (message instanceof ObjectMessage) {
			log.debug("OBJECT MESSAGE");
			try {
				Long groupId = (Long) ((ObjectMessage) message)
						.getObject();
				if (log.isDebugEnabled()) {
					log.debug("Group.ID: " + groupId);
				}
				
				log.debug("Getting non han group data for id: " + groupId);
				group = service.getNonHanUserGroup(groupId);
				
				group.setStatus(BatchStatus.PROCESSING);				
				service.updateNonHanGroup(group);
				
				service.processUploadBatch(group);
				
				group.setStatus(BatchStatus.FINISHED);	
				
			} catch (Exception jex) {
				log.error(jex.getMessage(), jex);
				group.setErrorLog(jex.getMessage());
				group.setStatus(BatchStatus.ERROR);
				throw new RuntimeException(jex);
			} finally {
				try {
					group.setProcessedDate(new Date());
					service.updateNonHanGroup(group);
				} catch (Exception ex) {
					log
							.warn("Error updating non han user group status and processed date");
				}
			}

		} else {
			IllegalArgumentException iaex = new IllegalArgumentException(
					"Message must be of type ObjectMessage");
			log.error(iaex.getMessage(), iaex);
			throw iaex;
		}
	}
}
