package org.cdph.han.batch.service;

import java.util.List;

import org.cdph.han.batch.BatchException;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserGroup;
import org.cdph.han.persistence.UserData;

public interface BatchService {
	
	public UserData getUserDataById(long userId);
	
	public void createNonHanGroup(NonHanUserGroup group);
	
	public void updateNonHanGroup(NonHanUserGroup group);
	
	public void softDeleteNonHanGroup(NonHanUserGroup group);
	
	public NonHanUserGroup getNonHanUserGroup(Long nonHanGroupId);	
	
	public List<NonHanUser> getNonHanUserGroupMembers(Long nonHanGroupId);
	
	public Integer getNonHanUserGroupMembersCount(Long nonHanGroupId);
	
	public List<NonHanUser> getNonHanUserGroupMembers(Long nonHanGroupId, int startIndex, int maxResults);
	
	public List<NonHanUserGroup> getAllNonHanUserGroup();
	
	public List<NonHanUserGroup> getNonHanUserGroups(int startIndex, int maxResults);
	
	public Integer getNonHanUserGroupCount();
	
	public Integer getNonHanUserGroupCount(String groupName,
			String groupDescription);
	
	public List<NonHanUserGroup> searchNonHanUserGroup(String groupName,
			String groupDescription) ;
	
	public List<NonHanUserGroup> searchNonHanUserGroup(String groupName,
			String groupDescription, int startIndex, int maxResults);
	
	public void registerUploadBatchRequest(NonHanUserGroup group) throws BatchException;
	
	public void enqueueBatchUploadRequest(Long groupId) throws BatchException;
	
	public void processUploadBatch(NonHanUserGroup group) throws BatchException;
}

