package org.cdph.han.batch.dao.jdbc;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.sql.DataSource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.batch.dao.NonHanUserDao;
import org.cdph.han.persistence.NonHanUser;
import org.cdph.han.persistence.NonHanUserContactDevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Service;

//abey ...
//@Service("nonHanUserDao")
public class NonHanUserJdbcDao implements NonHanUserDao {

	/** Class logger */
	private static Log log = LogFactory.getLog(NonHanUserJdbcDao.class);

	private JdbcTemplate jdbcTemplate;

	//abey..
	//@Autowired
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@SuppressWarnings("unchecked")
	public void batchUpdate(List<NonHanUser> users) {
		log.info("batchUpdate(nonhanusers)");

		if (users == null || users.size() == 0) {
			log.warn("User list was empty. Exiting...");
			return;
		}

		// Insert Non Han Users
		users = insertNonHanUsers(users);
		
		List<NonHanUserContactDevice> devices = new ArrayList<NonHanUserContactDevice>();
		
		//Update the devices user id
		for (NonHanUser nonHanUser : users) {
			for (NonHanUserContactDevice device : nonHanUser.getNonHanContactDevices()) {
				device.setNonHanUser(nonHanUser);
			}
			devices.addAll((Collection<NonHanUserContactDevice>) nonHanUser.getNonHanContactDevices());
		}

		if(devices == null || devices.size() <=0){
			log.warn("devices is empty");
		}
		
		// Insert Contact device Preferences
		insertNonHanUserContactDevices(devices);
		

	}

	@SuppressWarnings("unchecked")
	private List<NonHanUser> insertNonHanUsers(final List<NonHanUser> users) {
		log.info("insertNonHanUsers...");		
		//TODO assign primary keys
		return (List<NonHanUser>) jdbcTemplate.execute(
				new PreparedStatementCreator(){

					public PreparedStatement createPreparedStatement(
							Connection con) throws SQLException {
						log.debug("In insertNonHanUsers PreparedStatementCreator.createPreparedStatement... ");
						String SQL = "INSERT INTO non_han_user ("
							+ "id,firstName,lastName,middleInitial,addressLine1,addressLine2,"
							+ "city, state, zip, non_han_group_id) VALUES(" 
							+ "?,?,?,?,?,?,?,?,?,?)";
						
						PreparedStatement ps =
				                con.prepareStatement(SQL);
						return ps;
					}
					
				}
				
				,
				new PreparedStatementCallback(){

					public Object doInPreparedStatement(PreparedStatement ps)
							throws SQLException, DataAccessException {
						log.debug("In insertNonHanUsers PreparedStatementCreator.doInPreparedStatement... ");
						long t1 = System.currentTimeMillis();
						
						if(users == null){
							log.warn("List of users is empty. Nothing will be inserted to DB...");
							return null;							
						}
						
						List<NonHanUser> updatedUsers = (List<NonHanUser>) ((ArrayList)users).clone();
						
						for (NonHanUser nonHanUser : updatedUsers) {
							ps.setLong(1, nonHanUser.getId());
				            ps.setString(2, nonHanUser.getFirstName());
							ps.setString(3, nonHanUser.getLastName());
							ps.setString(4, nonHanUser.getMiddleInitial());
							ps.setString(5, nonHanUser.getAddressLine1());
							ps.setString(6, nonHanUser.getAddressLine2());
							ps.setString(7, nonHanUser.getCity());
							ps.setString(8, nonHanUser.getState());
							ps.setString(9, nonHanUser.getZip());
							ps.setLong(10,  nonHanUser.getNonHanGroup().getId());

							ps.addBatch();
						} 
						ps.executeBatch();
						
						long t2 = System.currentTimeMillis();
						log.info("insertNonHanUsers, Time in milliseconds: " + (t2-t1));
			            return updatedUsers;
					}
					
					
				});

	}

	private void insertNonHanUserContactDevices(final List<NonHanUserContactDevice> devices) {
		log.info("insertNonHanUserContactDevices(devices)...");
		/*
		 * CREATE TABLE `lportal`.`non_han_user_contact_device` ( `id`
		 * bigint(20) NOT NULL, `contactDevice` varchar(100) NOT NULL,
		 * `contacType` int(11) NOT NULL, `priority` int(11) NOT NULL,
		 * `non_han_user_id` bigint(20) default NULL, PRIMARY KEY (`id`), KEY
		 * `FKF985822DCA6265C1` (`id`), KEY `FKF985822DF2FC5899`
		 * (`non_han_user_id`) ) ENGINE=MyISAM DEFAULT CHARSET=utf8
		 */
		if(devices == null || devices.size() == 0){
			log.warn("Devices list is empty");
			return;
		}
		
		jdbcTemplate.execute(
				new PreparedStatementCreator(){

					public PreparedStatement createPreparedStatement(
							Connection con) throws SQLException {
						log.debug("In insertNonHanUserContactDevices PreparedStatementCreator.createPreparedStatement... ");
						
						String SQL = "INSERT INTO non_han_user_contact_device ("
							+ "id,contactDevice,contacType,priority,non_han_user_id) "
							+ "VALUES(?,?,?,?,?)";
						
						PreparedStatement ps =
				                con.prepareStatement(SQL);
						return ps;
					}
					
				}
				
				,
				new PreparedStatementCallback(){

					public Object doInPreparedStatement(PreparedStatement ps)
							throws SQLException, DataAccessException {
						log.debug("In insertNonHanUserContactDevices PreparedStatementCallback.doInPreparedStatement... ");
						//ASSIGN PRIMARY KEYS
						long t1 = System.currentTimeMillis();						
						
						for (NonHanUserContactDevice device : devices) {

							ps.setLong(1, device.getId());			
							ps.setString(2, device.getContactDevice());
							ps.setInt(3, device.getContactType().ordinal());
							ps.setInt(4, device.getPriority());
							ps.setLong(5, device.getNonHanUser().getId());
							
							ps.addBatch();
						}
						ps.executeBatch();
						
						long t2 = System.currentTimeMillis();
						log.info("insertNonHanUserContactDevices, Time in milliseconds: " + (t2-t1));
						
			            return null;
					}
				});
	}
	
	/**
	 * Method needed to obtain primary keys.
	 * @param sequence_name in the hibernate_sequences DB table
	 * @return next id for that sequence_name
	 */
	public Long getNextSequenceID(String sequenceName){
	
		String nextIdSQL = "SELECT sequence_next_hi_value FROM hibernate_sequences " +
				"WHERE sequence_name=? FOR UPDATE";
		
		String updateCountSQL = "UPDATE hibernate_sequences SET sequence_next_hi_value=? " +
				"WHERE sequence_name=?";
		
		int id = jdbcTemplate.queryForObject(nextIdSQL, new Object[]{sequenceName}, Integer.class);
		
		jdbcTemplate.update(updateCountSQL, new Object[]{new Long(id+1), sequenceName});
		
		return new Long(id);
	
	}

}
