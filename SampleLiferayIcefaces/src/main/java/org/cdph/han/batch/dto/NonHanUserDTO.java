package org.cdph.han.batch.dto;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.batch.service.BatchService;
import org.cdph.han.dto.LazyDataModel;
import org.cdph.han.persistence.NonHanUser;

public class NonHanUserDTO extends LazyDataModel<NonHanUser> {
	
	/** Class logger */
	private static final Log log = LogFactory
			.getLog(NonHanUserDTO.class);

	private BatchService service;

	private Long nonHanGroupId;
	
	public NonHanUserDTO(BatchService service) {
		this.service = service;
	}

	public int countRows() {
		
		if(nonHanGroupId == null) return 0;
		
		Integer members = service.getNonHanUserGroupMembersCount(nonHanGroupId);		
		
		return members;
	}

	@Override
	public List<NonHanUser> findRows(int startRow, int finishRow) {
		return service.getNonHanUserGroupMembers(nonHanGroupId, startRow,
				(finishRow - startRow) + 1);
	}
	
	public Long getNonHanGroupId() {
		return nonHanGroupId;
	}

	public void setNonHanGroupId(Long nonHanGroupId) {
		this.nonHanGroupId = nonHanGroupId;
	}
}
