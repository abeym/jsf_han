package org.cdph.han.batch.validator;

import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.batch.BatchException;

public class BatchUploadLineValidator {

	/** Class logger */
	private static Log log = LogFactory.getLog(BatchUploadLineValidator.class);

	/*
	 * private final String LINE_FORMAT = "^" +
	 * "((\"(\\w|\\-|\\.|\\@|\\u0020)+\"\t)|((\\w|\\-|\\.|\\@|\\u0020)+\t)){13}"
	 * + "((\"(\\w|\\-|\\.|\\@|\\u0020)+\")|((\\w|\\-|\\.|\\@|\\u0020)+))" +
	 * "$";
	 */

	private final static String LINE_FORMAT = // "^"
	"(.*(\t)?){3,14}";
	// + "$";

	private final static String EMAIL = "([a-zA-Z0-9\\_\\-\\.]+)@"
			+ "(([0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$";

	/** phone number in form of xxx-xxx-xxxx */
	private final static String PHONE_NUM = "\\d{3}[-]?\\d{3}[-]?\\d{4}";

	private final static String ZIP_CODE = "\\d{5}";

	private final static Pattern lineMask = Pattern.compile(LINE_FORMAT);
	private final static Pattern emailMask = Pattern.compile(EMAIL);
	private final static Pattern phoneMask = Pattern.compile(PHONE_NUM);
	private final static Pattern zipMask = Pattern.compile(ZIP_CODE);
	
	
	public synchronized static void validateLine(String line)
			throws BatchException {

		Matcher lineMatcher = null;
		Matcher emailMatcher = null;
		Matcher phoneMatcher = null;
		Matcher zipMatcher = null;

		log.debug(line);

		// Validate the line REGEX
		lineMatcher = lineMask.matcher(line);

		if (!lineMatcher.matches()) {
			throw new BatchException(
					"Line content does not match format. "
							+ "Either it is not in TAB delimited format or it doesn't have the required fields."
							+ "Required fields are Last Name, First Name and at least one Contact Device "
							+ "(Work Phone, Work Email, Cell Phone, Home Telephone or Home Email)");
		}

		// Parse the line
		/*
		 * "Last Name" "First Name" "Middle Initial" "Work Phone"
		 * "Work Extension" "Work E-mail" "Cell Phone" "Address Line 1"
		 * "Address Line 2" "City" "State" "Zip" "Home Telephone"
		 * "Personal E-mail"
		 */
		
		String[] fieldArray = line.split("\\t",-1);
		//StringTokenizer st = new StringTokenizer(line, "\t");
		
		if(log.isDebugEnabled()){
			log.debug("Number of tokens: " + (fieldArray != null ? (fieldArray.length + "") : "0"));
		}
		
		if(fieldArray == null || fieldArray.length != 14){
			throw new BatchException("Line does not contain enough fields (Min 3 Max 14).");
		}
		
		
		boolean contactDevicePresent = false;

		// Last Name (100)
		String lastName = fieldArray[0];
		
		if(emptyString(lastName)){
			throw new BatchException("Last Name is a required field.");
		}
		if(lastName.trim().length() > 100){
			throw new BatchException("Last Name exceeds maximum length (100 characters).");
		}

		// First Name (100)
		String firstName = fieldArray[1];
		
		if(emptyString(firstName)){
			throw new BatchException("First Name is a required field.");
		}
		if(firstName.trim().length() > 100){
			throw new BatchException("First Name exceeds maximum length (100 characters).");
		}			

		// Middle Initial (1)
		String middleInitial = fieldArray[2];
		
		if(!emptyString(middleInitial) &&
				middleInitial.length() > 1){
			throw new BatchException("Middle Initial exceeds maximum length (1 character).");
		}

		// Work Phone (100)
		String workPhone =fieldArray[3];
		
		if(!emptyString(workPhone)){			
			phoneMatcher = phoneMask.matcher(workPhone);
			if (!phoneMatcher.matches()) {			
				throw new BatchException("Invalid Work Phone format. Valid formats are 123-456-7890 or 1234567890.");
			}			
			contactDevicePresent = true;
		}
		

		// Work Extension (100)
		String workExtension = fieldArray[4];
		
		if(!emptyString(workExtension) &&
				workExtension.length() > 100){
			throw new BatchException("Work Extension exceeds maximum length (100 characters).");
		}
		
		//Work E-mail (100)
		String workEmail = fieldArray[5];
		
		if(!emptyString(workEmail)){		
			
			if(workEmail.length() > 100){
				throw new BatchException("Work Email exceeds maximum length (100 characters).");
			}
			
			emailMatcher = emailMask.matcher(workEmail);
			if (!emailMatcher.matches()) {			
				throw new BatchException("Invalid Work Email format.");
			}			
			contactDevicePresent = true;
		}
		
		//Cell Phone (100)
		String cellPhone = fieldArray[6];
		
		if(!emptyString(cellPhone)){			
			phoneMatcher = phoneMask.matcher(cellPhone);
			if (!phoneMatcher.matches()) {			
				throw new BatchException("Invalid Cell Phone format. Valid formats are 123-456-7890 or 1234567890.");
			}			
			contactDevicePresent = true;
		}
		
		//Address Line 1 (250)
		String addressLine = fieldArray[7];
		
		if(!emptyString(addressLine) &&
				addressLine.length() > 250){
			throw new BatchException("Addres Line exceeds maximum length (250 characters).");
		}

		//Address Line 2 (250)
		String addressLine2 = fieldArray[8];
		
		if(!emptyString(addressLine2) &&
				addressLine2.length() > 250){
			throw new BatchException("Addres Line 2 exceeds maximum length (250 characters).");
		}

		// City (100)
		String city = fieldArray[9];
		
		if(!emptyString(city) &&
				city.length() > 100){
			throw new BatchException("City exceeds maximum length (100 characters).");
		}

		// State (50)
		String state = fieldArray[10];
		
		if(!emptyString(state) &&
				state.length() > 50){
			throw new BatchException("Addres Line exceeds maximum length (100 characters).");
		}

		// Zip (10)
		String zipCode = fieldArray[11];
		
		if(!emptyString(zipCode) &&
				zipCode.length() > 10){
			throw new BatchException("Addres Line exceeds maximum length (100 characters).");
		}else if(!emptyString(zipCode)){
			zipMatcher = zipMask.matcher(zipCode);
			if(!zipMatcher.matches()){
				throw new BatchException("Invalid Zip Code format.");
			}
		}

		// Home Telephone (100)
		String personalPhone = fieldArray[12];
		
		if(!emptyString(personalPhone)){			
			phoneMatcher = phoneMask.matcher(personalPhone);
			if (!phoneMatcher.matches()) {			
				throw new BatchException("Invalid Personal Phone format. Valid formats are 123-456-7890 or 1234567890.");
			}			
			contactDevicePresent = true;
		}

		// Personal E-mail (100)
		String personalEmail = fieldArray[13];
		
		if(!emptyString(personalEmail)){
			
			if(personalEmail.length() > 100){
				throw new BatchException("Personal Email exceeds maximum length (100 characters).");
			}
			
			emailMatcher = emailMask.matcher(personalEmail);
			if (!emailMatcher.matches()) {			
				throw new BatchException("Invalid Personal Email format.");
			}			
			contactDevicePresent = true;
		}
		
		if(!contactDevicePresent){
			throw new BatchException("At least one contact device needs to be present." +
					"Contact devices are Work Phone, Work Email, Cell Phone, Home Telephone or Home Email.");
		}

	}
	
	private static boolean emptyString(String value){
		return (value == null || value.trim().length() == 0);
	}

}
