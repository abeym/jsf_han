package org.cdph.han.batch.dao;

import java.util.List;

import org.cdph.han.persistence.NonHanUser;

public interface NonHanUserDao {

	public void batchUpdate(final List<NonHanUser> users);
	
	public Long getNextSequenceID(String sequenceName);
}
