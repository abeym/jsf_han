package org.cdph.han.custgroups.services;

import java.util.List;

import org.cdph.han.custgroups.dto.RecipientTO;

/**
 * This interface defines the behavior needed to save and update groups.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface GroupManagementService {

	/**
	 * The implementation of this method must create a custom user group in the database
	 * @param userId The Id of the user inserting the new group
	 * @param name The name of the group
	 * @param recipients The recipients of the group
	 * @return the id of the new group
	 */
	long saveGroup (final long userId, final String name, final List<RecipientTO> recipients);

	/**
	 * The implementation of this method must update the group information
	 * @param userId the id of the user modifying the group
	 * @param groupId the id of the group to modify
	 * @param recipients the updated recipients
	 */
	void updateGroup (final long userId, final long groupId, final List<RecipientTO> recipients);

	/**
	 * The implementation of this method must soft delete the given group after validating that the group can be
	 * deleted
	 * @param userId id of the user trying to delete the group
	 * @param groupId id of the group to delete
	 * @return result of the operation
	 */
	DeleteResult deleteGroup (final long userId, final long groupId);

}
