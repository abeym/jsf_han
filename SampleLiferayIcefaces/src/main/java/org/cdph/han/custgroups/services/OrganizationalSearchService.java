package org.cdph.han.custgroups.services;

import java.util.List;

import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.KeyDescriptionTO;
import org.cdph.han.custgroups.dto.UserDisplayTO;

/**
 * This interface defines the required methods to implement for a service to search the organizational structure
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface OrganizationalSearchService {

	/**
	 * The implementation of this method must return all the available organization types
	 * @return empty array if no organization type is found or the organization types found
	 */
	KeyDescriptionTO[] getOrganizationTypes ();

	/**
	 * The implementation of this method must search for all the organizations that belong the the given
	 * organization type id, if the flag for only Chicago entities is set, only the organizations in the
	 * Chicago area must be listed
	 * @param orgTypeId ID of the organization type (parent organization id)
	 * @param onlyChicagoEntities flag for searching only Chicago based organizations
	 * @return empty array if no organization is found or the organizations belonging to the organization type
	 */
	KeyDescriptionTO[] getOrganizations (final long orgTypeId, final boolean onlyChicagoEntities);

	/**
	 * The implementation of this method must return all the area/fields associated to the given organization type
	 * @param orgTypeId the id of the organization type
	 * @return empty array if there are no area/fields associated to the organization type
	 */
	KeyDescriptionTO[] getAreaFields (final long orgTypeId);

	/**
	 * The implementation of this method must return all the roles associated to the given organization type
	 * @param orgTypeId the id of the organization type
	 * @return empty array if there are no roles associated to the organization type
	 */
	KeyDescriptionTO[] getRoles (final long orgTypeId);

	/**
	 * Retrieves all the users in the selected organization
	 * @param organizationId id of the organization
	 * @return list of users in the organization, empty if no user is in the organization
	 */
	List<UserDisplayTO> getUsersInOrganization (final long organizationId);

	/**
	 * The implementation of this method must use the provided parameters to filter users
	 * @param organizationIds IDs of the organizations to use as filters
	 * @param areaFieldIds IDs of the area fields to use as filters
	 * @param roleIds IDs of the roles to use as filters
	 * @return list of users matching the filters
	 */
	List<UserDisplayTO> filterUsers (final long[] organizationIds, final long[] areaFieldIds, final long[] roleIds);

	/**
	 * Retrieves all the users in the given organization type
	 * @param orgTypeId id of the organization type
	 * @return list of users in the organization type, empty if no user is in the organization type
	 */
	List<UserDisplayTO> getUsersByOrganizationType (final long orgTypeId);

	/**
	 * The implementation of this method must return all the users associated to the given bulk upload lists ids
	 * @param listIds ids of the bulk upload lists
	 * @return users found in the bulk upload lists, empty array if none
	 */
	UserDisplayTO[] searchUsersInBulkUpload (final List<Long> listIds);

	/**
	 * The implementation of this method must return all the users associated to the given group, if a member is
	 * different of a user the method should be able to search the given member for users
	 * @param groupIds ids of the groups to search
	 * @return users found in the groups, empty array if none
	 */
	UserDisplayTO[] searchUsersInGroup (final List<Long> groupIds);

	/**
	 * The implementation of this method should search for individuals by first name and/or last name
	 * @param firstName the individual first name
	 * @param lastName the individual last name
	 * @param chicagoOnly flag for searching chicago entities only
	 * @return found users or empty array
	 */
	UserDisplayTO[] searchIndividuals (final String firstName, final String lastName, final boolean chicagoOnly);

	/**
	 * The implementation of this method must search the names of the bulk upload lists, if the provided name is
	 * null or empty the method must return all the bulk upload list names
	 * @param nameFilter filter to use in the name
	 * @return the found list names
	 */
	GroupDisplayTO[] getBulkUploadNames (final String nameFilter);

}
