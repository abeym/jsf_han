package org.cdph.han.custgroups.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * Class representing a recipient in a custom group. The recipient can be a type of organization, organization,
 * area or field, role or user.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class RecipientTO {
	/** ID of the recipient */
	private long id;
	/** Name of the recipient */
	private String name;
	/** Type of recipient */
	private RecipientType type;
	/** If the recipient is selected in the GUI */
	private boolean selected;
	/** Flag to determine if the recipient was notified already */
	private boolean notified;
	/** List with the filter criteria */
	private List<FilterCriteriaTO> criteria;
	/** Flag for Chicago only entities */
	private boolean chicagoOnly;

	/**
	 * Empty constructor
	 */
	public RecipientTO () {
		criteria = new ArrayList<FilterCriteriaTO> ();
	}

	/**
	 * Constructor
	 * @param id the id of the recipient
	 * @param name the name of the recipient
	 * @param type the type of the recipient
	 */
	public RecipientTO (final long id, final String name, final RecipientType type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof RecipientTO) {
			final RecipientTO other = (RecipientTO) obj;
			equal = (other.getId () == id) && (other.getType ().equals (type));
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @return the typeMessage
	 */
	public String getTypeMessage () {
		return type.TypeMsgId;
	}

	/**
	 * @return the type
	 */
	public RecipientType getType () {
		return type;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (final long id) {
		this.id = id;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (final String name) {
		this.name = name;
	}

	/**
	 * @param type the type to set
	 */
	public void setType (final RecipientType type) {
		this.type = type;
	}

	/**
	 * @return the selected
	 */
	public boolean isSelected () {
		return selected;
	}

	/**
	 * @param selected the selected to set
	 */
	public void setSelected (final boolean selected) {
		this.selected = selected;
	}

	/**
	 * @return the notified
	 */
	public boolean isNotified () {
		return notified;
	}

	/**
	 * @param notified the notified to set
	 */
	public void setNotified (boolean notified) {
		this.notified = notified;
	}

	/**
	 * @return the criteria
	 */
	public List<FilterCriteriaTO> getCriteria () {
		return criteria;
	}

	/**
	 * @return the chicagoOnly
	 */
	public boolean isChicagoOnly () {
		return chicagoOnly;
	}

	/**
	 * @param chicagoOnly the chicagoOnly to set
	 */
	public void setChicagoOnly (boolean chicagoOnly) {
		this.chicagoOnly = chicagoOnly;
	}

}
