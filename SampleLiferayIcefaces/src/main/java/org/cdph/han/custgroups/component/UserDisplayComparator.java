package org.cdph.han.custgroups.component;

import java.util.Comparator;

import org.cdph.han.custgroups.dto.UserDisplayTO;

/**
 * Comparator used to sort the results in the personal groups using one of the columns
 * @author Oswaldo
 *
 */
public class UserDisplayComparator implements Comparator<UserDisplayTO> {
	/** Column to use for sorting */
	private SortColumn sortColumn;
	/** Ascending or descending order */
	private boolean ascending;

	/**
	 * Default constructor
	 * @param sortColumn to use for sorting
	 */
	public UserDisplayComparator (final SortColumn sortColumn, final boolean ascending) {
		this.sortColumn = sortColumn;
		this.ascending = ascending;
		if (this.sortColumn == null) {
			this.sortColumn = SortColumn.ID;
		}
	}

	/* (non-Javadoc)
	 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
	 */
	@Override
	public int compare (final UserDisplayTO user1, final UserDisplayTO user2) {
		int result = 0;
		if (SortColumn.ID.equals (sortColumn)) {
			result = ((Long) user1.getId ()).compareTo (user2.getId ());
		} else if (SortColumn.FIRST_NAME.equals (sortColumn)) {
			result = user1.getFirstName ().compareToIgnoreCase (user2.getFirstName ());
		} else if (SortColumn.LAST_NAME.equals (sortColumn)) {
			result = user1.getLastName ().compareToIgnoreCase (user2.getLastName ());
		} else if (SortColumn.ORGANIZATION.equals (sortColumn)) {
			result = user1.getOrganization ().compareTo (user2.getOrganization ());
		} else if (SortColumn.ROLE.equals (sortColumn)) {
			result = user1.getRole ().compareTo (user2.getRole ());
		}
		return ascending ? result : result * -1;
	}

	/**
	 * Enumeration listing the available columns to use for sorting
	 * @author Horacio Oswaldo Ferro
	 * @version 1.0
	 */
	public enum SortColumn {
		ID, FIRST_NAME, LAST_NAME, ORGANIZATION, ROLE
	}
}
