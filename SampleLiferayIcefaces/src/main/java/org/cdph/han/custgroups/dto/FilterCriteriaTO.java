package org.cdph.han.custgroups.dto;

/**
 * Transfer object for creating a filter criteria
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class FilterCriteriaTO {
	/** Organization type ID */
	private long organizationTypeId;
	/** Organization ID */
	private Long organizationId;
	/** Role ID */
	private Long roleId;
	/** Area/Field ID */
	private Long areaFieldId;

	/**
	 * @return the organizationTypeId
	 */
	public long getOrganizationTypeId () {
		return organizationTypeId;
	}

	/**
	 * @param organizationTypeId the organizationTypeId to set
	 */
	public void setOrganizationTypeId (long organizationTypeId) {
		this.organizationTypeId = organizationTypeId;
	}

	/**
	 * @return the organizationId
	 */
	public Long getOrganizationId () {
		return organizationId;
	}

	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId (Long organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * @return the roleId
	 */
	public Long getRoleId () {
		return roleId;
	}

	/**
	 * @param roleId the roleId to set
	 */
	public void setRoleId (Long roleId) {
		this.roleId = roleId;
	}

	/**
	 * @return the areaFieldId
	 */
	public Long getAreaFieldId () {
		return areaFieldId;
	}

	/**
	 * @param areaFieldId the areaFieldId to set
	 */
	public void setAreaFieldId (Long areaFieldId) {
		this.areaFieldId = areaFieldId;
	}

}
