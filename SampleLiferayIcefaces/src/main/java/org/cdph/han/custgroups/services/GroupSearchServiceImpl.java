package org.cdph.han.custgroups.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.cdph.han.custgroups.dto.FilterCriteriaTO;
import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.GroupTO;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.persistence.CustomGroupCriteria;
import org.cdph.han.persistence.CustomGroupMember;
import org.cdph.han.persistence.UserCustomGroup;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the GroupSearchService using JPA
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("groupSearchService")
public class GroupSearchServiceImpl implements GroupSearchService {
	/** Persistence context */
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#searchMyGroups(long)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public GroupDisplayTO[] searchMyGroups (final long userId) {
		final Query q = entityManager.createNamedQuery ("UserCustomGroup.FindByUser");
		q.setParameter ("userId", userId);
		final List<?> foundGroups = q.getResultList ();
		final List<GroupDisplayTO> groups = new ArrayList<GroupDisplayTO> (foundGroups.size ());
		UserCustomGroup current;
		GroupDisplayTO group;
		for (Object o : foundGroups) {
			current = (UserCustomGroup) o;
			group = new GroupDisplayTO ();
			group.setCreatedBy (current.getUserData ().getContact ().getFirstName ()
					+ " " + current.getUserData ().getContact ().getLastName ());
			group.setDateCreated (current.getCreatedDate ());
			group.setId (current.getGroupId ());
			group.setName (current.getName ());
			if (current.getModifiedBy () != null) {
				group.setDateModified (current.getDateModified ());
				group.setModifiedBy (current.getModifiedBy ().getContact ().getFirstName ()
						+ " " + current.getModifiedBy ().getContact ().getLastName ());
			}
			groups.add (group);
		}
		return groups.toArray (new GroupDisplayTO[groups.size ()]);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#isNameAvailable(long, java.lang.String)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public boolean isNameAvailable (final long userId, final String name) {
		final Query q = entityManager.createNamedQuery ("UserCustomGroup.FindByUserName");
		q.setParameter ("userId", userId);
		q.setParameter ("name", name.toUpperCase ());
		final List<?> result = q.getResultList ();
		return result.isEmpty ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#searchUserGroups(java.lang.String)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public GroupDisplayTO[] searchUserGroups (String name) {
		final Query q = entityManager.createQuery ((name != null && name.length () > 0) ?
				"select g from UserCustomGroup g where upper (g.name) like '" + name.toUpperCase ()
				+ "%' and g.deleted = false order by g.name"
				: "select g from UserCustomGroup g where g.deleted = false order by g.name");
		final List<?> customGroups = q.getResultList ();
		final List<GroupDisplayTO> foundCustomGroups = new ArrayList<GroupDisplayTO> ();
		UserCustomGroup customGroup;
		for (Object o : customGroups) {
			customGroup = (UserCustomGroup) o;
			if (customGroup.getModifiedBy () != null) {
				foundCustomGroups.add (new GroupDisplayTO (customGroup.getGroupId (),
						customGroup.getName (),
						customGroup.getUserData ().getContact ().getFirstName () +
						" " + customGroup.getUserData ().getContact ().getLastName (),
						customGroup.getCreatedDate (),
						customGroup.getModifiedBy ().getContact ().getFirstName () + " " +
						customGroup.getModifiedBy ().getContact ().getLastName (),
						customGroup.getDateModified ()));
			} else {
				foundCustomGroups.add (new GroupDisplayTO (customGroup.getGroupId (), customGroup.getName (),
						customGroup.getUserData ().getContact ().getFirstName () +
						" " + customGroup.getUserData ().getContact ().getLastName (),
						customGroup.getCreatedDate (), "", null));
			}
		}
		return foundCustomGroups.toArray (new GroupDisplayTO[foundCustomGroups.size ()]);
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.custgroups.services.GroupSearchService#loadGroup(long)
	 */
	@Override
	//abey ...@Transactional (readOnly = true)
	public GroupTO loadGroup (long id) {
		final UserCustomGroup group = entityManager.find (UserCustomGroup.class, id);
		final List<RecipientTO> recipients = new ArrayList<RecipientTO> ();
		for (CustomGroupMember member : group.getMembers ()) {
			if (member.getAreaField () != null) {
				recipients.add (new RecipientTO (member.getAreaField ().getAreaFieldId (),
						member.getAreaField ().getDescription (), RecipientType.AREA_FIELD));
			} else if (member.getChildGroup () != null) {
				recipients.add (new RecipientTO (member.getChildGroup ().getGroupId (),
						member.getChildGroup ().getName (), RecipientType.GROUP));
			} else if (member.getNonHanUser () != null) {
				recipients.add (new RecipientTO (member.getNonHanUser ().getId (),
						member.getNonHanUser ().getFirstName () + " " + member.getNonHanUser ().getLastName (),
						RecipientType.NON_HAN_USER));
			} else if (member.getNonHanUserGroup () != null) {
				recipients.add (new RecipientTO (member.getNonHanUserGroup ().getId (),
						member.getNonHanUserGroup ().getName (), RecipientType.NON_HAN_GROUP));
			} else if (member.getOrganization () != null) {
				recipients.add (new RecipientTO (member.getOrganization ().getOrganizationId (),
						member.getOrganization ().getName (), member.getOrganization ().getParentOrganizationId () == 0 ?
								RecipientType.ORGANIZATION_TYPE : RecipientType.ORGANIZATION));
			} else if (member.getRole () != null) {
				recipients.add (new RecipientTO (member.getRole ().getRoleId (),
						member.getRole ().getName (), RecipientType.ROLE));
			} else if (member.getUserData () != null) {
				recipients.add (new RecipientTO (member.getUserData ().getUserId (),
						member.getUserData ().getContact ().getFirstName () + " " +
						member.getUserData ().getContact ().getLastName (), RecipientType.USER));
			} else if (member.getCriterias () != null && !member.getCriterias ().isEmpty ()) {
				StringBuilder organization = new StringBuilder ();
				StringBuilder role = new StringBuilder ();
				StringBuilder areaField = new StringBuilder ();
				String organizationType = null;
				FilterCriteriaTO criteria = null;
				RecipientTO recipientTO = new RecipientTO ();
				for (CustomGroupCriteria dbCriteria : member.getCriterias ()) {
					criteria = new FilterCriteriaTO ();
					criteria.setOrganizationTypeId (dbCriteria.getOrganizationType ().getOrganizationId ());
					if (dbCriteria.getAreaField () != null) {
						areaField.append (dbCriteria.getAreaField ().getDescription ()).append (", ");
						criteria.setAreaFieldId ((long) dbCriteria.getAreaField ().getAreaFieldId ());
					}
					if (dbCriteria.getOrganization () != null) {
						organization.append (dbCriteria.getOrganization ().getName ()).append (", ");
						criteria.setOrganizationId (dbCriteria.getOrganization ().getOrganizationId ());
					}
					if (dbCriteria.getRole () != null) {
						role.append (dbCriteria.getRole ().getName ()).append (", ");
						criteria.setRoleId (dbCriteria.getRole ().getRoleId ());
					}
					if (organizationType == null) {
						organizationType = dbCriteria.getOrganizationType ().getName ();
					}
					recipientTO.getCriteria ().add (criteria);
				}
				StringBuilder caption = new StringBuilder ();
				caption.append (organizationType).append (" / ");
				if (organization.length () > 0) {
					caption.append (organization.substring (0, organization.lastIndexOf (", ")));
				}
				caption.append (" / ");
				if (areaField.length () > 0) {
					caption.append (areaField.substring (0, areaField.lastIndexOf (", ")));
				}
				caption.append (" / ");
				if (role.length () > 0) {
					caption.append (role.substring (0, role.lastIndexOf (", ")));
				}
				recipientTO.setName (caption.toString ());
				recipientTO.setType (RecipientType.CRITERIA);
				recipientTO.setChicagoOnly (member.isChicagoOnly ());
				recipientTO.setId (0);
				recipients.add (recipientTO);
			}
		}
		return new GroupTO (group.getGroupId (), group.getName (), recipients);
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
