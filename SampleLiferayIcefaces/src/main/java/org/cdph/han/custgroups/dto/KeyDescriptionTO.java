package org.cdph.han.custgroups.dto;

/**
 * Utility object to transfer the key and the description of a given organizational unit
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class KeyDescriptionTO {
	/** The unit key */
	private long key;
	/** The unit description */
	private String description;
	/** Organization type id */
	private Long organizationTypeId;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equals = false;
		if (obj != null && obj instanceof KeyDescriptionTO) {
			final KeyDescriptionTO other = (KeyDescriptionTO) obj;
			equals = key == other.getKey ();
			equals = equals && (description == null ? other.getDescription () == null
					: description.equals (other.getDescription ()));
		}
		return equals;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (key).hashCode () + description != null ? description.hashCode () : 0;
	}

	/**
	 * @return the key
	 */
	public long getKey () {
		return key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey (long key) {
		this.key = key;
	}

	/**
	 * @return the description
	 */
	public String getDescription () {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription (String description) {
		this.description = description;
	}

	/**
	 * @return the organizationTypeId
	 */
	public Long getOrganizationTypeId () {
		return organizationTypeId;
	}

	/**
	 * @param organizationTypeId the organizationTypeId to set
	 */
	public void setOrganizationTypeId (Long organizationTypeId) {
		this.organizationTypeId = organizationTypeId;
	}

}
