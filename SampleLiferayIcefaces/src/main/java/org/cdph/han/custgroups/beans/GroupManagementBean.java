package org.cdph.han.custgroups.beans;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.portlet.PortletRequest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.custgroups.component.UserDisplayComparator;
import org.cdph.han.custgroups.component.UserDisplayComparator.SortColumn;
import org.cdph.han.custgroups.dto.FilterCriteriaTO;
import org.cdph.han.custgroups.dto.GroupDisplayTO;
import org.cdph.han.custgroups.dto.GroupTO;
import org.cdph.han.custgroups.dto.KeyDescriptionTO;
import org.cdph.han.custgroups.dto.RecipientTO;
import org.cdph.han.custgroups.dto.RecipientType;
import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.custgroups.services.DeleteResult;
import org.cdph.han.custgroups.services.GroupManagementService;
import org.cdph.han.custgroups.services.GroupSearchService;
import org.cdph.han.custgroups.services.OrganizationalSearchService;

import com.icesoft.faces.component.ext.RowSelectorEvent;

/**
 * Backed bean for managing groups. This bean creates, deletes and modifies the user custom groups.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class GroupManagementBean implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -1019220321883974172L;
	/** Class logger */
	private static final Log log = LogFactory.getLog (GroupManagementBean.class);
	/** Available found users */
	private List<UserDisplayTO> foundUsers;
	/** Available organization types */
	private SelectItem[] organizationTypes;
	/** Currently selected organization types */
	private Long selectedOrganizationTypes;
	/** Available organizations filtered by the selected organization types */
	private SelectItem[] organizations;
	/** Currently selected organizations */
	private long[] selectedOrganizations;
	/** Available areas/fields filtered by the selected organization types */
	private SelectItem[] areaFields;
	/** Currently selected areas/fields */
	private long[] selectedAreaFields;
	/** Available roles filtered by the selected organization types */
	private SelectItem[] roles;
	/** Currently selected roles */
	private long[] selectedRoles;
	/** Selected recipients */
	private List<RecipientTO> recipients;
	/** First name used as a filter to search users */
	private String firstNameFilter;
	/** Last name used as a filter to search users */
	private String lastNameFilter;
	/** Bulk list name used as filter */
	private String bulkListNameFilter;
	/** Array of selected bulk upload lists */
	private Long[] selectedBulkUploadLists;
	/** Array of found lists */
	private GroupDisplayTO[] bulkUploadList;
	/** Name of the group to use as a filter */
	private String groupNameFilter;
	/** Found user groups */
	private GroupDisplayTO[] groupList;
	/** Name of the group */
	private String groupName;
	/** Selected group id */
	private Long groupId;
	/** Search only for Chicago Entities */
	private boolean onlySelectChicago;
	/** Search only Chicago Users */
	private boolean searchOnlyChicagoUsers;
	/** Flag for collapsing the group search panel */
	private boolean collapsedGroupSearch;
	/** Flag for collapsing the criteria search panel */
	private boolean collapsedCriteriaSearch;
	/** Flag for collapsing the individual search panel */
	private boolean collapsedIndividualSearch;
	/** Flag for collapsing the bulk search panel */
	private boolean collapsedBulkSearch;
	/** Service for performing group searchs */
	private GroupSearchService groupSearchService;
	/** Service for performing search on the organizational structure */
	private OrganizationalSearchService organizationalSearchService;
	/** Service used to manage the custom groups */
	private GroupManagementService groupManagementService;
	/** The groupId to delete */
	private long groupIdToDelete;
	/** Flag for displaying the delete confirmation */
	private boolean deleteConfirmationVisible;	
	/** Flag for displaying the edit confirmation */
	private boolean editConfirmationVisible;
	/** Original name of the group (if its changed the group is saved as a new one) */
	private String originalName;
	/** Sets the ascending or descending order in the found users */
	private boolean ascending = true;
	/** Column to use for sorting */
	private String sortColumn = "lastName";
	/** Old ascending order */
	private boolean oldAscending = false;
	/** Old sort column */
	private String oldSortColumn;

	/**
	 * The constructor retrieves the available organization types
	 */
	public void init () {
		log.info ("Init...");
		final KeyDescriptionTO[] orgTypes = organizationalSearchService.getOrganizationTypes ();
		organizationTypes = new SelectItem[orgTypes.length];
		for (int i = 0; i < orgTypes.length; i++) {
			organizationTypes[i] = new SelectItem (orgTypes[i].getKey (), orgTypes[i].getDescription ());
		}
		recipients = new ArrayList<RecipientTO> ();
		foundUsers = new ArrayList<UserDisplayTO> ();
	}

	/**
	 * Reloads the organizations, area/fields and roles when the organization type is changed
	 * @param event event generated by the value change
	 */
	public void organizationTypesChanged (final ValueChangeEvent event) {
		final Long newValue = (Long) event.getNewValue ();
		//if (!Arrays.equals (selectedOrganizationTypes, newValue)) {
		boolean different = selectedOrganizationTypes == null ? newValue != null :
			!selectedOrganizationTypes.equals (newValue);
		if (different) {
			final List<SelectItem> organizationsList = new ArrayList<SelectItem> ();
			final List<SelectItem> areaFieldsList = new ArrayList<SelectItem> ();
			final List<SelectItem> rolesList = new ArrayList<SelectItem> ();
			final List<UserDisplayTO> foundUsersList = new ArrayList<UserDisplayTO> ();
			if (newValue != null) {
				KeyDescriptionTO[] temp;
				temp = organizationalSearchService.getOrganizations (newValue, onlySelectChicago);
				for (KeyDescriptionTO keyDesc : temp) {
					organizationsList.add (new SelectItem (keyDesc.getKey (), keyDesc.getDescription ()));
					foundUsersList.addAll (organizationalSearchService.getUsersInOrganization (keyDesc.getKey ()));
				}
				temp = organizationalSearchService.getAreaFields (newValue);
				for (KeyDescriptionTO keyDesc : temp) {
					areaFieldsList.add (new SelectItem (keyDesc.getKey (), keyDesc.getDescription ()));
				}
				temp = organizationalSearchService.getRoles (newValue);
				for (KeyDescriptionTO keyDesc : temp) {
					rolesList.add (new SelectItem (keyDesc.getKey (), keyDesc.getDescription ()));
				}
			}
			foundUsers = foundUsersList;
			sort ();
			organizations = organizationsList.toArray (new SelectItem [organizationsList.size ()]);
			areaFields = areaFieldsList.toArray (new SelectItem [areaFieldsList.size ()]);
			roles = rolesList.toArray (new SelectItem [rolesList.size ()]);
			selectedOrganizationTypes = newValue;
		}
	}

	/**
	 * Value change listener invoked when an organization is selected or removed
	 * @param event ValueChangeEvent generated in the process
	 */
	public void organizationChanged (final ValueChangeEvent event) {
		try {
			long[] newOrganizationIds = (long[]) event.getNewValue ();
			if (!Arrays.equals (selectedOrganizations, newOrganizationIds)) {
				selectedOrganizations = newOrganizationIds;
				reloadUsers (newOrganizationIds, selectedAreaFields, selectedRoles);
			}
		} catch (Exception ex) {
			log.error ("Error updating the criteria filter.", ex);
		}
	}

	/**
	 * Value change listener invoked when an area or field are selected or removed
	 * @param event ValueChangeEvent generated in the process
	 */
	public void areaFieldChanged (final ValueChangeEvent event) {
		try {
			long[] areaFieldIds = (long[]) event.getNewValue ();
			if (!Arrays.equals (selectedAreaFields, areaFieldIds)) {
				selectedAreaFields = areaFieldIds;
				reloadUsers (selectedOrganizations, areaFieldIds, selectedRoles);
			}
		} catch (Exception ex) {
			log.error ("Error updating the criteria filter.", ex);
		}
	}

	/**
	 * Value change listener invoked when a role is selected or removed
	 * @param event ValueChangeEvent generated in the process
	 */
	public void roleChanged (final ValueChangeEvent event) {
		try {
			long[] roleIds = (long[]) event.getNewValue ();
			if (!Arrays.equals (selectedRoles, roleIds)) {
				selectedRoles = roleIds;
				reloadUsers (selectedOrganizations, selectedAreaFields, roleIds);
			}
		} catch (Exception ex) {
			log.error ("Error updating the criteria filter.", ex);
		}
	}

	/**
	 * Value change listener invoked when the user sets or unsets the chicago only entities.
	 * @param event ValueChangeEvent triggered by the user
	 */
	public void onlyChicagoChanged (final ValueChangeEvent event) {
		this.onlySelectChicago = (Boolean) event.getNewValue ();
	}

	/**
	 * Value change listener invoked when the users sets or unsets the chicago only users.
	 * @param event ValueChangeEvent triggered by the user
	 */
	public void searchOnlyChicagoUsersChanged (final ValueChangeEvent event) {
		this.searchOnlyChicagoUsers = (Boolean) event.getNewValue ();
	}

	/**
	 * The method reload the users using the provided parameters as filters
	 * @param organizationIds organization ids to use as filter
	 * @param areaFieldIds area/field ids to use as filter
	 * @param roleIds role ids to use as filter
	 */
	private void reloadUsers (final long[] organizationIds, final long[] areaFieldIds, final long[] roleIds) {
		List<UserDisplayTO> foundUsersList;
		if ((organizationIds != null && organizationIds.length > 0)
				|| (areaFieldIds != null && areaFieldIds.length > 0)
				|| (roleIds != null && roleIds.length > 0)) {
			foundUsersList = organizationalSearchService.filterUsers (organizationIds, areaFieldIds, roleIds);
		} else if (selectedOrganizationTypes != null) {
			foundUsersList = new ArrayList<UserDisplayTO> ();
			foundUsersList.addAll (organizationalSearchService.getUsersByOrganizationType (
						selectedOrganizationTypes));
		} else {
			// Empty the user results
			foundUsersList = new ArrayList<UserDisplayTO> ();
		}
		foundUsers = foundUsersList;
		sort ();
	}

	/**
	 * Action for adding a new organization type in the recipients list
	 * @return outcome of the operation
	 */
	public String addOrganizationType () {
		String outcome = "success";
		RecipientTO newRecipient;
		for (SelectItem select : organizationTypes) {
			if (select.getValue ().equals (selectedOrganizationTypes)) {
				newRecipient = new RecipientTO ((Long) select.getValue (), select.getLabel (),
						RecipientType.ORGANIZATION_TYPE);
				if (!recipients.contains (newRecipient)) {
					recipients.add (newRecipient);
				}
				break;
			}
		}
		return outcome;
	}

	/**
	 * Action for collapsing the criteria panel
	 * @return "success"
	 */
	public String toggleCriteria () {
		collapsedCriteriaSearch = true;
		return "success";
	}

	/**
	 * Action for collapsing the criteria panel
	 * @return "success"
	 */
	public String toggleIndividual () {
		collapsedIndividualSearch = true;
		return "success";
	}

	/**
	 * Action for adding a new organization to the recipients list
	 * @return outcome of the operation
	 */
	public String addOrganization () {
		String outcome = "success";
		RecipientTO newRecipient;
		for (long organizationId : selectedOrganizations) {
			for (SelectItem select : organizations) {
				if (select.getValue ().equals (organizationId)) {
					newRecipient = new RecipientTO ((Long) select.getValue (), select.getLabel (),
							RecipientType.ORGANIZATION);
					if (!recipients.contains (newRecipient)) {
						recipients.add (newRecipient);
					}
					break;
				}
			}
		}
		return outcome;
	}

	/**
	 * Action for adding a new organization to the recipients list
	 * @return outcome of the operation
	 */
	public String addAreaField () {
		String outcome = "success";
		RecipientTO newRecipient;
		for (long areaFieldId : selectedAreaFields) {
			for (SelectItem select : areaFields) {
				if (select.getValue ().equals (areaFieldId)) {
					newRecipient = new RecipientTO ((Long) select.getValue (), select.getLabel (),
							RecipientType.AREA_FIELD);
					if (!recipients.contains (newRecipient)) {
						recipients.add (newRecipient);
					}
					break;
				}
			}
		}
		return outcome;
	}

	/**
	 * Action for adding a new organization to the recipients list
	 * @return outcome of the operation
	 */
	public String addRole () {
		String outcome = "success";
		RecipientTO newRecipient;
		for (long roleId : selectedRoles) {
			for (SelectItem select : roles) {
				if (select.getValue ().equals (roleId)) {
					newRecipient = new RecipientTO ((Long) select.getValue (), select.getLabel (),
							RecipientType.ROLE);
					if (!recipients.contains (newRecipient)) {
						recipients.add (newRecipient);
					}
					break;
				}
			}
		}
		return outcome;
	}

	/**
	 * Adds a filter to the recipient list
	 * @return outcome of the operation
	 */
	public String addFilter () {
		String outcome = "success";
		RecipientTO newRecipient = new RecipientTO ();
		FilterCriteriaTO criteria;
		newRecipient.setName ("Filter Criteria");
		newRecipient.setType (RecipientType.CRITERIA);
		newRecipient.setChicagoOnly (onlySelectChicago);
		String organizationType = getValue (organizationTypes, selectedOrganizationTypes);
		StringBuilder organization = new StringBuilder ();
		StringBuilder areaField = new StringBuilder ();
		StringBuilder role = new StringBuilder ();
		if (selectedOrganizations != null && selectedOrganizations.length > 0) {
			if (selectedRoles != null && selectedRoles.length > 0) {
				if (selectedAreaFields != null && selectedAreaFields.length > 0) {
					for (long orgId : selectedOrganizations) {
						organization.append (getValue (organizations, orgId)).append (", ");
						for (long roleId : selectedRoles) {
							role.append (getValue (roles, roleId)).append (", ");
							for (long areaFieldId : selectedAreaFields) {
								areaField.append (getValue (areaFields, areaFieldId)).append (", ");
								criteria = new FilterCriteriaTO ();
								criteria.setAreaFieldId (areaFieldId);
								criteria.setOrganizationId (orgId);
								criteria.setOrganizationTypeId (selectedOrganizationTypes);
								criteria.setRoleId (roleId);
								newRecipient.getCriteria ().add (criteria);
							}
						}
					}
				} else {
					for (long orgId : selectedOrganizations) {
						organization.append (getValue (organizations, orgId)).append (", ");
						for (long roleId : selectedRoles) {
							role.append (getValue (roles, roleId)).append (", ");
							criteria = new FilterCriteriaTO ();
							criteria.setOrganizationId (orgId);
							criteria.setOrganizationTypeId (selectedOrganizationTypes);
							criteria.setRoleId (roleId);
							newRecipient.getCriteria ().add (criteria);
						}
					}
				}
			} else {
				if (selectedAreaFields != null && selectedAreaFields.length > 0) {
					for (long orgId : selectedOrganizations) {
						organization.append (getValue (organizations, orgId)).append (", ");
						for (long areaFieldId : selectedAreaFields) {
							areaField.append (getValue (areaFields, areaFieldId)).append (", ");
							criteria = new FilterCriteriaTO ();
							criteria.setAreaFieldId (areaFieldId);
							criteria.setOrganizationId (orgId);
							criteria.setOrganizationTypeId (selectedOrganizationTypes);
							newRecipient.getCriteria ().add (criteria);
						}
					}
				} else {
					for (long orgId : selectedOrganizations) {
						organization.append (getValue (organizations, orgId)).append (", ");
						criteria = new FilterCriteriaTO ();
						criteria.setOrganizationId (orgId);
						criteria.setOrganizationTypeId (selectedOrganizationTypes);
						newRecipient.getCriteria ().add (criteria);
					}
				}
			}
		} else {
			if (selectedRoles != null && selectedRoles.length > 0) {
				if (selectedAreaFields != null && selectedAreaFields.length > 0) {
					for (long roleId : selectedRoles) {
						role.append (getValue (roles, roleId)).append (", ");
						for (long areaFieldId : selectedAreaFields) {
							areaField.append (getValue (areaFields, areaFieldId)).append (", ");
							criteria = new FilterCriteriaTO ();
							criteria.setAreaFieldId (areaFieldId);
							criteria.setOrganizationTypeId (selectedOrganizationTypes);
							criteria.setRoleId (roleId);
							newRecipient.getCriteria ().add (criteria);
						}
					}
				} else {
					for (long roleId : selectedRoles) {
						role.append (getValue (roles, roleId)).append (", ");
						criteria = new FilterCriteriaTO ();
						criteria.setOrganizationTypeId (selectedOrganizationTypes);
						criteria.setRoleId (roleId);
						newRecipient.getCriteria ().add (criteria);
					}
				}
			} else {
				if (selectedAreaFields != null && selectedAreaFields.length > 0) {
					for (long areaFieldId : selectedAreaFields) {
						areaField.append (getValue (areaFields, areaFieldId)).append (", ");
						criteria = new FilterCriteriaTO ();
						criteria.setAreaFieldId (areaFieldId);
						criteria.setOrganizationTypeId (selectedOrganizationTypes);
						newRecipient.getCriteria ().add (criteria);
					}
				} else {
					criteria = new FilterCriteriaTO ();
					criteria.setOrganizationTypeId (selectedOrganizationTypes);
					newRecipient.getCriteria ().add (criteria);
				}
			}
		}
		StringBuilder caption = new StringBuilder ();
		caption.append (organizationType).append (" / ");
		if (organization.length () > 0) {
			caption.append (organization.substring (0, organization.lastIndexOf (", ")));
		}
		caption.append (" / ");
		if (areaField.length () > 0) {
			caption.append (areaField.substring (0, areaField.lastIndexOf (", ")));
		}
		caption.append (" / ");
		if (role.length () > 0) {
			caption.append (role.substring (0, role.lastIndexOf (", ")));
		}
		//recipients.add (new RecipientTO (0, caption.toString (), RecipientType.CRITERIA));
		newRecipient.setName (caption.toString ());
		recipients.add (newRecipient);
		return outcome;
	}

	/**
	 * Finds the label of a SelectItem by its value
	 * @param toSearch array to search
	 * @param id the value to search
	 * @return the label corresponding to the value
	 */
	private String getValue (final SelectItem[] toSearch, final Long id) {
		String value = null;
		for (SelectItem current : toSearch) {
			if (current.getValue ().equals (id)) {
				value = current.getLabel ();
				break;
			}
		}
		return value;
	}

	/**
	 * Adds a user into the recipient list
	 * @param event RowSelectorEvent triggered by the user
	 */
	public void userSelected (final RowSelectorEvent event) {
		RecipientTO newRecipient = new RecipientTO (foundUsers.get (event.getRow ()).getId (),
				foundUsers.get (event.getRow ()).getFirstName () + " "
				+ foundUsers.get (event.getRow ()).getLastName (),
				foundUsers.get (event.getRow ()).isNotHanUser () ? RecipientType.NON_HAN_USER : RecipientType.USER);
		if (!recipients.contains (newRecipient)) {
			recipients.add (newRecipient);
		}
	}

	/**
	 * Adds the current selected lists to the recipients
	 * @return outcome
	 */
	public String addSelectedLists () {
		String outcome = "success";
		RecipientTO newRecipient;
		for (GroupDisplayTO list : bulkUploadList) {
			if (list.isSelected ()) {
				newRecipient = new RecipientTO (list.getId (),
						list.getName (), RecipientType.NON_HAN_GROUP);
				if (!recipients.contains (newRecipient)) {
					recipients.add (newRecipient);
				}
			}
		}
		return outcome;
	}

	/**
	 * Adds the selected groups as recipients
	 * @return outcome
	 */
	public String addSelectedGroups () {
		String outcome = "success";
		RecipientTO newRecipient;
		for (GroupDisplayTO group : groupList) {
			if (group.isSelected ()) {
				newRecipient = new RecipientTO (group.getId (), group.getName (),
						RecipientType.GROUP);
				if (!recipients.contains (newRecipient)) {
					recipients.add (newRecipient);
				}
			}
		}
		return outcome;
	}

	/**
	 * Save the changes to the current group, if the group does not exist is created
	 * @return outcome
	 */
	public String saveGroup () {
		String outcome = "success";
		// Check if the group was loaded to edit and if the name changes, save as new.
		if (groupId != null && groupName.equals (originalName)) {
			groupId = null;
		}
		if (groupName != null && groupName.length () > 0 && !recipients.isEmpty ()) {
			if (groupId == null && groupSearchService.isNameAvailable (Long.parseLong (
					FacesContext.getCurrentInstance ().getExternalContext ().getUserPrincipal ().getName ()),
					groupName)) {
				try {
					// Retrieve user information
					ExternalContext context = FacesContext.getCurrentInstance ().getExternalContext ();
					Object requestObj = context.getRequest ();
					String userId = null;
					if (requestObj instanceof PortletRequest) {
						PortletRequest portletRequest = (PortletRequest) requestObj;
						userId = portletRequest.getRemoteUser ();
					} else if (requestObj instanceof HttpServletRequest) {
						HttpServletRequest request = (HttpServletRequest) requestObj;
						userId = request.getRemoteUser ();
					}
					groupManagementService.saveGroup (Long.parseLong (userId), groupName, recipients);
	
					String msg;
					try {
						ResourceBundle bundle = ResourceBundle.getBundle ("messages",
								FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
						msg = bundle.getString ("member.save.group.success");
					} catch (Exception ex1) {
						msg = "The group has been successfully saved with the name:";
					}
					msg += " " + groupName;
					FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_INFO, msg, msg);
					FacesContext.getCurrentInstance ().addMessage (null, message);
				} catch (Exception ex) {
					log.error ("Error saving group.", ex);
					String msg;
					try {
						ResourceBundle bundle = ResourceBundle.getBundle ("messages",
								FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
						msg = bundle.getString ("member.save.group.error");
					} catch (Exception ex1) {
						msg = "Error saving group with name:";
					}
					msg += " " + groupName;
					FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
					FacesContext.getCurrentInstance ().addMessage (null, message);
					outcome = "error";
				}
			} else if (groupId != null) {
				try {
					// Retrieve user information
					ExternalContext context = FacesContext.getCurrentInstance ().getExternalContext ();
					Object requestObj = context.getRequest ();
					String userId = null;
					if (requestObj instanceof PortletRequest) {
						PortletRequest portletRequest = (PortletRequest) requestObj;
						userId = portletRequest.getRemoteUser ();
					} else if (requestObj instanceof HttpServletRequest) {
						HttpServletRequest request = (HttpServletRequest) requestObj;
						userId = request.getRemoteUser ();
					}
					groupManagementService.updateGroup (Long.parseLong (userId), groupId, recipients);

					String msg;
					try {
						ResourceBundle bundle = ResourceBundle.getBundle ("messages",
								FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
						msg = bundle.getString ("member.save.group.success");
					} catch (Exception ex1) {
						msg = "The group has been successfully saved with the name:";
					}
					msg += " " + groupName;
					FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_INFO, msg, msg);
					FacesContext.getCurrentInstance ().addMessage (null, message);
				} catch (Exception ex) {
					log.error ("Error saving group.", ex);
					String msg;
					try {
						ResourceBundle bundle = ResourceBundle.getBundle ("messages",
								FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
						msg = bundle.getString ("member.save.group.error");
					} catch (Exception ex1) {
						msg = "Error saving group with name:";
					}
					msg += " " + groupName;
					FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
					FacesContext.getCurrentInstance ().addMessage (null, message);
					outcome = "error";
				}
			} else {
				String msg = null;
				try {
					ResourceBundle bundle = ResourceBundle.getBundle ("messages",
							FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
					msg = bundle.getString ("member.save.name.already.exists");
				} catch (Exception ex1) {
					msg = "You already have a group with the given name.";
				}
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
				FacesContext.getCurrentInstance ().addMessage (null, message);
				outcome = "error";
			}
		} else {
			String msg;
			if (groupName == null || groupName.length () == 0) {
				try {
					ResourceBundle bundle = ResourceBundle.getBundle ("messages",
							FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
					msg = bundle.getString ("member.save.no.groupname");
				} catch (Exception ex1) {
					msg = "You must provide a group name.";
				}
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
				FacesContext.getCurrentInstance ().addMessage (null, message);
				outcome = "error";
			}
			if (recipients.isEmpty ()) {
				try {
					ResourceBundle bundle = ResourceBundle.getBundle ("messages",
							FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
					msg = bundle.getString ("member.group.empty");
				} catch (Exception ex1) {
					msg = "The group most contain at least one recipient.";
				}
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
				FacesContext.getCurrentInstance ().addMessage (null, message);
				outcome = "error";
			}
		}
		return outcome;
	}

	/**
	 * Adds a list from the bulk uploads to the recipients list
	 * @param event RowSelectorEvent triggered by the user
	 */
	public void listSelected (final RowSelectorEvent event) {
		updateUsersFromBULists ();
	}

	/**
	 * Update the users by searching the non han users in the selected bulk upload lists
	 */
	private void updateUsersFromBULists () {
		List<Long> selectedListsIds = new ArrayList<Long> ();
		for (GroupDisplayTO buList : bulkUploadList) {
			if (buList.isSelected ()) {
				selectedListsIds.add (buList.getId ());
			}
		}
		foundUsers = new ArrayList<UserDisplayTO> ();
		if (selectedListsIds.size () > 0) {
			UserDisplayTO[] found = organizationalSearchService.searchUsersInBulkUpload (selectedListsIds);
			for (UserDisplayTO user : found) {
				foundUsers.add (user); 
			}
		}
	}

	/**
	 * Search the users in the selected groups
	 * @param event the RowSelectorEvent triggered by the user
	 */
	public void groupSelected (final RowSelectorEvent event) {
		updateUsersFromGroups ();
	}

	/**
	 * Updates the users using the groups selected
	 */
	public void updateUsersFromGroups () {
		List<Long> selectedGroupsIds = new ArrayList<Long> ();
		for (GroupDisplayTO group : groupList) {
			if (group.isSelected ()) {
				selectedGroupsIds.add (group.getId ());
			}
		}
		foundUsers = new ArrayList<UserDisplayTO> ();
		if (selectedGroupsIds.size () > 0) {
			UserDisplayTO[] found = organizationalSearchService.searchUsersInGroup (selectedGroupsIds);
			for (UserDisplayTO user : found) {
				foundUsers.add (user); 
			}
		}
	}

	/**
	 * Removes a recipient from the recipients list
	 * @param event RowSelectorEvent triggered by the user
	 */
	public void recipientSelected (final RowSelectorEvent event) {
		final RecipientTO recipient = recipients.get (event.getRow ());
		if (recipient.getType ().equals (RecipientType.USER)) {
			for (UserDisplayTO current : foundUsers) {
				if (current.getId () == recipient.getId ()) {
					current.setSelected (false);
					break;
				}
			}
		}
		recipients.remove (event.getRow ());
	}

	/**
	 * Perform a search of users by first and last name. At least one parameter must be provided.
	 * @return outcome
	 */
	public String searchByIndividual () {
		String outcome = "success";
		try {
			if ((firstNameFilter == null || firstNameFilter.equals ("")) && (lastNameFilter == null || lastNameFilter.equals (""))) {
				String msg;
				try {
					ResourceBundle bundle = ResourceBundle.getBundle ("messages",
							FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
					msg = bundle.getString ("member.individual.search.nofilter");
				} catch (Exception ex1) {
					msg = "You must provide a filter criteria.";
				}
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
				FacesContext.getCurrentInstance ().addMessage (null, message);
			} else {
				UserDisplayTO[] foundUsers = organizationalSearchService.searchIndividuals (
						firstNameFilter, lastNameFilter, searchOnlyChicagoUsers);
				this.foundUsers = new ArrayList<UserDisplayTO> ();
				if (foundUsers != null) {
					for (UserDisplayTO foundUser : foundUsers) {
						this.foundUsers.add (foundUser);
					}
				}
			}
			sort ();
		} catch (Exception ex) {
			log.error ("Error performing search of individuals by first and last name.", ex);
			String msg;
			try {
				ResourceBundle bundle = ResourceBundle.getBundle ("messages",
						FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
				msg = bundle.getString ("member.individual.search.error");
			} catch (Exception ex1) {
				msg = "The system couldn't perform the search, try again later.";
			}
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
			FacesContext.getCurrentInstance ().addMessage (null, message);
			outcome = "error";
		}
		return outcome;
	}

	/**
	 * Retrieves all the bulk upload lists.
	 * @return outcome
	 */
	public String loadBulkUploadLists () {
		collapsedBulkSearch = true;
		String outcome = "success";
		try {
			bulkUploadList = organizationalSearchService.getBulkUploadNames (bulkListNameFilter);
		} catch (Exception ex) {
			log.error ("Error loading bulk upload lists", ex);
			String msg;
			try {
				ResourceBundle bundle = ResourceBundle.getBundle ("messages",
						FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
				msg = bundle.getString ("member.individual.search.error");
			} catch (Exception ex1) {
				msg = "The system couldn't perform the search, try again later.";
			}
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
			FacesContext.getCurrentInstance ().addMessage (null, message);
			outcome = "error";
			outcome = "failure";
		}
		return outcome;
	}

	/**
	 * Loads the groups created by the user
	 * @return outcome
	 */
	public String loadMyGroups () {
		groupList = groupSearchService.searchMyGroups (Long.parseLong (
				FacesContext.getCurrentInstance ().getExternalContext ().getUserPrincipal ().getName ()));
		return "success";
	}

	/**
	 * Loads all the custom user groups
	 * @return outcome
	 */
	public String loadUserGroups () {
		collapsedGroupSearch = true;
		String outcome = "success";
		try {
			groupList = groupSearchService.searchUserGroups (groupNameFilter);
		} catch (Exception ex) {
			log.error ("Error loading user groups.", ex);
			String msg;
			try {
				ResourceBundle bundle = ResourceBundle.getBundle ("messages",
						FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
				msg = bundle.getString ("member.individual.search.error");
			} catch (Exception ex1) {
				msg = "The system couldn't perform the search, try again later.";
			}
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
			FacesContext.getCurrentInstance ().addMessage (null, message);
			outcome = "error";
		}
		return outcome;
	}

	/**
	 * Loads a group to edit
	 * @return outcome
	 */
	public String loadGroupForEdit () {
		String outcome = "succes";
		editConfirmationVisible = false;
		long groupId = -1;
		boolean multipleSelected = false;
		for (GroupDisplayTO group : groupList) {
			if (group.isSelected ()) {
				if (groupId == -1) {
					groupId = group.getId ();
				} else {
					multipleSelected = true;
				}
			}
		}
		if (multipleSelected) {
			String msg;
			try {
				ResourceBundle bundle = ResourceBundle.getBundle ("messages",
						FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
				msg = bundle.getString ("member.group.multiple.selection");
			} catch (Exception ex) {
				msg = "You can only edit one group at a time, select just one group.";
			}
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
			FacesContext.getCurrentInstance ().addMessage (null, message);
		} else if (groupId == -1) {
			String msg;
			try {
				ResourceBundle bundle = ResourceBundle.getBundle ("messages",
						FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
				msg = bundle.getString ("member.group.no.selection");
			} catch (Exception ex) {
				msg = "You must select one group to edit.";
			}
			FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
			FacesContext.getCurrentInstance ().addMessage (null, message);
		} else {
			try {
				final GroupTO group = groupSearchService.loadGroup (groupId);
				groupName = group.getName ();
				originalName = groupName;
				//this.groupId = group.getId ();
				recipients = group.getRecipients ();
			} catch (Exception ex) {
				String msg;
				try {
					ResourceBundle bundle = ResourceBundle.getBundle ("messages",
							FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
					msg = bundle.getString ("member.group.error.load.group");
				} catch (Exception ex1) {
					msg = "Error loading group to edit.";
				}
				FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
				FacesContext.getCurrentInstance ().addMessage (null, message);
				outcome = "failure";
			}
		}
		return outcome;
	}

	/**
	 * Action to clear all data in the group management bean.
	 * @return outcome
	 */
	public String clear () {
		String outcome = "success";
		areaFields = new SelectItem[]{};
		bulkListNameFilter = "";
		bulkUploadList = new GroupDisplayTO[]{};
		firstNameFilter = "";
		foundUsers = new ArrayList<UserDisplayTO> ();
		groupId = null;
		groupList = new GroupDisplayTO[]{};
		groupName = "";
		groupNameFilter = "";
		lastNameFilter = "";
		organizations = new SelectItem[]{};
		recipients = new ArrayList<RecipientTO> ();
		roles = new SelectItem[]{};
		selectedAreaFields = new long[]{};
		selectedBulkUploadLists = new Long[]{};
		selectedOrganizations = new long[]{};
		selectedOrganizationTypes = null;
		selectedRoles = new long[]{};
		onlySelectChicago = false;
		return outcome;
	}

	/**
	 * Action to clear the group search data
	 * @return "success"
	 */
	public String clearGroupSearch () {
		groupList = new GroupDisplayTO[]{};
		groupNameFilter = "";
		return "success";
	}

	/**
	 * Listener that cleans the bulkListNameFilter variable
	 * @param event ActionEvent
	 */
	public void cleanBulkNameListener (final ActionEvent event) {
		this.bulkListNameFilter = "";
	}

	/**
	 * Listener that cleans the bulkListNameFilter variable
	 * @param event ActionEvent
	 */
	public void cleanGroupNameListener (final ActionEvent event) {
		this.groupNameFilter = "";
	}

	/**
	 * Listener to save the recipient list into the session map.
	 * @param event ActionEvent
	 */
	public void saveListInSession (final ActionEvent event) {
		FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().put ("recipients", recipients);
	}

	/**
	 * Listener to load the recipient list from the session map.
	 * @param event ActionEvent
	 */
	@SuppressWarnings ("unchecked")
	public void loadListFromSession (final ActionEvent event) {
		List<RecipientTO> temp = (List<RecipientTO>) FacesContext.getCurrentInstance (
				).getExternalContext ().getSessionMap ().get ("recipients");
		if (temp != null) {
			clear ();
			recipients = temp;
			FacesContext.getCurrentInstance ().getExternalContext ().getSessionMap ().remove ("recipients");
		}
		if (recipients == null) {
			recipients = new ArrayList<RecipientTO> ();
		}
	}

	/**
	 * Action listener for deleting groups
	 * @param event ActionEvent
	 */
	public void deleteGroup (final ActionEvent event) {
		deleteConfirmationVisible = false;
		final String userIdString = FacesContext.getCurrentInstance ().getExternalContext (
				).getUserPrincipal ().getName ();
		final long userId = Long.parseLong (userIdString);
		final List<GroupDisplayTO> groups = new ArrayList<GroupDisplayTO> ();
		final List<GroupDisplayTO> deleted = new ArrayList<GroupDisplayTO> ();
		// Iterate over groups to delete selected
		for (GroupDisplayTO toDelete : groupList) {
			groups.add (toDelete);
			if (toDelete.isSelected ()) {
				final long groupId = toDelete.getId ();
				final DeleteResult result = groupManagementService.deleteGroup (userId, groupId);
				if (DeleteResult.GROUP_IN_USE.equals (result)) {
					String msg;
					try {
						ResourceBundle bundle = ResourceBundle.getBundle ("messages",
								FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
						msg = bundle.getString ("member.group.delete.in.use");
					} catch (Exception ex1) {
						msg = "You cannot delete a group that is being used in another group. Group Name:";
					}
					msg += " " + toDelete.getName ();
					FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
					FacesContext.getCurrentInstance ().addMessage (null, message);
				} else if (DeleteResult.NOT_OWNER.equals (result)) {
					String msg;
					try {
						ResourceBundle bundle = ResourceBundle.getBundle ("messages",
								FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
						msg = bundle.getString ("member.group.delete.not.owner");
					} catch (Exception ex1) {
						msg = "You cannot delete a group that does not belong to you. Group Name:";
					}
					msg += " " + toDelete.getName ();
					FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_ERROR, msg, msg);
					FacesContext.getCurrentInstance ().addMessage (null, message);
				} else if (DeleteResult.SUCCESS.equals (result)) {
					deleted.add (toDelete);
					String msg;
					try {
						ResourceBundle bundle = ResourceBundle.getBundle ("messages",
								FacesContext.getCurrentInstance ().getViewRoot ().getLocale ());
						msg = bundle.getString ("member.group.delete.success");
					} catch (Exception ex1) {
						msg = "The selected group was deleted successfully. Group Name:";
					}
					msg += " " + toDelete.getName ();
					FacesMessage message = new FacesMessage (FacesMessage.SEVERITY_INFO, msg, msg);
					FacesContext.getCurrentInstance ().addMessage (null, message);
				}
			}
		}
		groups.removeAll (deleted);
		groupList = groups.toArray (new GroupDisplayTO[groups.size ()]);
	}

	/**
	 * Listener to close all the search panels
	 * @param event ActionEvent
	 */
	public void closePanels (final ActionEvent event) {
		collapsedBulkSearch = false;
		collapsedCriteriaSearch = false;
		collapsedGroupSearch = false;
		collapsedIndividualSearch = false;
	}

	/**
	 * Listener to display the confirmation dialog for deleting a group
	 * @param event ActionEvent
	 */
	public void toggleConfirmDelete (final ActionEvent event) {
		/*if (!deleteConfirmationVisible) {
			final String groupId = (String) FacesContext.getCurrentInstance (
					).getExternalContext ().getRequestParameterMap ().get ("groupId");
			final String name = (String) FacesContext.getCurrentInstance (
					).getExternalContext ().getRequestParameterMap ().get ("name");
			groupNameToDelete = name;
			groupIdToDelete = Long.parseLong (groupId);
			deleteConfirmationVisible = true;
		} else {
			groupNameToDelete = null;
			groupIdToDelete = -1;
			deleteConfirmationVisible = false;
		}*/
		deleteConfirmationVisible = !deleteConfirmationVisible;
	}
	
	/**
	 * Listener to display the confirmation dialog for editing a group
	 * @param event ActionEvent
	 */
	public void toggleConfirmEdit (final ActionEvent event) {
		editConfirmationVisible= !editConfirmationVisible;
	}

	/**
	 * Sorts the found users using the given parameters
	 */
	private void sort () {
		if (!sortColumn.equals (oldSortColumn) || ascending != oldAscending) {
			UserDisplayComparator comparator;
			if ("id".equals (sortColumn)) {
				comparator = new UserDisplayComparator (SortColumn.ID, ascending);
			} else if ("firstName".equals (sortColumn)) {
				comparator = new UserDisplayComparator (SortColumn.FIRST_NAME, ascending);
			} else if ("lastName".equals (sortColumn)) {
				comparator = new UserDisplayComparator (SortColumn.LAST_NAME, ascending);
			} else if ("organization".equals (sortColumn)) {
				comparator = new UserDisplayComparator (SortColumn.ORGANIZATION, ascending);
			} else if ("role".equals (sortColumn)) {
				comparator = new UserDisplayComparator (SortColumn.ROLE, ascending);
			} else {
				comparator = new UserDisplayComparator (SortColumn.ID, ascending);
			}
			Collections.sort (foundUsers, comparator);
		}
	}

	/**
	 * @return the organizationTypes
	 */
	public SelectItem[] getOrganizationTypes () {
		return organizationTypes;
	}

	/**
	 * @return the selectedOrganizationTypes
	 */
	public Long getSelectedOrganizationTypes () {
		return selectedOrganizationTypes;
	}

	/**
	 * @param organizationTypes the organizationTypes to set
	 */
	public void setOrganizationTypes (final SelectItem[] organizationTypes) {
		this.organizationTypes = organizationTypes;
	}

	/**
	 * @param selectedOrganizationTypes the selectedOrganizationTypes to set
	 */
	public void setSelectedOrganizationTypes (final Long selectedOrganizationTypes) {
		this.selectedOrganizationTypes = selectedOrganizationTypes;
	}

	/**
	 * @return the foundUsers
	 */
	public List<UserDisplayTO> getFoundUsers () {
		return foundUsers;
	}

	/**
	 * @param foundUsers the foundUsers to set
	 */
	public void setFoundUsers (final List<UserDisplayTO> foundUsers) {
		this.foundUsers = foundUsers;
	}

	/**
	 * @return the organizations
	 */
	public SelectItem[] getOrganizations () {
		return organizations;
	}

	/**
	 * @return the selectedOrganizations
	 */
	public long[] getSelectedOrganizations () {
		return selectedOrganizations;
	}

	/**
	 * @param organizations the organizations to set
	 */
	public void setOrganizations (final SelectItem[] organizations) {
		this.organizations = organizations;
	}

	/**
	 * @param selectedOrganizations the selectedOrganizations to set
	 */
	public void setSelectedOrganizations (final long[] selectedOrganizations) {
		this.selectedOrganizations = selectedOrganizations;
	}

	/**
	 * @return the areaFields
	 */
	public SelectItem[] getAreaFields () {
		return areaFields;
	}

	/**
	 * @return the selectedAreaFields
	 */
	public long[] getSelectedAreaFields () {
		return selectedAreaFields;
	}

	/**
	 * @return the roles
	 */
	public SelectItem[] getRoles () {
		return roles;
	}

	/**
	 * @return the selectedRoles
	 */
	public long[] getSelectedRoles () {
		return selectedRoles;
	}

	/**
	 * @param areaFields the areaFields to set
	 */
	public void setAreaFields (final SelectItem[] areaFields) {
		this.areaFields = areaFields;
	}

	/**
	 * @param selectedAreaFields the selectedAreaFields to set
	 */
	public void setSelectedAreaFields (final long[] selectedAreaFields) {
		this.selectedAreaFields = selectedAreaFields;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles (final SelectItem[] roles) {
		this.roles = roles;
	}

	/**
	 * @param selectedRoles the selectedRoles to set
	 */
	public void setSelectedRoles (final long[] selectedRoles) {
		this.selectedRoles = selectedRoles;
	}

	/**
	 * @return the recipients
	 */
	public List<RecipientTO> getRecipients () {
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients (final List<RecipientTO> recipients) {
		this.recipients = recipients;
	}

	/**
	 * @return the firstNameFilter
	 */
	public String getFirstNameFilter () {
		return firstNameFilter;
	}

	/**
	 * @return the lastNameFilter
	 */
	public String getLastNameFilter () {
		return lastNameFilter;
	}

	/**
	 * @param firstNameFilter the firstNameFilter to set
	 */
	public void setFirstNameFilter (final String firstNameFilter) {
		this.firstNameFilter = firstNameFilter;
	}

	/**
	 * @param lastNameFilter the lastNameFilter to set
	 */
	public void setLastNameFilter (final String lastNameFilter) {
		this.lastNameFilter = lastNameFilter;
	}

	/**
	 * @return the bulkListNameFilter
	 */
	public String getBulkListNameFilter () {
		return bulkListNameFilter;
	}

	/**
	 * @param bulkListNameFilter the bulkListNameFilter to set
	 */
	public void setBulkListNameFilter (String bulkListNameFilter) {
		this.bulkListNameFilter = bulkListNameFilter;
	}

	/**
	 * @return the selectedBulkUploadLists
	 */
	public Long[] getSelectedBulkUploadLists () {
		return selectedBulkUploadLists;
	}

	/**
	 * @return the bulkUploadList
	 */
	public GroupDisplayTO[] getBulkUploadList () {
		return bulkUploadList;
	}

	/**
	 * @param selectedBulkUploadLists the selectedBulkUploadLists to set
	 */
	public void setSelectedBulkUploadLists (Long[] selectedBulkUploadLists) {
		this.selectedBulkUploadLists = selectedBulkUploadLists;
	}

	/**
	 * @param bulkUploadList the bulkUploadList to set
	 */
	public void setBulkUploadList (GroupDisplayTO[] bulkUploadList) {
		this.bulkUploadList = bulkUploadList;
	}

	/**
	 * @return the groupNameFilter
	 */
	public String getGroupNameFilter () {
		return groupNameFilter;
	}

	/**
	 * @param groupNameFilter the groupNameFilter to set
	 */
	public void setGroupNameFilter (String groupNameFilter) {
		this.groupNameFilter = groupNameFilter;
	}

	/**
	 * @return the groupList
	 */
	public GroupDisplayTO[] getGroupList () {
		return groupList;
	}

	/**
	 * @param groupList the groupList to set
	 */
	public void setGroupList (GroupDisplayTO[] groupList) {
		this.groupList = groupList;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName () {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName (String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the groupId
	 */
	public Long getGroupId () {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId (Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the onlySelectChicago
	 */
	public boolean isOnlySelectChicago () {
		return onlySelectChicago;
	}

	/**
	 * @param onlySelectChicago the onlySelectChicago to set
	 */
	public void setOnlySelectChicago (boolean onlySelectChicago) {
		this.onlySelectChicago = onlySelectChicago;
	}

	/**
	 * @return the searchOnlyChicagoUsers
	 */
	public boolean isSearchOnlyChicagoUsers () {
		return searchOnlyChicagoUsers;
	}

	/**
	 * @param searchOnlyChicagoUsers the searchOnlyChicagoUsers to set
	 */
	public void setSearchOnlyChicagoUsers (boolean searchOnlyChicagoUsers) {
		this.searchOnlyChicagoUsers = searchOnlyChicagoUsers;
	}

	/**
	 * @return the collapsedGroupSearch
	 */
	public boolean isCollapsedGroupSearch () {
		return collapsedGroupSearch;
	}

	/**
	 * @return the collapsedCriteriaSearch
	 */
	public boolean isCollapsedCriteriaSearch () {
		return collapsedCriteriaSearch;
	}

	/**
	 * @return the collapsedIndividualSearch
	 */
	public boolean isCollapsedIndividualSearch () {
		return collapsedIndividualSearch;
	}

	/**
	 * @return the collapsedBulkSearch
	 */
	public boolean isCollapsedBulkSearch () {
		return collapsedBulkSearch;
	}

	/**
	 * @param collapsedGroupSearch the collapsedGroupSearch to set
	 */
	public void setCollapsedGroupSearch (boolean collapsedGroupSearch) {
		this.collapsedGroupSearch = collapsedGroupSearch;
	}

	/**
	 * @param collapsedCriteriaSearch the collapsedCriteriaSearch to set
	 */
	public void setCollapsedCriteriaSearch (boolean collapsedCriteriaSearch) {
		this.collapsedCriteriaSearch = collapsedCriteriaSearch;
	}

	/**
	 * @param collapsedIndividualSearch the collapsedIndividualSearch to set
	 */
	public void setCollapsedIndividualSearch (boolean collapsedIndividualSearch) {
		this.collapsedIndividualSearch = collapsedIndividualSearch;
	}

	/**
	 * @param collapsedBulkSearch the collapsedBulkSearch to set
	 */
	public void setCollapsedBulkSearch (boolean collapsedBulkSearch) {
		this.collapsedBulkSearch = collapsedBulkSearch;
	}

	/**
	 * @param groupSearchService the groupSearchService to set
	 */
	public void setGroupSearchService (GroupSearchService groupSearchService) {
		this.groupSearchService = groupSearchService;
	}

	/**
	 * @param organizationalSearchService the organizationalSearchService to set
	 */
	public void setOrganizationalSearchService (
			OrganizationalSearchService organizationalSearchService) {
		this.organizationalSearchService = organizationalSearchService;
		init ();
	}

	/**
	 * @param groupManagementService the groupManagementService to set
	 */
	public void setGroupManagementService (
			GroupManagementService groupManagementService) {
		this.groupManagementService = groupManagementService;
	}

	/**
	 * @return the groupIdToDelete
	 */
	public long getGroupIdToDelete () {
		return groupIdToDelete;
	}

	/**
	 * @param groupIdToDelete the groupIdToDelete to set
	 */
	public void setGroupIdToDelete (long groupIdToDelete) {
		this.groupIdToDelete = groupIdToDelete;
	}

	/**
	 * @return the deleteConfirmationVisible
	 */
	public boolean isDeleteConfirmationVisible () {
		return deleteConfirmationVisible;
	}

	/**
	 * @param deleteConfirmationVisible the deleteConfirmationVisible to set
	 */
	public void setDeleteConfirmationVisible (boolean deleteConfirmationVisible) {
		this.deleteConfirmationVisible = deleteConfirmationVisible;
	}

	/**
	 * @return the groupNameToDelete
	 */
	public String getGroupNameToDelete () {
		StringBuilder builder = new StringBuilder ();
		boolean addComma = false;
		if (groupList != null && groupList.length > 0) {
			for (GroupDisplayTO group : groupList) {
				if (group.isSelected ()) {
					if (addComma) {
						builder.append (", ");
					}
					builder.append (group.getName ());
					addComma = true;
				}
			}
		}
		return builder.toString ();
	}

	public boolean isEditConfirmationVisible() {
		return editConfirmationVisible;
	}

	public void setEditConfirmationVisible(boolean editConfirmationVisible) {
		this.editConfirmationVisible = editConfirmationVisible;
	}

	public boolean isCanDelete () {
		boolean canDelete = false;
		if (groupList != null && groupList.length > 0) {
			for (GroupDisplayTO group : groupList) {
				if (group.isSelected ()) {
					canDelete = true;
					break;
				}
			}
		}
		return canDelete;
	}

	public int getUsersInArray () {
		log.info ("Called getUsersInArray...");
		return foundUsers == null ? 0 : foundUsers.size ();
	}

	/**
	 * @return the ascending
	 */
	public boolean isAscending () {
		return ascending;
	}

	/**
	 * @param ascending the ascending to set
	 */
	public void setAscending (boolean ascending) {
		oldAscending = this.ascending;
		this.ascending = ascending;
		if (oldAscending != ascending) {
			sort ();
		}
	}

	/**
	 * @return the sortColumn
	 */
	public String getSortColumn () {
		return sortColumn;
	}

	/**
	 * @param sortColumn the sortColumn to set
	 */
	public void setSortColumn (String sortColumn) {
		oldSortColumn = this.sortColumn;
		this.sortColumn = sortColumn;
		if (!sortColumn.equals (oldSortColumn)) {
			sort ();
		}
	}

}
