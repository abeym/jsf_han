package org.cdph.han.jsf.lifecycle;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.icesoft.faces.context.effects.JavascriptContext;


public class KeepAlivePhaseListener implements PhaseListener {
	
	/** Class logger */
	private static Log log = LogFactory.getLog(KeepAlivePhaseListener.class);
	
	/** script to keep the liferay session alive while only ajax requests are being performed
     */
    private final static String KEEP_LIFERAY_ALIVE_SCRIPT = 
        "Liferay.Session.banner.slideUp();\n" +
        "Liferay.Session.extend();";

	public void afterPhase(PhaseEvent arg0) {				
		JavascriptContext.addJavascriptCall(
				FacesContext.getCurrentInstance(), 
				KEEP_LIFERAY_ALIVE_SCRIPT);
	}

	public void beforePhase(PhaseEvent arg0) {
		// TODO Auto-generated method stub
	}

	public PhaseId getPhaseId() {
		return PhaseId.RENDER_RESPONSE;
	}

}
