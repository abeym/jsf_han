package org.cdph.han.persistence;

public enum AuditTrailAction {
	
	SUCCESSFUL_LOGIN, FAILED_LOGIN, USER_LOCKOUT, SESSION_DESTROYED, UNDEFINED,
	// Alert actions
	ALERT_MODIFIED, ALERT_PUBLISHED, ALERT_DELETED, ALERT_SCHEDULED, ALERT_ARCHIVED;
	
	public String getDisplayFormat() {
		String value = null;
		switch (this) {
		case SUCCESSFUL_LOGIN:
			value = "Successful Login";
			break;
		case FAILED_LOGIN:
			value = "Failed Login";
			break;
		case USER_LOCKOUT:
			value = "User Locked Out";
			break;
		case SESSION_DESTROYED:
			value = "Session Destroyed";
			break;
		case ALERT_MODIFIED:
			value = "Alert Modified";
			break;
		case ALERT_PUBLISHED:
			value = "Alert Published";
			break;
		case ALERT_DELETED:
			value = "Alert Soft Deleted";
			break;
		case ALERT_SCHEDULED:
			value = "Alert Scheduled";
			break;
		case ALERT_ARCHIVED:
			value = "Alert Archived";
			break;
		default:
			value = "Undefined";
			break;
		}
		return value;
	}

}
