package org.cdph.han.persistence;

import java.util.Collection;

/**
 * This interface is used to extract information from current and archived alerts
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AlertReportRecipientModel {

	/**
	 * @return the id
	 */
	long getId ();

	/**
	 * @return the recipientReportId
	 */
	int getRecipientReportId ();

	/**
	 * @return the firstName
	 */
	String getFirstName ();

	/**
	 * @return the lastName
	 */
	String getLastName ();

	/**
	 * @return the devices
	 */
	Collection<? extends AlertReportDeviceModel> getDevices ();

}