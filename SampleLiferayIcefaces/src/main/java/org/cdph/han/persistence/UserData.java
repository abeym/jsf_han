package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;
import javax.persistence.SecondaryTables;
import javax.persistence.Table;

/**
 * This persistence class retrieves the information of the user (Primary email).
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "UserData")
@Table (name = "User_", schema = "lportal")
@SecondaryTables ( value = {
	@SecondaryTable (name = "han_user_data", schema = "lportal",
			pkJoinColumns = {@PrimaryKeyJoinColumn (name = "userId", referencedColumnName = "userid")})
})
@NamedQueries (value = {
	@NamedQuery (name = "UserData.FindByFN", query = "select u from UserData u where upper (u.contact.firstName) like :firstName"),
	@NamedQuery (name = "UserData.FindByLN", query = "select u from UserData u where upper (u.contact.lastName) like :lastName"),
	@NamedQuery (name = "UserData.FindByFNLN", query = "select u from UserData u where upper (u.contact.firstName) like :firstName and upper (u.contact.lastName) like :lastName")
})
@NamedNativeQueries (
	value = {
		@NamedNativeQuery (
			name = "UserData.FindAlertInitiator",
			query = "select " +
					"    distinct u.*, hud.* " +
					"from " +
					"    User_ u left outer join han_user_data hud on (u.userId = hud.userid), " +
					"    UserGroupRole ugr, " +
					"    Users_Roles ur, " +
					"    (select r.* from Role_ r where ((r.name = 'Power User' or r.name = 'Administrator') and r.type_ = 1) or (r.name = 'HAN Alert Initiator' and r.type_ = 2)) r " +
					"where " +
					"    ((ugr.roleId = r.roleId and ugr.userId = u.userId) or (ur.roleId = r.roleId and ur.userId = u.userId));",
			resultClass = UserData.class
		),
		@NamedNativeQuery (
			name = "UserData.FindModifiedIds",
			query = "select " +
					"	* " +
					"from " +
					"	User_ u, " +
					"	han_user_data hud " +
					"where " +
					"   u.userId = hud.userid " +
					"	and hud.last_update > :fromDate",
			resultClass = UserData.class
		)
	}
)
public class UserData implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -992855628179051937L;
	/** The id of the user */
	@Id
	@Column (name = "userId", nullable = false, insertable = false, updatable = false)
	private long userId;
	/** The email of the user */
	@Column (name = "emailAddress", nullable = false, insertable = false, updatable = false)
	private String email;
	/** The user phone devices */
	@OneToMany (mappedBy = "userData")
	@OrderBy (value = "index asc")
	private Collection<ContactPreference> contactPreferences;
	/** The organizations that the user belongs to */
	@ManyToMany
	@JoinTable (name = "Users_Orgs", schema = "lportal",
			joinColumns = @JoinColumn (name = "userId", referencedColumnName = "userId"),
			inverseJoinColumns = @JoinColumn (name = "organizationId", referencedColumnName = "organizationId"))
	private Collection<Organization> organizations;
	/** Users roles */
	@ManyToMany
	@JoinTable (name = "Users_Roles", schema = "lportal",
			joinColumns = @JoinColumn (name = "userId", referencedColumnName = "userId"),
			inverseJoinColumns = @JoinColumn (name = "roleId", referencedColumnName = "roleId"))
	private Collection<Role> roles;
	/** Users organizations, roles and areas or fields */
	@OneToMany (mappedBy = "userData")
	private Collection<UserOrganizationalData> userOrganizationalData;
	/** Id of the contact field */
	@OneToOne
	@JoinColumn (name = "contactId")
	private Contact contact;
	/** Screen name of the user, must be the same as mir3 user name */
	@Column (name = "screenName")
	private String screenName;
	/** Telephony password */
	@Column (name = "telephony_pass", table = "han_user_data")
	private String telephonyPassword;
	/** Telephony PIN */
	@Column (name = "telephony_pin", table = "han_user_data")
	private String telephonyPIN;
	/** Telephony ID */
	@Column (name = "telephony_id", table = "han_user_data")
	private String telephonyId;
	/** User alternate contacts */
	@OneToMany (mappedBy = "userData")
	private Collection<AlternateContact> alternateContacts;
	/** Group Roles */
	@OneToMany
	@JoinTable (name = "UserGroupRole", schema = "lportal",
			joinColumns = @JoinColumn (name = "userId", referencedColumnName = "userId"),
			inverseJoinColumns = @JoinColumn (name = "roleId", referencedColumnName = "roleId"))
	private Collection<Role> groupRoles;
	/** Users department */
	@Column (name = "department", table = "han_user_data")
	private String department;
	/** Flag to determine if the user is active */
	@Column (name = "active_", nullable = false)
	private boolean active;
	/** ID generated by mir3 */
	@Column (name = "mir3_id", table = "han_user_data")
	private Integer mir3Id;
	/** Password for the internal email */
	@Column (name = "email_pass", table = "han_user_data")
	private String emailPassword;
	/** If the user was warned of being close to the quota limit */
	@Column (name = "quota_warned", table = "han_user_data")
	private Boolean quotaWarned;

	/**
	 * @return the userId
	 */
	public long getUserId () {
		return userId;
	}

	/**
	 * @return the email
	 */
	public String getEmail () {
		return email;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId (final long userId) {
		this.userId = userId;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail (final String email) {
		this.email = email;
	}

	/**
	 * @return the contactPreferences
	 */
	public Collection<ContactPreference> getContactPreferences () {
		return contactPreferences;
	}

	/**
	 * @param contactPreferences the contactPreferences to set
	 */
	public void setContactPreferences (
			Collection<ContactPreference> contactPreferences) {
		this.contactPreferences = contactPreferences;
	}

	/**
	 * @return the organizations
	 */
	public Collection<Organization> getOrganizations () {
		return organizations;
	}

	/**
	 * @param organizations the organizations to set
	 */
	public void setOrganizations (Collection<Organization> organizations) {
		this.organizations = organizations;
	}

	/**
	 * @return the userOrganizationalData
	 */
	public Collection<UserOrganizationalData> getUserOrganizationalData () {
		return userOrganizationalData;
	}

	/**
	 * @param userOrganizationalData the userOrganizationalData to set
	 */
	public void setUserOrganizationalData (final Collection<UserOrganizationalData> userOrganizationalData) {
		this.userOrganizationalData = userOrganizationalData;
	}

	/**
	 * @return the contactId
	 */
	public Contact getContact () {
		return contact;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContact (final Contact contact) {
		this.contact = contact;
	}

	/**
	 * @return the screenName
	 */
	public String getScreenName () {
		return screenName;
	}

	/**
	 * @param screenName the screenName to set
	 */
	public void setScreenName (String screenName) {
		this.screenName = screenName;
	}

	/**
	 * @return the telephonyPassword
	 */
	public String getTelephonyPassword () {
		return telephonyPassword;
	}

	/**
	 * @param telephonyPassword the telephonyPassword to set
	 */
	public void setTelephonyPassword (String telephonyPassword) {
		this.telephonyPassword = telephonyPassword;
	}

	/**
	 * @return the roles
	 */
	public Collection<Role> getRoles () {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles (Collection<Role> roles) {
		this.roles = roles;
	}

	/**
	 * @return the telephonyPIN
	 */
	public String getTelephonyPIN () {
		return telephonyPIN;
	}

	/**
	 * @param telephonyPIN the telephonyPIN to set
	 */
	public void setTelephonyPIN (String telephonyPIN) {
		this.telephonyPIN = telephonyPIN;
	}

	/**
	 * @return the telephonyId
	 */
	public String getTelephonyId () {
		return telephonyId;
	}

	/**
	 * @param telephonyId the telephonyId to set
	 */
	public void setTelephonyId (String telephonyId) {
		this.telephonyId = telephonyId;
	}

	/**
	 * @return the alternateContacts
	 */
	public Collection<AlternateContact> getAlternateContacts () {
		return alternateContacts;
	}

	/**
	 * @param alternateContacts the alternateContacts to set
	 */
	public void setAlternateContacts (Collection<AlternateContact> alternateContacts) {
		this.alternateContacts = alternateContacts;
	}

	/**
	 * @return the groupRoles
	 */
	public Collection<Role> getGroupRoles () {
		return groupRoles;
	}

	/**
	 * @param groupRoles the groupRoles to set
	 */
	public void setGroupRoles (Collection<Role> groupRoles) {
		this.groupRoles = groupRoles;
	}

	/**
	 * @return the department
	 */
	public String getDepartment () {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment (String department) {
		this.department = department;
	}

	/**
	 * @return the active
	 */
	public boolean isActive () {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive (boolean active) {
		this.active = active;
	}

	/**
	 * @return the mir3Id
	 */
	public Integer getMir3Id () {
		return mir3Id;
	}

	/**
	 * @param mir3Id the mir3Id to set
	 */
	public void setMir3Id (Integer mir3Id) {
		this.mir3Id = mir3Id;
	}

	/**
	 * @return the emailPassword
	 */
	public String getEmailPassword () {
		return emailPassword;
	}

	/**
	 * @param emailPassword the emailPassword to set
	 */
	public void setEmailPassword (String emailPassword) {
		this.emailPassword = emailPassword;
	}

	/**
	 * @return the quotaWarned
	 */
	public Boolean isQuotaWarned () {
		return quotaWarned;
	}

	/**
	 * @param quotaWarned the quotaWarned to set
	 */
	public void setQuotaWarned (Boolean quotaWarned) {
		this.quotaWarned = quotaWarned;
	}

}
