package org.cdph.han.persistence;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.Table;
import javax.persistence.ManyToOne;

/**
 * Entity class used to link the user data with the organizational data such as area or field and organizational role
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "UserOrganizationalData")
@Table (name = "users_areafields", schema = "lportal")
public class UserOrganizationalData {
	/** Primary key */
	@EmbeddedId
	private UserOrganizationalDataPK primaryKey;
	/** User data */
	@ManyToOne
	@JoinColumn (name = "userId", nullable = false, insertable = false, updatable = false)
	private UserData userData;
	/** Users organization */
	@ManyToOne
	@JoinColumn (name = "organizationId", nullable = false, insertable = false, updatable = false)
	private Organization organization;
	/** Users area or field */
	@ManyToOne
	@JoinColumn (name = "area_field_id", nullable = false, insertable = false, updatable = false)
	private AreaField areaField;
	/** Users organizational role */
	@ManyToOne
	@JoinColumn (name = "roleId", nullable = false, insertable = false, updatable = false)
	private Role role;

	/**
	 * @return the userData
	 */
	public UserData getUserData () {
		return userData;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization () {
		return organization;
	}

	/**
	 * @return the areaField
	 */
	public AreaField getAreaField () {
		return areaField;
	}

	/**
	 * @return the role
	 */
	public Role getRole () {
		return role;
	}

	/**
	 * @param userData the userData to set
	 */
	public void setUserData (final UserData userData) {
		this.userData = userData;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization (final Organization organization) {
		this.organization = organization;
	}

	/**
	 * @param areaField the areaField to set
	 */
	public void setAreaField (final AreaField areaField) {
		this.areaField = areaField;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole (final Role role) {
		this.role = role;
	}

	/**
	 * @return the primaryKey
	 */
	public UserOrganizationalDataPK getPrimaryKey () {
		return primaryKey;
	}

	/**
	 * @param primaryKey the primaryKey to set
	 */
	public void setPrimaryKey (final UserOrganizationalDataPK primaryKey) {
		this.primaryKey = primaryKey;
	}

}
