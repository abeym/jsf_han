package org.cdph.han.persistence;

import java.util.Collection;
import java.util.Date;

/**
 * Convenience interface to retrieve data from Alert or ArchivedAlert objects
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AlertModel {

	/**
	 * @return the id
	 */
	long getId ();

	/**
	 * @return the author
	 */
	UserData getAuthor ();

	/**
	 * @return the contact
	 */
	UserData getContact ();

	/**
	 * @return the modifier
	 */
	UserData getModifier ();

	/**
	 * @return the dateCreated
	 */
	Date getDateCreated ();

	/**
	 * @return the datePublished
	 */
	Date getDatePublished ();

	/**
	 * @return the dateModified
	 */
	Date getDateModified ();

	/**
	 * @return the dateScheduled
	 */
	Date getDateScheduled ();

	/**
	 * @return the name
	 */
	String getName ();

	/**
	 * @return the dateExpired
	 */
	Date getDateExpired ();

	/**
	 * @return the validateRecipients
	 */
	boolean isValidateRecipients ();

	/**
	 * @return the replayMessages
	 */
	boolean isReplayMessages ();

	/**
	 * @return the oneTextContact
	 */
	boolean isOneTextContact ();

	/**
	 * @return the requiresPIN
	 */
	boolean isRequiresPIN ();

	/**
	 * @return the confirmResponse
	 */
	boolean isConfirmResponse ();

	/**
	 * @return the reportRecipients
	 */
	boolean isReportRecipients ();

	/**
	 * @return the playGreeting
	 */
	boolean isPlayGreeting ();

	/**
	 * @return the useAlternates
	 */
	boolean isUseAlternates ();

	/**
	 * @return the selectLanguage
	 */
	boolean isSelectLanguage ();

	/**
	 * @return the useTopics
	 */
	boolean isUseTopics ();

	/**
	 * @return the sendToSubscribers
	 */
	boolean isSendToSubscribers ();

	/**
	 * @return the category
	 */
	String getCategory ();

	/**
	 * @return the priority
	 */
	String getPriority ();

	/**
	 * @return the severity
	 */
	String getSeverity ();

	/**
	 * @return the contactCycleDelay
	 */
	Integer getContactCycleDelay ();

	/**
	 * @return the textDeviceDelay
	 */
	Integer getTextDeviceDelay ();

	/**
	 * @return the contactAttempCycles
	 */
	Integer getContactAttempCycles ();

	/**
	 * @return the expeditedDelivery
	 */
	boolean isExpeditedDelivery ();

	/**
	 * @return the leaveMessage
	 */
	Integer getLeaveMessage ();

	/**
	 * @return the attachments
	 */
	Collection<? extends AlertAttachmentModel> getAttachments ();

	/**
	 * @return the messages
	 */
	Collection<? extends AlertMessageModel> getMessages ();

	/**
	 * @return the options
	 */
	Collection<? extends AlertOptionModel> getOptions ();

	/**
	 * @return the recipients
	 */
	Collection<? extends AlertRecipientModel> getRecipients ();

	/**
	 * @return the topic
	 */
	AlertTopic getTopic ();

	/**
	 * @return the incidentDate
	 */
	Date getIncidentDate ();

	/**
	 * @return the duration
	 */
	Integer getDuration ();

	/**
	 * @return the entireMachine
	 */
	boolean isEntireMachine ();

	/**
	 * @return the partialMachine
	 */
	boolean isPartialMachine ();

	/**
	 * @return the entireMessage
	 */
	boolean isEntireMessage ();

	/**
	 * @return the answerPhone
	 */
	boolean isAnswerPhone ();

	/**
	 * @return the notificationMethod
	 */
	Integer getNotificationMethod ();

	/**
	 * @return the voiceDelivery
	 */
	Integer getVoiceDelivery ();

	/**
	 * @return the publisher
	 */
	UserData getPublisher ();

	/**
	 * @return the voiceFile
	 */
	String getVoiceFile ();

	/**
	 * @return the alertReports
	 */
	Collection<? extends AlertReportModel> getAlertReports ();

	/**
	 * @return the deleted
	 */
	boolean isDeleted ();

	/**
	 * @return the locations
	 */
	Collection<? extends AlertLocationModel> getLocations ();

	/**
	 * @return the overrideDefaultOnly
	 */
	boolean isOverrideDefaultsOnly ();

	/**
	 * @return the locationOverride
	 */
	boolean isLocationOverride ();

}