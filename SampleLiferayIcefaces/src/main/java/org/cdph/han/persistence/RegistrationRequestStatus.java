package org.cdph.han.persistence;

public enum RegistrationRequestStatus {
	UNKNOWN(-1), SUBMITTED(1), APPROVED(2), REJECTED(3);

	private int value;

	RegistrationRequestStatus(int value) {
		this.value = value;
	}

	// the identifierMethod
	public int toInt() {
		return value;
	}

	// the valueOfMethod
	public static RegistrationRequestStatus fromInt(int value) {
		switch (value) {
		case 1:
			return SUBMITTED;
		case 2:
			return APPROVED;
		case 3:
			return REJECTED;
		default:
			return UNKNOWN;
		}
	}

	public String toString() {
		switch (this) {
		case SUBMITTED:
			return "SUBMITTED";
		case APPROVED:
			return "APPROVED";
		case REJECTED:
			return "REJECTED";
		default:
			return "UNKNOWN";
		}
	}

}
