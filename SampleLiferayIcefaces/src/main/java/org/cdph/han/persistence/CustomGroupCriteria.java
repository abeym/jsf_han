package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class representing a criteria filter for a personal group
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "cust_group_criteria", schema = "lportal")
public class CustomGroupCriteria implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -3224706843847399997L;
	/** Id of the criteria */
	@Id
	@Column (name = "criteriaId", nullable = false)
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long criteriaId;
	/** Custom member representing the criteria */
	@ManyToOne
	@JoinColumn (name = "memberId")
	private CustomGroupMember member;
	/** Organization type */
	@ManyToOne
	@JoinColumn (name = "organizationTypeId")
	private Organization organizationType;
	/** Organization */
	@ManyToOne
	@JoinColumn (name = "organizationId", nullable = true)
    private Organization organization;
	/** Area/Field */
	@ManyToOne
	@JoinColumn (name = "areaFieldId", nullable = true)
    private AreaField areaField;
	/** Role */
	@ManyToOne
	@JoinColumn (name = "roleId", nullable = true)
    private Role role;

	/**
	 * @return the criteriaId
	 */
	public long getCriteriaId () {
		return criteriaId;
	}

	/**
	 * @param criteriaId the criteriaId to set
	 */
	public void setCriteriaId (long criteriaId) {
		this.criteriaId = criteriaId;
	}

	/**
	 * @return the member
	 */
	public CustomGroupMember getMember () {
		return member;
	}

	/**
	 * @param member the member to set
	 */
	public void setMember (CustomGroupMember member) {
		this.member = member;
	}

	/**
	 * @return the organizationType
	 */
	public Organization getOrganizationType () {
		return organizationType;
	}

	/**
	 * @param organizationType the organizationType to set
	 */
	public void setOrganizationType (Organization organizationType) {
		this.organizationType = organizationType;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization () {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization (Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the areaField
	 */
	public AreaField getAreaField () {
		return areaField;
	}

	/**
	 * @param areaField the areaField to set
	 */
	public void setAreaField (AreaField areaField) {
		this.areaField = areaField;
	}

	/**
	 * @return the role
	 */
	public Role getRole () {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole (Role role) {
		this.role = role;
	}

}
