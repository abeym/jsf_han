package org.cdph.han.persistence;

import java.util.Date;

/**
 * This interface is used to retrieve the information from the report from current and archived alerts
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AlertReportDeviceModel {

	/**
	 * @return the id
	 */
	long getId ();

	/**
	 * @return the address
	 */
	String getAddress ();

	/**
	 * @return the result
	 */
	String getResult ();

	/**
	 * @return the timeSent
	 */
	Date getTimeSent ();

	/**
	 * @return the timeResponded
	 */
	Date getTimeResponded ();

	/**
	 * @return the duration
	 */
	int getDuration ();

	/**
	 * @return the description
	 */
	String getDescription ();

	/**
	 * @return the contactId
	 */
	int getContactId ();

	/**
	 * @return the response
	 */
	Integer getResponse ();

}