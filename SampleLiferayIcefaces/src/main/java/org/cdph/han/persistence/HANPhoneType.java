package org.cdph.han.persistence;

public enum HANPhoneType {
		
	WORK(1),
	MOBILE(2),
	HOME(3),
	TTY(4),
	ONE_WAY_PAGER(5),
	TWO_WAY_PAGER(6),
	NUMERIC_PAGER(7),
	SMS(8),
	FAX(9),
	HOME_EMAIL(10),
	WORK_EMAIL(11);
	
	
	private final int listTypeId;
	
	HANPhoneType(int listTypeId) {
		this.listTypeId = listTypeId;
	}
	
	public int listTypeId(){
		return listTypeId;
	}	
	
}
