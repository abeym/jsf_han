package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Entity class representing a message of an archived alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_messages_archive", schema = "lportal")
public class ArchivedAlertMessage implements Serializable, AlertMessageModel {
	/** Class serial version UID */
	private static final long serialVersionUID = 991757657698182347L;
	/** ID of the message */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which this message belongs */
	@ManyToOne
	@JoinColumn (name = "archived_alert_id", nullable = false)
	private ArchivedAlert archivedAlert;
	/** Alert locale */
	@ManyToOne
	@JoinColumn (name = "alert_locale_id", nullable = false)
	private AlertLocale locale;
	/** Attachment for the telephony message */
	@OneToOne
	@JoinColumn (name = "alert_attachment_id", nullable = true)
	private ArchivedAlertAttachment attachment;
	/** Telephony message */
	@Column (name = "telephony_message", nullable = true, length = 3950)
	private String telephonyMessage;
	/** Message to be displayed in the portal */
	@Lob
	@Column (name = "portal_message", nullable = true, length = 65535)
	private String portalMessage;
	/** Abstract of the alert */
	@Lob
	@Column (name = "abstract", nullable = true, length = 65535)
	private String abstractText;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof ArchivedAlertMessage) {
			final ArchivedAlertMessage other = (ArchivedAlertMessage) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the archivedAlert
	 */
	public ArchivedAlert getArchivedAlert () {
		return archivedAlert;
	}

	/**
	 * @param archivedAlert the archivedAlert to set
	 */
	public void setArchivedAlert (ArchivedAlert archivedAlert) {
		this.archivedAlert = archivedAlert;
	}

	/**
	 * @return the locale
	 */
	public AlertLocale getLocale () {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale (AlertLocale locale) {
		this.locale = locale;
	}

	/**
	 * @return the attachment
	 */
	public ArchivedAlertAttachment getAttachment () {
		return attachment;
	}

	/**
	 * @param attachment the attachment to set
	 */
	public void setAttachment (ArchivedAlertAttachment attachment) {
		this.attachment = attachment;
	}

	/**
	 * @return the telephonyMessage
	 */
	public String getTelephonyMessage () {
		return telephonyMessage;
	}

	/**
	 * @param telephonyMessage the telephonyMessage to set
	 */
	public void setTelephonyMessage (String telephonyMessage) {
		this.telephonyMessage = telephonyMessage;
	}

	/**
	 * @return the portalMessage
	 */
	public String getPortalMessage () {
		return portalMessage;
	}

	/**
	 * @param portalMessage the portalMessage to set
	 */
	public void setPortalMessage (String portalMessage) {
		this.portalMessage = portalMessage;
	}

	/**
	 * @return the abstractText
	 */
	public String getAbstractText () {
		return abstractText;
	}

	/**
	 * @param abstractText the abstractText to set
	 */
	public void setAbstractText (String abstractText) {
		this.abstractText = abstractText;
	}

}
