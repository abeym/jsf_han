package org.cdph.han.persistence;

public enum ContactDeviceType {
	WORK_PHONE, WORK_EXT, WORK_EMAIL, CELL_PHONE, HOME_PHONE, PERSONAL_EMAIL;

	public static String getAbbreviation(String name) {
		if(name.equals("Work Phone")){
			return "wP";
		}else if(name.equals("Work Extension")){
			return "wEm";
		}else if(name.equals("Work EMail")){
			return "wE";
		}else if(name.equals("Cell Phone")){
			return "cP";
		}else if(name.equals("Home Phone")){
			return "hP";
		}else if(name.equals("Personal EMail")){
			return "pE";
		}else{
			return "undefined";
		}
	}

	public String toString() {
		String value = null;
		switch (this) {
		case WORK_PHONE:
			value = "Work Phone";
			break;
		case WORK_EXT:
			value = "Work Extension";
			break;
		case WORK_EMAIL:
			value = "Work EMail";
			break;
		case CELL_PHONE:
			value = "Cell Phone";
			break;
		case HOME_PHONE:
			value = "Home Phone";
			break;
		case PERSONAL_EMAIL:
			value = "Personal EMail";
			break;
		default:
			value = "Undefined";
			break;
		}
		return value;
	}
}
