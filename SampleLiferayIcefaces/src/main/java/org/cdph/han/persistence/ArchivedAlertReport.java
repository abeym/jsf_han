package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity class representing an Alert Report
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_reports_archive", schema = "lportal")
public class ArchivedAlertReport implements Serializable, AlertReportModel {
	/** Class serial version UID */
	private static final long serialVersionUID = 383189894838091783L;
	/** Report ID */
	@Id
	@Column (name = "id", nullable = false)
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which the report belongs */
	@ManyToOne
	@JoinColumn (name = "alert_id", nullable = false)
	private ArchivedAlert alert;
	/** Id of the report provided by mir3 */
	@Column (name = "notification_report_id", nullable = false)
	private int notificationReportId;
	/** Date and time when the notification was completed */
	@Column (name = "completed")
	private Date completed;
	/** Phone time spend */
	@Column (name = "phone_time")
	private Integer phoneTime;
	/** Total recipients in the notification */
	@Column (name = "total_recipients")
	private Integer totalRecipients;
	/** Total recipients contacted */
	@Column (name = "total_contacted")
	private Integer totalContacted;
	/** Total recipients that responded the notification */
	@Column (name = "total_responded")
	private Integer totalResponded;
	/** Total calls made */
	@Column (name = "total_calls")
	private Integer totalCalls;
	/** Total emails sent */
	@Column (name = "total_emails")
	private Integer totalEmails;
	/** Total pages sent */
	@Column (name = "total_pages")
	private Integer totalPages;
	/** Total faxes sent */
	@Column (name = "total_faxes")
	private Integer totalFaxes;
	/** Total sms sent */
	@Column (name = "total_sms")
	private Integer totalSms;
	/** Total hang ups */
	@Column (name = "hang_up")
	private Integer hangUp;
	/** Total times not answered */
	@Column (name = "no_answer")
	private Integer noAnswer;
	/** Total times line busy */
	@Column (name = "busy")
	private Integer busy;
	/** Total times not at this location */
	@Column (name = "not_at_this_location")
	private Integer notAtThisLocation;
	/** Total invalid responses */
	@Column (name = "invalid_response")
	private Integer invalidResponse;
	/** Total answering machine response */
	@Column (name = "answering_machine")
	private Integer answeringMachine;
	/** Total messages left */
	@Column (name = "left_message")
	private Integer leftMessage;
	/** Total wrong addresses or phone numbers */
	@Column (name = "wrong_address_tel")
	private Integer wrongAddressTel;
	/** Total other issues */
	@Column (name = "other")
	private Integer other;
	/** If the notification is finished or not */
	@Column (name = "notification_finished")
	private boolean notificationFinished;
	/** Report recipients */
	@OneToMany (mappedBy = "report", cascade = {CascadeType.ALL})
	private Collection<ArchivedAlertReportRecipient> recipients;
	/** Last time the report was fetch */
	@Column (name = "last_fetch")
	private Date lastFetch;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof ArchivedAlertReport) {
			final ArchivedAlertReport other = (ArchivedAlertReport) obj;
			equal = other.getId () == id && other.getNotificationReportId () == notificationReportId;
			equal = equal && other.getAlert ().equals (alert);
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportModel#getId()
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportModel#getAlert()
	 */
	public ArchivedAlert getAlert () {
		return alert;
	}

	/**
	 * @param alert the alert to set
	 */
	public void setAlert (ArchivedAlert alert) {
		this.alert = alert;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportModel#getNotificationReportId()
	 */
	public int getNotificationReportId () {
		return notificationReportId;
	}

	/**
	 * @param notificationReportId the notificationReportId to set
	 */
	public void setNotificationReportId (int notificationReportId) {
		this.notificationReportId = notificationReportId;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportModel#getCompleted()
	 */
	public Date getCompleted () {
		return completed;
	}

	/**
	 * @param completed the completed to set
	 */
	public void setCompleted (Date completed) {
		this.completed = completed;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportModel#getPhoneTime()
	 */
	public Integer getPhoneTime () {
		return phoneTime;
	}

	/**
	 * @param phoneTime the phoneTime to set
	 */
	public void setPhoneTime (Integer phoneTime) {
		this.phoneTime = phoneTime;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportModel#getTotalRecipients()
	 */
	public Integer getTotalRecipients () {
		return totalRecipients;
	}

	/**
	 * @param totalRecipients the totalRecipients to set
	 */
	public void setTotalRecipients (Integer totalRecipients) {
		this.totalRecipients = totalRecipients;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportModel#getTotalContacted()
	 */
	public Integer getTotalContacted () {
		return totalContacted;
	}

	/**
	 * @param totalContacted the totalContacted to set
	 */
	public void setTotalContacted (Integer totalContacted) {
		this.totalContacted = totalContacted;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertReportModel#getTotalResponded()
	 */
	public Integer getTotalResponded () {
		return totalResponded;
	}

	/**
	 * @param totalResponded the totalResponded to set
	 */
	public void setTotalResponded (Integer totalResponded) {
		this.totalResponded = totalResponded;
	}

	/**
	 * @return the totalCalls
	 */
	public Integer getTotalCalls () {
		return totalCalls;
	}

	/**
	 * @param totalCalls the totalCalls to set
	 */
	public void setTotalCalls (Integer totalCalls) {
		this.totalCalls = totalCalls;
	}

	/**
	 * @return the totalEmails
	 */
	public Integer getTotalEmails () {
		return totalEmails;
	}

	/**
	 * @param totalEmails the totalEmails to set
	 */
	public void setTotalEmails (Integer totalEmails) {
		this.totalEmails = totalEmails;
	}

	/**
	 * @return the totalPages
	 */
	public Integer getTotalPages () {
		return totalPages;
	}

	/**
	 * @param totalPages the totalPages to set
	 */
	public void setTotalPages (Integer totalPages) {
		this.totalPages = totalPages;
	}

	/**
	 * @return the totalFaxes
	 */
	public Integer getTotalFaxes () {
		return totalFaxes;
	}

	/**
	 * @param totalFaxes the totalFaxes to set
	 */
	public void setTotalFaxes (Integer totalFaxes) {
		this.totalFaxes = totalFaxes;
	}

	/**
	 * @return the totalSms
	 */
	public Integer getTotalSms () {
		return totalSms;
	}

	/**
	 * @param totalSms the totalSms to set
	 */
	public void setTotalSms (Integer totalSms) {
		this.totalSms = totalSms;
	}

	/**
	 * @return the hangUp
	 */
	public Integer getHangUp () {
		return hangUp;
	}

	/**
	 * @param hangUp the hangUp to set
	 */
	public void setHangUp (Integer hangUp) {
		this.hangUp = hangUp;
	}

	/**
	 * @return the noAnswer
	 */
	public Integer getNoAnswer () {
		return noAnswer;
	}

	/**
	 * @param noAnswer the noAnswer to set
	 */
	public void setNoAnswer (Integer noAnswer) {
		this.noAnswer = noAnswer;
	}

	/**
	 * @return the busy
	 */
	public Integer getBusy () {
		return busy;
	}

	/**
	 * @param busy the busy to set
	 */
	public void setBusy (Integer busy) {
		this.busy = busy;
	}

	/**
	 * @return the notAtThisLocation
	 */
	public Integer getNotAtThisLocation () {
		return notAtThisLocation;
	}

	/**
	 * @param notAtThisLocation the notAtThisLocation to set
	 */
	public void setNotAtThisLocation (Integer notAtThisLocation) {
		this.notAtThisLocation = notAtThisLocation;
	}

	/**
	 * @return the invalidResponse
	 */
	public Integer getInvalidResponse () {
		return invalidResponse;
	}

	/**
	 * @param invalidResponse the invalidResponse to set
	 */
	public void setInvalidResponse (Integer invalidResponse) {
		this.invalidResponse = invalidResponse;
	}

	/**
	 * @return the answeringMachine
	 */
	public Integer getAnsweringMachine () {
		return answeringMachine;
	}

	/**
	 * @param answeringMachine the answeringMachine to set
	 */
	public void setAnsweringMachine (Integer answeringMachine) {
		this.answeringMachine = answeringMachine;
	}

	/**
	 * @return the leftMessage
	 */
	public Integer getLeftMessage () {
		return leftMessage;
	}

	/**
	 * @param leftMessage the leftMessage to set
	 */
	public void setLeftMessage (Integer leftMessage) {
		this.leftMessage = leftMessage;
	}

	/**
	 * @return the wrongAddressTel
	 */
	public Integer getWrongAddressTel () {
		return wrongAddressTel;
	}

	/**
	 * @param wrongAddressTel the wrongAddressTel to set
	 */
	public void setWrongAddressTel (Integer wrongAddressTel) {
		this.wrongAddressTel = wrongAddressTel;
	}

	/**
	 * @return the other
	 */
	public Integer getOther () {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther (Integer other) {
		this.other = other;
	}

	/**
	 * @return the notificationFinished
	 */
	public boolean isNotificationFinished () {
		return notificationFinished;
	}

	/**
	 * @param notificationFinished the notificationFinished to set
	 */
	public void setNotificationFinished (boolean notificationFinished) {
		this.notificationFinished = notificationFinished;
	}

	/**
	 * @return the recipients
	 */
	public Collection<ArchivedAlertReportRecipient> getRecipients () {
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients (Collection<ArchivedAlertReportRecipient> recipients) {
		this.recipients = recipients;
	}

	/**
	 * @return the lastFetch
	 */
	public Date getLastFetch () {
		return lastFetch;
	}

	/**
	 * @param lastFetch the lastFetch to set
	 */
	public void setLastFetch (Date lastFetch) {
		this.lastFetch = lastFetch;
	}

}
