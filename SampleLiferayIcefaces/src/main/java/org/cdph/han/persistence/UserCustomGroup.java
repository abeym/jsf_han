package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity class representing a custom user group for propagating alerts
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "UserCustomGroup")
@Table (name = "user_cust_groups", schema = "lportal")
@NamedQueries (
	value = {
		@NamedQuery (name = "UserCustomGroup.FindByUser", query = "select g from UserCustomGroup g where g.userData.userId = :userId and g.deleted = false order by g.name"),
		@NamedQuery (name = "UserCustomGroup.FindByUserName", query = "select g from UserCustomGroup g where g.userData.userId = :userId and upper (g.name) like :name and g.deleted = false order by g.name")
	}
)
public class UserCustomGroup implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -2874057804210161944L;
	/** Primary key */
	@Id
	@Column (name = "groupId", nullable = false)
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long groupId;
	/** User data associated to the custom group */
	@ManyToOne
	@JoinColumn (name = "userId", nullable = false)
	private UserData userData;
	/** Name of the custom group */
	@Column (name = "name", nullable = false, length = 40)
	private String name;
	/** Members of the current group */
	@OneToMany (mappedBy = "userCustomGroup")
	private Collection<CustomGroupMember> members;
	/** Group creation date */
	@Column (name = "created_date", nullable = false)
	private Date createdDate;
	/** Last user modifying the group */
	@ManyToOne
	@JoinColumn (name = "modified_by_userId", nullable = true)
	private UserData modifiedBy;
	/** Date of last modification */
	@Column (name = "date_modified")
	private Date dateModified;
	/** Flag for soft delete */
	@Column (name = "deleted")
	private boolean deleted;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof UserCustomGroup) {
			final UserCustomGroup other = (UserCustomGroup) obj;
			equal = other.getGroupId () == groupId;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (groupId).hashCode ();
	}

	/**
	 * @return the groupId
	 */
	public long getGroupId () {
		return groupId;
	}

	/**
	 * @return the userData
	 */
	public UserData getUserData () {
		return userData;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @return the members
	 */
	public Collection<CustomGroupMember> getMembers () {
		return members;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId (long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @param userData the userData to set
	 */
	public void setUserData (UserData userData) {
		this.userData = userData;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @param members the members to set
	 */
	public void setMembers (Collection<CustomGroupMember> members) {
		this.members = members;
	}

	/**
	 * @return the createdDate
	 */
	public Date getCreatedDate () {
		return createdDate;
	}

	/**
	 * @param createdDate the createdDate to set
	 */
	public void setCreatedDate (Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the modifiedBy
	 */
	public UserData getModifiedBy () {
		return modifiedBy;
	}

	/**
	 * @param modifiedBy the modifiedBy to set
	 */
	public void setModifiedBy (UserData modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	/**
	 * @return the dateModified
	 */
	public Date getDateModified () {
		return dateModified;
	}

	/**
	 * @param dateModified the dateModified to set
	 */
	public void setDateModified (Date dateModified) {
		this.dateModified = dateModified;
	}

	/**
	 * @return the deleted
	 */
	public boolean isDeleted () {
		return deleted;
	}

	/**
	 * @param deleted the deleted to set
	 */
	public void setDeleted (boolean deleted) {
		this.deleted = deleted;
	}

}
