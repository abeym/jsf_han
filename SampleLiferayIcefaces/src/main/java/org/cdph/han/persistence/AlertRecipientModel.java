package org.cdph.han.persistence;

import java.util.Collection;

public interface AlertRecipientModel {

	/**
	 * @return the user
	 */
	UserData getUser ();

	/**
	 * @return the nonHanUser
	 */
	NonHanUser getNonHanUser ();

	/**
	 * @return the userCustomGroup
	 */
	UserCustomGroup getUserCustomGroup ();

	/**
	 * @return the organization
	 */
	Organization getOrganization ();

	/**
	 * @return the areaField
	 */
	AreaField getAreaField ();

	/**
	 * @return the role
	 */
	Role getRole ();

	/**
	 * @return the nonHanUserGroup
	 */
	NonHanUserGroup getNonHanUserGroup ();

	/**
	 * @return the notified
	 */
	boolean isNotified ();

	/**
	 * @return the criterias
	 */
	Collection<? extends AlertRecipientCriteriaModel> getCriterias ();

	/**
	 * @return the chicagoOnly
	 */
	boolean isChicagoOnly ();

}