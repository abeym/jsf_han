package org.cdph.han.persistence;

import java.util.Collection;
import java.util.Date;

public interface AlertReportModel {

	/**
	 * @return the id
	 */
	long getId ();

	/**
	 * @return the notificationReportId
	 */
	int getNotificationReportId ();

	/**
	 * @return the completed
	 */
	Date getCompleted ();

	/**
	 * @return the phoneTime
	 */
	Integer getPhoneTime ();

	/**
	 * @return the totalRecipients
	 */
	Integer getTotalRecipients ();

	/**
	 * @return the totalContacted
	 */
	Integer getTotalContacted ();

	/**
	 * @return the totalResponded
	 */
	Integer getTotalResponded ();

	/**
	 * @return the totalCalls
	 */
	Integer getTotalCalls ();

	/**
	 * @return the totalEmails
	 */
	Integer getTotalEmails ();

	/**
	 * @return the totalPages
	 */
	Integer getTotalPages ();

	/**
	 * @return the totalFaxes
	 */
	Integer getTotalFaxes ();

	/**
	 * @return the totalSms
	 */
	Integer getTotalSms ();

	/**
	 * @return the hangUp
	 */
	Integer getHangUp ();

	/**
	 * @return the noAnswer
	 */
	Integer getNoAnswer ();

	/**
	 * @return the busy
	 */
	Integer getBusy ();

	/**
	 * @return the notAtThisLocation
	 */
	Integer getNotAtThisLocation ();

	/**
	 * @return the invalidResponse
	 */
	Integer getInvalidResponse ();

	/**
	 * @return the answeringMachine
	 */
	Integer getAnsweringMachine ();

	/**
	 * @return the leftMessage
	 */
	Integer getLeftMessage ();

	/**
	 * @return the wrongAddressTel
	 */
	Integer getWrongAddressTel ();

	/**
	 * @return the other
	 */
	Integer getOther ();

	/**
	 * @return the notificationFinished
	 */
	boolean isNotificationFinished ();

	/**
	 * @return the recipients
	 */
	Collection<? extends AlertReportRecipientModel> getRecipients ();

	/**
	 * @return the lastFetch
	 */
	Date getLastFetch ();

}