package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Embeddable;

/**
 * Class representing the primary key of the UserOrganizationalData entity
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Embeddable
public class UserOrganizationalDataPK implements Serializable {
	/** Serial version UID */
	private static final long serialVersionUID = 2719239762231379641L;
	/** User Id */
	private Long userId;
	/** Organization id */
	private Long organizationId;

	/**
	 * Empty constructor
	 */
	public UserOrganizationalDataPK () {}

	/**
	 * Constructor
	 * @param userId the user id
	 * @param organizationId the organization id
	 */
	public UserOrganizationalDataPK (final Long userId, final Long organizationId) {
		this.userId = userId;
		this.organizationId = organizationId;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId () {
		return userId;
	}

	/**
	 * @return the organizationId
	 */
	public Long getOrganizationId () {
		return organizationId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId (Long userId) {
		this.userId = userId;
	}

	/**
	 * @param organizationId the organizationId to set
	 */
	public void setOrganizationId (Long organizationId) {
		this.organizationId = organizationId;
	}

}
