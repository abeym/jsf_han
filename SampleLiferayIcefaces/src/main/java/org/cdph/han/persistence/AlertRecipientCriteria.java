package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class representing a criteria for an alert recipient set
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_recipients_criteria", schema = "lportal")
public class AlertRecipientCriteria implements Serializable, AlertRecipientCriteriaModel {
	/** Class serial version UID */
	private static final long serialVersionUID = -2012758430994791011L;
	/** Entity ID */
	@Id
	@Column (name = "criteriaId")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long criteriaId;
	/** Custom member representing the criteria */
	@ManyToOne
	@JoinColumn (name = "recipientId")
	private AlertRecipient recipient;
	/** Organization type */
	@ManyToOne
	@JoinColumn (name = "organizationTypeId")
	private Organization organizationType;
	/** Organization */
	@ManyToOne
	@JoinColumn (name = "organizationId", nullable = true)
    private Organization organization;
	/** Area/Field */
	@ManyToOne
	@JoinColumn (name = "areaFieldId", nullable = true)
    private AreaField areaField;
	/** Role */
	@ManyToOne
	@JoinColumn (name = "roleId", nullable = true)
    private Role role;

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientCriteriaModel#getCriteriaId()
	 */
	public long getCriteriaId () {
		return criteriaId;
	}

	/**
	 * @param criteriaId the criteriaId to set
	 */
	public void setCriteriaId (long criteriaId) {
		this.criteriaId = criteriaId;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientCriteriaModel#getRecipient()
	 */
	public AlertRecipient getRecipient () {
		return recipient;
	}

	/**
	 * @param recipient the recipient to set
	 */
	public void setRecipient (AlertRecipient recipient) {
		this.recipient = recipient;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientCriteriaModel#getOrganizationType()
	 */
	public Organization getOrganizationType () {
		return organizationType;
	}

	/**
	 * @param organizationType the organizationType to set
	 */
	public void setOrganizationType (Organization organizationType) {
		this.organizationType = organizationType;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientCriteriaModel#getOrganization()
	 */
	public Organization getOrganization () {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization (Organization organization) {
		this.organization = organization;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientCriteriaModel#getAreaField()
	 */
	public AreaField getAreaField () {
		return areaField;
	}

	/**
	 * @param areaField the areaField to set
	 */
	public void setAreaField (AreaField areaField) {
		this.areaField = areaField;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertRecipientCriteriaModel#getRole()
	 */
	public Role getRole () {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole (Role role) {
		this.role = role;
	}

}
