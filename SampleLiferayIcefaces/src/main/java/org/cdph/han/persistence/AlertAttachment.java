package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class representing an attachment for the alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_attachments", schema = "lportal")
public class AlertAttachment implements Serializable, AlertAttachmentModel {
	/** Class serial version UID */
	private static final long serialVersionUID = 1929191894973098265L;
	/** Id of the attachment */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which this attachment belongs */
	@ManyToOne
	@JoinColumn (name = "alert_id", nullable = false)
	private Alert alert;
	/** Byte array of the attachment */
	@Lob
	@Column (name = "attachment", nullable = false)
	private byte[] attachment;
	/** MIME type of the attachment */
	@Column (name = "mime_type", nullable = false, length = 100)
	private String mimeType;
	/** The attachment name */
	@Column (name = "name", nullable = false, length = 100)
	private String name;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertAttachment) {
			AlertAttachmentModel other = (AlertAttachmentModel) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#getId()
	 */
	public long getId () {
		return id;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#setId(long)
	 */
	public void setId (long id) {
		this.id = id;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#getAlert()
	 */
	public Alert getAlert () {
		return alert;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#setAlert(org.cdph.han.persistence.AlertModel)
	 */
	public void setAlert (Alert alert) {
		this.alert = alert;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#getAttachment()
	 */
	public byte[] getAttachment () {
		return attachment;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#setAttachment(byte[])
	 */
	public void setAttachment (byte[] attachment) {
		this.attachment = attachment;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#getMimeType()
	 */
	public String getMimeType () {
		return mimeType;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#setMimeType(java.lang.String)
	 */
	public void setMimeType (String mimeType) {
		this.mimeType = mimeType;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#getName()
	 */
	public String getName () {
		return name;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertAttachmentModel#setName(java.lang.String)
	 */
	public void setName (String name) {
		this.name = name;
	}

}
