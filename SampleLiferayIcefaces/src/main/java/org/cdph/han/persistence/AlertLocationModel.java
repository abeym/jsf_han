package org.cdph.han.persistence;

public interface AlertLocationModel {

	/**
	 * @return the id
	 */
	long getId ();

	/**
	 * @return the priority
	 */
	int getPriority ();

	/**
	 * @return the deviceId
	 */
	int getDeviceId ();

}