package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity class representing a recipient of an archived alert
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_recipients_archive", schema = "lportal")
public class ArchivedAlertRecipient implements Serializable, AlertRecipientModel {
	/** Class serial version UID */
	private static final long serialVersionUID = -7717302514239570631L;
	/** Id of the alert recipient */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which this recipient was assigned */
	@ManyToOne
	@JoinColumn (name = "archived_alert_id", nullable = false)
	private ArchivedAlert archivedAlert;
	/** If the recipient is a HAN user this will hold the value */
	@ManyToOne
	@JoinColumn (name = "userId", nullable = true)
	private UserData user;
	/** If the recipient is a non HAN user this will hold the value */
	@ManyToOne
	@JoinColumn (name = "non_han_user_id", nullable = true)
	private NonHanUser nonHanUser;
	/** User custom group */
	@ManyToOne
	@JoinColumn (name = "groupId", nullable = true)
	private UserCustomGroup userCustomGroup;
	/** Organization */
	@ManyToOne
	@JoinColumn (name = "organizationId", nullable = true)
    private Organization organization;
	/** Area/Field */
	@ManyToOne
	@JoinColumn (name = "area_field_id", nullable = true)
    private AreaField areaField;
	/** Role */
	@ManyToOne
	@JoinColumn (name = "roleId", nullable = true)
    private Role role;
	/** Non Han Group */
	@ManyToOne
	@JoinColumn (name = "non_han_user_group_id", nullable = true)
	private NonHanUserGroup nonHanUserGroup;
	/** Criteria of the current member */
	@OneToMany (mappedBy = "recipient", cascade = {CascadeType.ALL})
	private Collection<ArchivedAlertRecipientCriteria> criterias;
	/** Flag for Chicago only entities */
	@Column (name = "chicago_only")
	private boolean chicagoOnly;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof ArchivedAlertRecipient) {
			final ArchivedAlertRecipient other = (ArchivedAlertRecipient) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the archivedAlert
	 */
	public ArchivedAlert getArchivedAlert () {
		return archivedAlert;
	}

	/**
	 * @param archivedAlert the archivedAlert to set
	 */
	public void setArchivedAlert (ArchivedAlert archivedAlert) {
		this.archivedAlert = archivedAlert;
	}

	/**
	 * @return the user
	 */
	public UserData getUser () {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser (UserData user) {
		this.user = user;
	}

	/**
	 * @return the nonHanUser
	 */
	public NonHanUser getNonHanUser () {
		return nonHanUser;
	}

	/**
	 * @param nonHanUser the nonHanUser to set
	 */
	public void setNonHanUser (NonHanUser nonHanUser) {
		this.nonHanUser = nonHanUser;
	}

	/**
	 * @return the userCustomGroup
	 */
	public UserCustomGroup getUserCustomGroup () {
		return userCustomGroup;
	}

	/**
	 * @param userCustomGroup the userCustomGroup to set
	 */
	public void setUserCustomGroup (UserCustomGroup userCustomGroup) {
		this.userCustomGroup = userCustomGroup;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization () {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization (Organization organization) {
		this.organization = organization;
	}

	/**
	 * @return the areaField
	 */
	public AreaField getAreaField () {
		return areaField;
	}

	/**
	 * @param areaField the areaField to set
	 */
	public void setAreaField (AreaField areaField) {
		this.areaField = areaField;
	}

	/**
	 * @return the role
	 */
	public Role getRole () {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole (Role role) {
		this.role = role;
	}

	/**
	 * @return the nonHanUserGroup
	 */
	public NonHanUserGroup getNonHanUserGroup () {
		return nonHanUserGroup;
	}

	/**
	 * @param nonHanUserGroup the nonHanUserGroup to set
	 */
	public void setNonHanUserGroup (NonHanUserGroup nonHanUserGroup) {
		this.nonHanUserGroup = nonHanUserGroup;
	}

	/**
	 * @return the notified
	 */
	public boolean isNotified () {
		return true;
	}

	/**
	 * @return the criterias
	 */
	public Collection<ArchivedAlertRecipientCriteria> getCriterias () {
		return criterias;
	}

	/**
	 * @param criterias the criterias to set
	 */
	public void setCriterias (Collection<ArchivedAlertRecipientCriteria> criterias) {
		this.criterias = criterias;
	}

	/**
	 * @return the chicagoOnly
	 */
	public boolean isChicagoOnly () {
		return chicagoOnly;
	}

	/**
	 * @param chicagoOnly the chicagoOnly to set
	 */
	public void setChicagoOnly (boolean chicagoOnly) {
		this.chicagoOnly = chicagoOnly;
	}

}
