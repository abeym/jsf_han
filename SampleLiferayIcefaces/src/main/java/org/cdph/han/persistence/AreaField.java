package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * Entity class representing an Area/Field in the HAN
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "AreaField")
@Table (name = "area_field_catalog", schema = "lportal")
public class AreaField implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -5771775029096840373L;
	/** Primary Key */
	@Id
	@Column (name = "area_field_id", nullable = false)
	private int areaFieldId;
	/** Description of the Area/Field */
	@Column (name = "description", length = 100)
	private String description;
	/** Organization of the Area/Field */
	@ManyToOne
	@JoinColumn (name = "organization_id", nullable = false)
	private Organization organization;
	/** Users in the Area/Field */
	@OneToMany (mappedBy = "areaField")
	private Collection<UserOrganizationalData> userDatas;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AreaField) {
			final AreaField other = (AreaField) obj;
			equal = other.getAreaFieldId () == areaFieldId;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return areaFieldId;
	}

	/**
	 * @return the areaFieldId
	 */
	public int getAreaFieldId () {
		return areaFieldId;
	}

	/**
	 * @return the description
	 */
	public String getDescription () {
		return description;
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization () {
		return organization;
	}

	/**
	 * @return the users
	 */
	public Collection<UserOrganizationalData> getUserDatas () {
		return userDatas;
	}

	/**
	 * @param areaFieldId the areaFieldId to set
	 */
	public void setAreaFieldId (int areaFieldId) {
		this.areaFieldId = areaFieldId;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription (String description) {
		this.description = description;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization (Organization organization) {
		this.organization = organization;
	}

	/**
	 * @param users the users to set
	 */
	public void setUserDatas (Collection<UserOrganizationalData> userDatas) {
		this.userDatas = userDatas;
	}

}
