package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Entity representing a locale for a message in the HAN System
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_locale", schema = "lportal")
public class AlertLocale implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 8353490575017582809L;
	/** Id of the locale */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private int id;
	/** Value of the locale */
	@Column (name = "value", nullable = false, length = 20)
	private String value;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertLocale) {
			AlertLocale other = (AlertLocale) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return id;
	}

	/**
	 * @return the id
	 */
	public int getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (int id) {
		this.id = id;
	}

	/**
	 * @return the value
	 */
	public String getValue () {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue (String value) {
		this.value = value;
	}

}
