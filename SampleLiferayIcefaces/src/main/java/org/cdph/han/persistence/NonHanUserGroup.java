package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/*import com.icesoft.faces.context.ByteArrayResource;
import com.icesoft.faces.context.Resource;
import com.icesoft.faces.context.StringResource;
*/
@Entity
@Table(name = "non_han_user_group", schema = "lportal")
@NamedQueries (
	value = {
		@NamedQuery (name = "BatchGroup.FindByNameDescAndStatus", query = "Select a from NonHanUserGroup a where a.name LIKE :groupName and a.description LIKE :groupDesc and a.status <> :status ORDER BY a.createdDate DESC"),
		@NamedQuery (name = "BatchGroup.FindByNameDescAndStatusCount", query = "Select count(a) from NonHanUserGroup a where a.name LIKE :groupName and a.description LIKE :groupDesc and a.status <> :status ORDER BY a.createdDate DESC"),
		@NamedQuery (name = "BatchGroup.FindAllExceptSoftDeleted" , query="Select a from NonHanUserGroup a WHERE a.status <> 4 order by a.createdDate desc"),
		@NamedQuery (name = "BatchGroup.GetCount", query="Select count(g) from NonHanUserGroup g  WHERE g.status <> 4")
	}
)
public class NonHanUserGroup implements Serializable {

	private static final long serialVersionUID = -2303754373583401726L;
	
	/** Class logger */
	private static Log log = LogFactory.getLog(NonHanUserGroup.class);

	/** Primary Key */
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	/** Name of the group */
	@Column(name = "name", nullable = false, length = 100)
	private String name;
	/** Description of the Group */
	@Column(name = "description", nullable = false, length = 250)
	private String description;
	/** Description of the Event */
	@Column(name = "eventDescription", nullable = false, length = 250)
	private String eventDescription;
	/** Creation date */
	@Column(name = "createdDate", nullable = false)
	private Date createdDate;
	/** The batch status */
	@Column(name = "status", nullable = false )
	private BatchStatus status;
	/** Creation date */
	@Column(name = "processedDate", nullable = true)
	private Date processedDate;
	/** ErrorLog */
	@Column(name = "errorLog", nullable = true)
	private String errorLog;
	
	
	/** Users in the Non-Han Group */
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "nonHanGroup")
	private Collection<NonHanUser> nonHanUsers;
	/** Id of the user that created the group */
	@ManyToOne
	@JoinColumn(name = "userId", nullable = false)
	private UserData user;

	/** Path to uploaded file */
	@Lob
	@Column(name = "file", nullable = false)
	private byte[] file;

	/** Path to uploaded file */
	@Column(name = "preferences", nullable = false)
	private String contactPreferences;
	
	/** Flag to indicated the icefaces datatable if this row is selected	 */
	@Transient 
	private boolean selected;
	
	/** Flag to indicated the icefaces datatable if this row is download button is rendered	 */
	@Transient 
	private boolean downloadButtonRendered;

	/** Flag to indicated the icefaces datatable if this row is delete button is rendered	 */
	@Transient 
	private boolean deleteButtonRendered;
	
	/** Flag to indicated the icefaces datatable if this row is the error log rendered	 */
	@Transient 
	private boolean errorLogRendered;
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEventDescription() {
		return eventDescription;
	}

	public void setEventDescription(String eventDescription) {
		this.eventDescription = eventDescription;
	}

	public Collection<NonHanUser> getNonHanUsers() {
		return nonHanUsers;
	}

	public void setNonHanUsers(Collection<NonHanUser> nonHanUsers) {
		this.nonHanUsers = nonHanUsers;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	/**
	 * @return the user
	 */
	public UserData getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(UserData user) {
		this.user = user;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(byte[] file) {
		this.file = file;
	}
	
	public String getContactPreferencesAbbreviated() {
		StringTokenizer st = new StringTokenizer(contactPreferences,",");
		StringBuffer sb = new StringBuffer();
		
		while(st.hasMoreTokens()){
			String token = st.nextToken();
			sb.append(ContactDeviceType.getAbbreviation(token));
			sb.append(", ");
		}
		return sb.substring(0, (sb.length()-2));
	}

	public String getContactPreferences() {
		return contactPreferences;
	}

	public void setContactPreferences(String contactPreferences) {
		this.contactPreferences = contactPreferences;
	}

	public void setContactPrefList(List<ContactDeviceType> prefs) {
		contactPreferences = "";
		for (ContactDeviceType contactDeviceType : prefs) {
			contactPreferences += contactDeviceType + ",";
		}
		contactPreferences = contactPreferences.substring(0, contactPreferences.lastIndexOf(','));
	}

	public BatchStatus getStatus() {
		return status;
	}

	public void setStatus(BatchStatus status) {
		this.status = status;
	}

	public Date getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}

	/*public Resource getFileResource() {
		
		Resource resource = null;

		if(file != null){
			resource = new ByteArrayResource(file);
		}

		return resource;
	}*/

	public boolean isDownloadButtonRendered() {
		if(status == null) return false;		
		return (status != BatchStatus.SOFT_DELETED && status != BatchStatus.PROCESSING);
	}


	public boolean isDeleteButtonRendered() {
		if(status == null) return false;
		return (status != BatchStatus.SOFT_DELETED && status != BatchStatus.PROCESSING);
	}

	public String getErrorLog() {
		return errorLog;
	}

	public void setErrorLog(String errorLog) {
		this.errorLog = errorLog;
	}
	
	/*public Resource getErrorLogResource() {
		
		Resource resource = null;

		if(file != null){
			resource = new StringResource(errorLog);
		}

		return resource;
	}*/	

	public boolean isErrorLogRendered() {
		if(status == null) return false;
		return (status == BatchStatus.ERROR);
	}
	
	public String getStatusImagePath(){
		return status.getStatusImagePath();
	}

}
