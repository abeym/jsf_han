package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity representing an option of the alert in the HAN system
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_options", schema = "lportal")
public class AlertOption implements Serializable, AlertOptionModel {
	/** Class serial version UID */
	private static final long serialVersionUID = 3940158134653844507L;
	/** Id of the alert option */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which this option belongs */
	@ManyToOne
	@JoinColumn (name = "alert_id", nullable = false)
	private Alert alert;
	/** Locale of the message */
	@ManyToOne
	@JoinColumn (name = "alert_locale_id", nullable = false)
	private AlertLocale locale;
	/** Message of the option */
	@Column (name = "message", length = 100, nullable = false)
	private String message;
	/** Action to perform in the option */
	@Column (name = "action_id")
	private int action;
	/** Cascade of the option */
	@Column (name = "cascade_id")
	private long cascade;
	/** Option ID in mir3 site */
	@Column (name = "option_id")
	private int optionId;
	/** The phonenumber to use in the call bridge */
	@Column (name = "call_bridge", length = 40)
	private String callBridge;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertOption) {
			final AlertOptionModel other = (AlertOptionModel) obj;
			equal = other.getId () == id;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (id).hashCode ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertOptionModel#getId()
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the alert
	 */
	public Alert getAlert () {
		return alert;
	}

	/**
	 * @param alert the alert to set
	 */
	public void setAlert (Alert alert) {
		this.alert = alert;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertOptionModel#getLocale()
	 */
	public AlertLocale getLocale () {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale (AlertLocale locale) {
		this.locale = locale;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertOptionModel#getMessage()
	 */
	public String getMessage () {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage (String message) {
		this.message = message;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertOptionModel#getAction()
	 */
	public int getAction () {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction (int action) {
		this.action = action;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertOptionModel#getCascade()
	 */
	public long getCascade () {
		return cascade;
	}

	/**
	 * @param cascade the cascade to set
	 */
	public void setCascade (long cascade) {
		this.cascade = cascade;
	}

	/**
	 * @return the optionId
	 */
	public int getOptionId () {
		return optionId;
	}

	/**
	 * @param optionId the optionId to set
	 */
	public void setOptionId (int optionId) {
		this.optionId = optionId;
	}

	/**
	 * @return the callBridge
	 */
	public String getCallBridge () {
		return callBridge;
	}

	/**
	 * @param callBridge the callBridge to set
	 */
	public void setCallBridge (String callBridge) {
		this.callBridge = callBridge;
	}

}
