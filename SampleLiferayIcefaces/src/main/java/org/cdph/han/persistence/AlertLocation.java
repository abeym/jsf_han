package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Entity class representing a location override
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity
@Table (name = "alert_locations", schema = "lportal")
public class AlertLocation implements Serializable, AlertLocationModel {
	/** Class serial version UID */
	private static final long serialVersionUID = 4496738022118154482L;
	/** Id of the location */
	@Id
	@Column (name = "id")
	@GeneratedValue (strategy = GenerationType.TABLE)
	private long id;
	/** Alert to which the location belongs */
	@ManyToOne
	@JoinColumn (name = "alert_id", nullable = false)
	private Alert alert;
	/** The priority of the device */
	@Column (name = "priority", nullable = false)
	private int priority;
	/** Id of the device */
	@Column (name = "device_id", nullable = false)
	private int deviceId;

	public AlertLocation () {
		
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((alert == null) ? 0 : alert.hashCode ());
		result = prime * result + deviceId;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + priority;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass () != obj.getClass ())
			return false;
		final AlertLocation other = (AlertLocation) obj;
		if (alert == null) {
			if (other.alert != null)
				return false;
		} else if (!alert.equals (other.alert))
			return false;
		if (deviceId != other.deviceId)
			return false;
		if (id != other.id)
			return false;
		if (priority != other.priority)
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertLocationModel#getId()
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the alert
	 */
	public Alert getAlert () {
		return alert;
	}

	/**
	 * @param alert the alert to set
	 */
	public void setAlert (Alert alert) {
		this.alert = alert;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertLocationModel#getPriority()
	 */
	public int getPriority () {
		return priority;
	}

	/**
	 * @param priority the priority to set
	 */
	public void setPriority (int priority) {
		this.priority = priority;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.persistence.AlertLocationModel#getDeviceId()
	 */
	public int getDeviceId () {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId (int deviceId) {
		this.deviceId = deviceId;
	}

}
