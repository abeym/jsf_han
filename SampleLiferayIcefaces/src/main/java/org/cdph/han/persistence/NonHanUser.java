package org.cdph.han.persistence;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name = "non_han_user", schema = "lportal")
@NamedQueries (
	value = {
		@NamedQuery (name = "NonHanUser.FindByGroupId", query = "Select u from NonHanUser u WHERE u.nonHanGroup.id=:groupId"),
		@NamedQuery (name = "NonHanUser.GroupMembersCount" , query = "Select count(u) from NonHanUser u WHERE u.nonHanGroup.id=:groupId")
	}
)
public class NonHanUser implements Serializable {
	
	private static final long serialVersionUID = -2917531848834102417L;
	
	/** Primary Key */
	@Id
	@Column(name = "id", nullable = false)
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long id;
	/** User first name */
	@Column(name = "lastName", nullable = false , length = 100)
	private String lastName;
	/** User last name */
	@Column(name = "firstName", nullable = false , length = 100)
	private String firstName;
	/** User middle initial */
	@Column(name = "middleInitial", length = 1)
	private String middleInitial;
	
	/** User address line 1 */
	@Column(name = "addressLine1", nullable = false , length = 250)
	private String addressLine1;
	/** User address line 2 */
	@Column(name = "addressLine2", length = 250)
	private String addressLine2;
	/** User city */
	@Column(name = "city", length = 100)
	private String city;
	/** User state */
	@Column(name = "state", length = 50)
	private String state;
	/** User zip */
	@Column(name = "zip", length = 10)
	private String zip;
		
	/** Non-Han Group of the user */
	@ManyToOne
	@JoinColumn (name = "non_han_group_id", nullable=false)
	private NonHanUserGroup nonHanGroup;
	
	/** User contact devices */
	@OneToMany(mappedBy = "nonHanUser" , cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@OrderBy ("priority asc")
	private Collection<NonHanUserContactDevice> nonHanContactDevices = new ArrayList<NonHanUserContactDevice>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	public NonHanUserGroup getNonHanGroup() {
		return nonHanGroup;
	}

	public void setNonHanGroup(NonHanUserGroup nonHanGroup) {
		this.nonHanGroup = nonHanGroup;
	}

	public Collection<NonHanUserContactDevice> getNonHanContactDevices() {
		return nonHanContactDevices;
	}

	public void setNonHanContactDevices(
			Collection<NonHanUserContactDevice> nonHanContactDevices) {
		this.nonHanContactDevices = nonHanContactDevices;
	}

}
