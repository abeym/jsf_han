package org.cdph.han.persistence;

import java.io.Serializable;

import javax.persistence.*;

/**
 * This class contains the email address associated to the given user if any.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Entity (name = "EmailAddress")
@Table (name = "email_addresses", schema = "lportal")
public class EmailAddress implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -6878832852316756120L;
	/** Id of the email address */
	@Id
	@Column (name = "emailAddressId", nullable = false, insertable = false, updatable = false)
	private long emailId;
	/** Email address */
	@Column (name = "address", nullable = false, insertable = false, updatable = false)
	private String address;
	/** Friendly name of the email address */
	@Column (name = "description", insertable = false, updatable = false)
	private String friendlyName;
	/** User data */
	@ManyToOne
	@JoinColumn (name = "classPK", nullable = false)
	private Contact contact;
	/** The type ID of the email */
	@Column (name = "typeId")
	private Long typeId;

	/**
	 * @return the emailId
	 */
	public long getEmailId () {
		return emailId;
	}

	/**
	 * @return the address
	 */
	public String getAddress () {
		return address;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId (long emailId) {
		this.emailId = emailId;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress (String address) {
		this.address = address;
	}

	/**
	 * @return the friendlyName
	 */
	public String getFriendlyName () {
		return friendlyName;
	}

	/**
	 * @param friendlyName the friendlyName to set
	 */
	public void setFriendlyName (final String friendlyName) {
		this.friendlyName = friendlyName;
	}

	/**
	 * @return the typeId
	 */
	public Long getTypeId () {
		return typeId;
	}

	/**
	 * @param typeId the typeId to set
	 */
	public void setTypeId (Long typeId) {
		this.typeId = typeId;
	}

	/**
	 * @return the contact
	 */
	public Contact getContact () {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact (Contact contact) {
		this.contact = contact;
	}

}
