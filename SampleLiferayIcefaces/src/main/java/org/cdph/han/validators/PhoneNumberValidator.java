package org.cdph.han.validators;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

public class PhoneNumberValidator extends HanValidator{
		
	/** phone number in form of xxx-xxx-xxxx */
	private static final String PHONE_NUM = "\\d{3}[-]?\\d{3}[-]?\\d{4}";
	
	private Pattern mask;

	public void validate(FacesContext context, UIComponent component,
			Object value) throws ValidatorException {
		/* create a mask */
		if(mask == null){
			mask = Pattern.compile(PHONE_NUM);
		}

		/* retrieve the string value of the field */
		String phoneNumber = (String) value;

		/* check to ensure that the value is a phone number */
		Matcher matcher = mask.matcher(phoneNumber);

		if (!matcher.matches()) {

			FacesMessage msg = new FacesMessage();
			msg.setDetail(getErrorMessage("phone.validator.invalid.format"));
			msg.setSummary(getErrorMessage("phone.validator.invalid.format"));
			msg.setSeverity(FacesMessage.SEVERITY_ERROR);
			throw new ValidatorException(msg);
		}

	}

}
