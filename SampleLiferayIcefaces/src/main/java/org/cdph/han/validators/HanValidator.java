package org.cdph.han.validators;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

public abstract class HanValidator implements Validator {

	private ResourceBundle bundle = null;
	
	protected String getErrorMessage(String key){
		if(bundle == null){
			bundle = ResourceBundle.getBundle("messages",
				FacesContext.getCurrentInstance().getViewRoot().getLocale());
		}
		return bundle.getString(key);
	}
	
	protected FacesMessage createFacesMessage(String key){
		FacesMessage msg = new FacesMessage();
		msg.setDetail(getErrorMessage(key));
		msg.setSummary(getErrorMessage(key));
		msg.setSeverity(FacesMessage.SEVERITY_ERROR);
		
		return msg;
	}

}
