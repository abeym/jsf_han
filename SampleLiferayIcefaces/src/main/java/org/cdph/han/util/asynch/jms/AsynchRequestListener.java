package org.cdph.han.util.asynch.jms;

import java.io.Serializable;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.AlertActionsService;
import org.cdph.han.util.asynch.dto.AsynchRequestTO;
import org.cdph.han.util.asynch.dto.AsynchResponseTO;
import org.cdph.han.util.asynch.services.AsynchRequestType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jms.core.JmsTemplate;

/**
 * This JMS listener will execute the request that are placed in the queue, these request are more likely to be
 * webservices calls
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AsynchRequestListener implements MessageListener {
	/** Class log */
	private static final Log log = LogFactory.getLog (AsynchRequestListener.class);
	/** JMS Template to use to send messages back */
	//abey...
	//@Autowired
	//@Qualifier ("hanAsynchRes")
	private JmsTemplate jmsTemplate;
	/** Service for retrieving alerts that requires an action */
	//abey...
	//@Autowired
	private AlertActionsService alertActionsService;

	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	@Override
	public void onMessage (final Message message) {
		// Check that the message is of the correct type
		if (message instanceof ObjectMessage) {
			final ObjectMessage objectMessage = (ObjectMessage) message;
			try {
				final Object o = objectMessage.getObject ();
				if (o instanceof AsynchRequestTO) {
					final AsynchRequestTO request = (AsynchRequestTO) o;
					final AsynchResponseTO response = processRequest (request);
					final ResponseMessageCreator creator = new ResponseMessageCreator (
							response, message.getJMSCorrelationID ());
					jmsTemplate.send (creator);
				} else {
					log.error ("Received object not supported: " + o.getClass ().getName ());
				}
			} catch (JMSException ex) {
				log.error ("Error unwrapping request.", ex);
			} catch (Exception ex) {
				log.error ("Error performing operation.", ex);
			}
		} else {
			// Log that the received message is not of the expected type
			log.error ("Unexpected message type: " + message.getClass ().getName ());
		}
	}

	/**
	 * Process the request using the request type to determine the proper 
	 * @param request with the parameters of the query
	 * @return response object with the result of the query
	 */
	private AsynchResponseTO processRequest (final AsynchRequestTO request) {
		AsynchResponseTO response = null;
		log.info ("Requested service: " + request.getRequestType ().toString ());
		if (request.getRequestType ().equals (AsynchRequestType.ALERTS_REQUIRING_ACTION)) {
			final long userId = Long.parseLong ((String) request.getRequest ().get ("USER_ID"));
			response = new AsynchResponseTO (null);
			response.getResult ().put ("FOUND_ALERTS",
					(Serializable) alertActionsService.alertsRequiringAction (userId));
		} else if (request.getRequestType ().equals (AsynchRequestType.ALERT_REQUIRES_ACTION)) {
			final long alertId = (Long) request.getRequest ().get ("ALERT_ID");
			final long userId = Long.parseLong ((String) request.getRequest ().get ("USER_ID"));
			final boolean oneOption = (Boolean) request.getRequest ().get ("ONE_OPTION");
			log.info ("AlertID: " + alertId + " UserID: " + userId + " oneOption: " + oneOption);
			response = new AsynchResponseTO (null);
			final Integer[] ids = alertActionsService.checkForRequiredAction (alertId, userId);
			log.info ("Got Ids? " + ids != null);
			if (ids != null && oneOption) {
				alertActionsService.respondAction (ids[0], ids[1], 1);
			} else {
				response.getResult ().put ("NOTIFICATION_REPORT_ID", ids != null ? ids [0] : null);
				response.getResult ().put ("RECIPIENT_REPORT_ID", ids != null ? ids [1] : null);
			}
		}
		return response;
	}

}
