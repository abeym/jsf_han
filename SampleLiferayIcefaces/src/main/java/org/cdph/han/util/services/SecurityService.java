package org.cdph.han.util.services;

/**
 * This interface defines utility services for security purposes
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface SecurityService {

	/**
	 * The implementation of this method should check for the users profiles to allow administration
	 * @return if the user has enough privileges
	 */
	boolean isUserAdmin ();

	/**
	 * The implementation of this methos must return the type of role that the user must have at mir3
	 * @param userId ID of the user to check
	 * @return the role for mir3
	 */
	String getMir3UserRole (final long userId);

}
