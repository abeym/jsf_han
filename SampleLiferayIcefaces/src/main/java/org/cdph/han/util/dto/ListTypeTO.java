package org.cdph.han.util.dto;

/**
 * Transfer object used to transfer the list types information to the presentation layer
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ListTypeTO {
	/** The id of the type */
	private long id;
	/** The name to display to the user */
	private String name;
	/** The type name */
	private String type;

	/**
	 * Default constructor
	 */
	public ListTypeTO () {
	}

	/**
	 * Constructor to set all fields
	 * @param id of the type
	 * @param name to display to the user
	 * @param type the name of the type
	 */
	public ListTypeTO (final long id, final String name, final String type) {
		this.id = id;
		this.name = name;
		this.type = type;
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType () {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType (String type) {
		this.type = type;
	}

}
