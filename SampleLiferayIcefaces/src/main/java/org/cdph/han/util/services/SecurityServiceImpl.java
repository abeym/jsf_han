package org.cdph.han.util.services;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.cdph.han.persistence.Role;
import org.cdph.han.persistence.UserData;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of the SecurityService that uses JSF to retrieve the user profiles
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
//abey commneting to deploy
//@Repository
//@Service ("securityService")
public class SecurityServiceImpl implements SecurityService {
	/** Name of the administrator role */
	private static final String ADMINISTRATOR_ROLE = "Administrator";
	/** Name of the super user role */
	private static final String SUPER_USER_ROLE = "Super User";
	/** Persistence context */
	private EntityManager entityManager;

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.SecurityService#isUserAdmin()
	 */
	//abey ...@Transactional (readOnly = true, propagation = Propagation.SUPPORTS)
	public boolean isUserAdmin () {
		boolean admin = false;
		final String userIdString = FacesContext.getCurrentInstance ().getExternalContext (
				).getUserPrincipal ().getName ();
		if (userIdString != null) {
			final long userId = Long.parseLong (userIdString);
			final UserData currentUser = entityManager.find (UserData.class, userId);
			for (Role role : currentUser.getGroupRoles ()) {
				if (role.getName ().equals (ADMINISTRATOR_ROLE) || role.getName ().equals (SUPER_USER_ROLE)
						|| role.getName ().equals ("HAN Alert Initiator")) {
					admin = true;
					break;
				}
			}
			if (!admin) {
				for (Role role : currentUser.getRoles ()) {
					if (role.getName ().equals (ADMINISTRATOR_ROLE) || role.getName ().equals (SUPER_USER_ROLE)
							|| role.getName ().equals ("HAN Alert Initiator")) {
						admin = true;
						break;
					}
				}
			}
		}
		return admin;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.util.services.SecurityService#getMir3UserRole(long)
	 */
	@Override
	public String getMir3UserRole (long userId) {
		String mir3Role = null;
		final UserData currentUser = entityManager.find (UserData.class, userId);
		for (Role role : currentUser.getGroupRoles ()) {
			if ((role.getName ().equals (ADMINISTRATOR_ROLE) || role.getName ().equals (SUPER_USER_ROLE))
					&& role.getType () == 1) {
				mir3Role = "Administrator";
			} else if (role.getName ().equals ("HAN Alert Initiator")) {
				mir3Role = "Initiator";
				break;
			}
		}
		if (mir3Role == null) {
			for (Role role : currentUser.getRoles ()) {
				if ((role.getName ().equals (ADMINISTRATOR_ROLE) || role.getName ().equals (SUPER_USER_ROLE))
						&& role.getType () == 1) {
					mir3Role = "Administrator";
				} else if (role.getName ().equals ("HAN Alert Initiator")) {
					mir3Role = "Initiator";
					break;
				}
			}
		}
		if (mir3Role == null) {
			mir3Role = "Recipient";
		}
		return mir3Role;
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey commneting to deploy
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
