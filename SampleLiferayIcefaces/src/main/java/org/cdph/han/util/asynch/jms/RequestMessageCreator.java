package org.cdph.han.util.asynch.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;

import org.cdph.han.util.asynch.dto.AsynchRequestTO;
import org.springframework.jms.core.MessageCreator;

/**
 * Class to create messages for requesting asynchronous queries
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class RequestMessageCreator implements MessageCreator {
	/** The request to add to the message */
	private AsynchRequestTO request;

	/**
	 * Constructor to set the request object
	 * @param request with the parameters of the query
	 */
	public RequestMessageCreator (final AsynchRequestTO request) {
		this.request = request;
	}

	/* (non-Javadoc)
	 * @see org.springframework.jms.core.MessageCreator#createMessage(javax.jms.Session)
	 */
	@Override
	public Message createMessage (final Session session) throws JMSException {
		final ObjectMessage message = session.createObjectMessage (request);
		message.setJMSCorrelationID (request.getCorrelationID ());
		return message;
	}

	/**
	 * @return the correlationId
	 */
	public String getCorrelationID () {
		return request.getCorrelationID ();
	}

}
