package org.cdph.han.util.asynch.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.util.asynch.dto.AsynchResponseTO;
import org.cdph.han.util.asynch.services.AsynchRefreshService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * JMS listener that uses the asynchronous update service to propagate updates to several users using AJAX Push
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AsynchRefreshListener implements MessageListener {
	/** Class logger */
	private static final Log log = LogFactory.getLog (AsynchRefreshListener.class);
	/** Asynchronous refresh service */
	//abey...
	//@Autowired
	private AsynchRefreshService asynchRefreshService;

	/* (non-Javadoc)
	 * @see javax.jms.MessageListener#onMessage(javax.jms.Message)
	 */
	@Override
	public void onMessage (final Message message) {
		// Check that the message is of the correct type and descard any other message
		log.info ("Received message.");
		if (message instanceof ObjectMessage) {
			final ObjectMessage objectMessage = (ObjectMessage) message;
			try {
				final Object o = objectMessage.getObject ();
				if (o instanceof AsynchResponseTO) {
					final AsynchResponseTO response = (AsynchResponseTO) o;
					asynchRefreshService.propagateUpdate (message.getJMSCorrelationID (),
							response.getGroupId (), response);
				}
			} catch (JMSException ex) {
				log.error ("Error while unwrapping response object from message.", ex);
			}
		} else {
			log.error ("Received message of wrong type: " + message.getClass ().getName ());
		}
	}

}
