package org.cdph.han.util.services;

/**
 * Interface to expose operations on the HAN system properties.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface HanPropertiesService {

	/**
	 * Retrieves the value of the given property
	 * @param name String with the name of the property
	 * @return String with the value of the requested property, null if not found
	 */
	String getValue (final String name);

	/**
	 * Inserts a new property in the database
	 * @param name String with the name of the new property
	 * @param value String with the value of the new property
	 */
	void insertProperty (final String name, final String value);

	/**
	 * Updates a property in the database using the provided name as the search parameter
	 * @param name String with the name of the property to update
	 * @param value String with the new value
	 */
	void updateProperty (final String name, final String value);

	/**
	 * Deletes a property from the HAN database
	 * @param name String with the name of the property
	 */
	void deleteProperty (final String name);

}
