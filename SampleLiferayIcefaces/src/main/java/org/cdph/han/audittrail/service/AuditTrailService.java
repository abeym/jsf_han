package org.cdph.han.audittrail.service;

import java.util.List;

import org.cdph.han.audittrail.dto.AuditTrailDTO;
import org.cdph.han.audittrail.dto.AuditTrailTO;

public interface AuditTrailService {

	public Integer getAuditTrailCount(AuditTrailDTO criteria);

	public List<AuditTrailTO> getAuditTrailRecords(AuditTrailDTO criteria,
			int startIndex, int maxResults);

}
