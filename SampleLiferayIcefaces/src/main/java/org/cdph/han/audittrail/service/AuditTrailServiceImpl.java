package org.cdph.han.audittrail.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.audittrail.dto.AuditTrailDTO;
import org.cdph.han.audittrail.dto.AuditTrailTO;
import org.cdph.han.persistence.AuditTrail;
import org.cdph.han.persistence.DeletedUser;
import org.cdph.han.persistence.UserData;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Service("auditTrailService")
public class AuditTrailServiceImpl implements AuditTrailService {

	/** Class logger */
	private static Log log = LogFactory.getLog(AuditTrailServiceImpl.class);

	private EntityManager em;

	//abey ...
	//@PersistenceContext
	public void setEntityManager(EntityManager em) {
		this.em = em;
	}

	//abey ...@Transactional(readOnly = true)
	public Integer getAuditTrailCount(AuditTrailDTO criteria) {

		Query query = createQuery(criteria, true);

		return ((Number) query.getSingleResult()).intValue();
	}

	@SuppressWarnings("unchecked")
	//abey ...@Transactional(readOnly = true)
	public List<AuditTrailTO> getAuditTrailRecords(AuditTrailDTO criteria,
			int startIndex, int maxResults) {

		Query query = createQuery(criteria, false);
		final List<AuditTrailTO> result = new ArrayList<AuditTrailTO> ();
		final List<AuditTrail> dbResult = query.setFirstResult (startIndex
				).setMaxResults (maxResults).getResultList ();
		AuditTrailTO dto;
		UserData userData;
		DeletedUser deletedUser;
		for (AuditTrail trail : dbResult) {
			dto = new AuditTrailTO ();
			dto.setActionPerformed (trail.getActionPerformed ());
			dto.setAlertId (trail.getAlertId ());
			dto.setDatePerformed (trail.getDatePerformed ());
			userData = em.find (UserData.class, trail.getUser ());
			if (userData != null) {
				dto.setUser (userData.getEmail ());
			} else {
				deletedUser = em.find (DeletedUser.class, trail.getUser ());
				if (deletedUser == null) {
					dto.setUser ("User Deleted (ID: " + trail.getUser () + ")");
				} else {
					dto.setUser ("User Deleted (" + deletedUser.getEmail () + ")");
				}
			}
			result.add (dto);
		}

		return result;
	}

	private Query createQuery(AuditTrailDTO criteria, boolean count){
		
		StringBuffer queryStr = new StringBuffer();
		
		if(count){
			queryStr.append("Select count(a) from AuditTrail a");
		}else{
			queryStr.append("Select a from AuditTrail a");
		}

		StringBuffer params = new StringBuffer();
		
		boolean[] paramPresent = new boolean[5];
		int paramCount = 0;

		if (criteria.getStartDate() != null) {
			params.append(" a.datePerformed >= ?" + ++paramCount);
			paramPresent[0] = true;
		}

		if (criteria.getEndDate() != null) {
			if(params.length() > 0){
				params.append(" AND");
			}
			params.append(" a.datePerformed < ?" + ++paramCount);
			paramPresent[1] = true;
		}

		if (!emptyString(criteria.getPerformedBy())) {
			if(params.length() > 0){
				params.append(" AND");
			}
			params.append(" a.user IN (Select u.userId from UserData u where u.email LIKE ?" + ++paramCount + ")");
			paramPresent[2] = true;
		}

		if (criteria.getActionPerformed() != null) {
			if(params.length() > 0){
				params.append(" AND");
			}
			params.append(" a.actionPerformed LIKE ?" + ++paramCount);
			paramPresent[3] = true;
		}

		if (!emptyString(criteria.getResult())) {
			if(params.length() > 0){
				params.append(" AND");
			}
			params.append(" a.result LIKE ?" + ++paramCount);
			paramPresent[4] = true;
		}

		if(params.length() > 0){
			params.insert(0, " WHERE ");
		}
		
		queryStr.append(params.toString());
		
		queryStr.append(" ORDER BY a.datePerformed DESC");

		log.debug(queryStr);
		
		Query query = em.createQuery(queryStr.toString());

		paramCount = 0;
		
		if (paramPresent[0]) {
			log.debug("Start Date: " + criteria.getStartDate());
			query.setParameter(++paramCount, criteria.getStartDate(), TemporalType.DATE);
		}
		if (paramPresent[1]) {
			Calendar cal = new GregorianCalendar();
			cal.setTime(criteria.getEndDate());
			cal.add(Calendar.DAY_OF_YEAR, 1);	
			log.debug("End Date: " + cal.getTime());
			query.setParameter(++paramCount, cal.getTime(), TemporalType.DATE);
		}
		if (paramPresent[2]) {
			query.setParameter(++paramCount, criteria.getPerformedBy());
		}
		if (paramPresent[3]) {
			query.setParameter(++paramCount, criteria.getActionPerformed());
		}
		if (paramPresent[4]) {
			query.setParameter(++paramCount, criteria.getResult());
		}
		
		return query;
		
	}

	private boolean emptyString(String value) {
		return (value == null || value.trim().length() == 0);
	}

}
