package org.cdph.han.audittrail.beans;

import java.util.Date;

import javax.faces.component.UIInput;
import javax.faces.component.UISelectOne;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.audittrail.dto.AuditTrailDTO;
import org.cdph.han.audittrail.service.AuditTrailService;
import org.cdph.han.beans.BaseBean;
import org.cdph.han.persistence.AuditTrailAction;

public class AuditTrailBean extends BaseBean {

	/** Class logger */
	private static final Log log = LogFactory.getLog(AuditTrailBean.class);

	private Date startDate;
	private Date endDate;
	private String performedBy;
	private AuditTrailAction actionPerformed;
	private String result;

	private UIInput startDateComp;
	private UIInput endDateComp;
	private UIInput performedByComp;
	private UISelectOne actionPerformedComp;

	private SelectItem[] actions = {
			new SelectItem(null, ""),
			new SelectItem(AuditTrailAction.SUCCESSFUL_LOGIN,
					AuditTrailAction.SUCCESSFUL_LOGIN.getDisplayFormat()),
			new SelectItem(AuditTrailAction.FAILED_LOGIN,
					AuditTrailAction.FAILED_LOGIN.getDisplayFormat()),
			new SelectItem(AuditTrailAction.USER_LOCKOUT,
					AuditTrailAction.USER_LOCKOUT.getDisplayFormat()),
			new SelectItem(AuditTrailAction.SESSION_DESTROYED,
					AuditTrailAction.SESSION_DESTROYED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_ARCHIVED,
					AuditTrailAction.ALERT_ARCHIVED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_DELETED,
					AuditTrailAction.ALERT_DELETED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_MODIFIED,
					AuditTrailAction.ALERT_MODIFIED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_PUBLISHED,
					AuditTrailAction.ALERT_PUBLISHED.getDisplayFormat()),
			new SelectItem(AuditTrailAction.ALERT_SCHEDULED,
					AuditTrailAction.ALERT_SCHEDULED.getDisplayFormat())

	};

	private AuditTrailDTO searchResults;

	private AuditTrailService service;

	/**
	 * Listener for the form search button click action.
	 * 
	 * @param e
	 *            click action event.
	 */
	public void searchButtonListener(ActionEvent e) {
		log.debug("In searchButtonListener...");

		if (startDate != null && endDate != null) {
			if (startDate.after(endDate)) {
				addFacesMessage(startDateComp.getClientId(FacesContext
						.getCurrentInstance()),
						"audittrail.search.dates.error1");
			}
			if (endDate.before(startDate)) {
				addFacesMessage(endDateComp.getClientId(FacesContext
						.getCurrentInstance()),
						"audittrail.search.dates.error2");
				return;
			}
		}

		searchResults.clearCache();

		searchResults.setStartDate(startDate);
		searchResults.setEndDate(endDate);
		searchResults.setPerformedBy(performedBy);
		searchResults.setActionPerformed(actionPerformed);
		searchResults.setResult(result);

	}

	/**
	 * Listener for the form clear button click action.
	 * 
	 * @param e
	 *            click action event.
	 */
	public void clearButtonListener(ActionEvent e) {
		log.debug("In clearButtonListener...");
		searchResults.clearCache();

		startDate = null;
		endDate = null;
		performedBy = null;
		actionPerformed = null;
		result = null;

		startDateComp.resetValue();
		endDateComp.resetValue();
		performedByComp.resetValue();
		actionPerformedComp.resetValue();

		searchResults.setStartDate(startDate);
		searchResults.setEndDate(endDate);
		searchResults.setPerformedBy(performedBy);
		searchResults.setActionPerformed(actionPerformed);
		searchResults.setResult(result);

	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getPerformedBy() {
		return performedBy;
	}

	public void setPerformedBy(String performedBy) {
		this.performedBy = performedBy;
	}

	public AuditTrailAction getActionPerformed() {
		return actionPerformed;
	}

	public void setActionPerformed(AuditTrailAction actionPerformed) {
		this.actionPerformed = actionPerformed;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public AuditTrailDTO getSearchResults() {
		if (searchResults == null) {
			searchResults = new AuditTrailDTO(service);
		}
		return searchResults;
	}

	public void setSearchResults(AuditTrailDTO searchResults) {
		this.searchResults = searchResults;
	}

	public void setService(AuditTrailService service) {
		this.service = service;
	}

	public SelectItem[] getActions() {
		return actions;
	}

	public void setStartDateComp(UIInput startDateComp) {
		this.startDateComp = startDateComp;
	}

	public void setEndDateComp(UIInput endDateComp) {
		this.endDateComp = endDateComp;
	}

	public UIInput getStartDateComp() {
		return startDateComp;
	}

	public UIInput getEndDateComp() {
		return endDateComp;
	}

	public UIInput getPerformedByComp() {
		return performedByComp;
	}

	public void setPerformedByComp(UIInput performedByComp) {
		this.performedByComp = performedByComp;
	}

	public UISelectOne getActionPerformedComp() {
		return actionPerformedComp;
	}

	public void setActionPerformedComp(UISelectOne actionPerformedComp) {
		this.actionPerformedComp = actionPerformedComp;
	}

}
