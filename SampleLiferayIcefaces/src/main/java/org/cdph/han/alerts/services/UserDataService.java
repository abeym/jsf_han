package org.cdph.han.alerts.services;

import java.util.List;

import org.cdph.han.custgroups.dto.UserDisplayTO;
import org.cdph.han.persistence.UserData;

/**
 * This interface defines the minimum methods required to retrieve user information for alert creation
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface UserDataService {

	/**
	 * Retrieves the current user information.
	 * @return UserData with the current user information
	 */
	UserData getCurrentUserData ();

	/**
	 * Retrieves all users that have a role to initiate alerts
	 * @return <code>List<UserData></code> with all the users found
	 */
	List<?> getAlertInitiators ();

	/**
	 * Retrieves the user data of the given userId
	 * @param userId long with the id of the user to retrieve
	 * @return <code>UserData</code> with the found user, otherwise null
	 */
	UserData getUser (final long userId);

	/**
	 * Retrieves a list of users matching the given filter
	 */
	List<UserDisplayTO> findUsersByFirstAndLastName (final String firstName, final String lastName);

	/**
	 * Retrieves the list of all registered users
	 * @return all users
	 */
	List<UserData> getAllUsers ();

	/**
	 * Toggle the flag for warned users being close to the quota limit
	 * @param userId to set the flag
	 * @param warned flag
	 */
	void toggleWarnedUser (final long userId, final boolean warned);

}
