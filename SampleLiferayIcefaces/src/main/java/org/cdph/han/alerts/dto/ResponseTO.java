package org.cdph.han.alerts.dto;

import java.io.Serializable;

/**
 * Data transfer object for the summary of a response in the mir3 notification system
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ResponseTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -2606415202227254103L;
	/** Index of the response */
	private int index;
	/** Text of the response */
	private String response;
	/** Total recipients providing the response */
	private int total;
	/** Mir3 response Id */
	private int mir3Id;
	/** The call bridge if any */
	private String callbridge;

	/**
	 * Default constructor
	 */
	public ResponseTO () {}

	/**
	 * @param index of the response
	 * @param response Text in the response
	 * @param total number of recipients providing the answer
	 */
	public ResponseTO (final int index, final String response, final int total) {
		this.index = index;
		this.response = response;
		this.total = total;
	}

	/**
	 * @return the index
	 */
	public int getIndex () {
		return index;
	}

	/**
	 * @param index the index to set
	 */
	public void setIndex (int index) {
		this.index = index;
	}

	/**
	 * @return the response
	 */
	public String getResponse () {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse (String response) {
		this.response = response;
	}

	/**
	 * @return the total
	 */
	public int getTotal () {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal (int total) {
		this.total = total;
	}

	/**
	 * @return the mir3Id
	 */
	public int getMir3Id () {
		return mir3Id;
	}

	/**
	 * @param mir3Id the mir3Id to set
	 */
	public void setMir3Id (int mir3Id) {
		this.mir3Id = mir3Id;
	}

	/**
	 * @return the callbridge
	 */
	public String getCallbridge () {
		return callbridge;
	}

	/**
	 * @param callbridge the callbridge to set
	 */
	public void setCallbridge (String callbridge) {
		this.callbridge = callbridge;
	}

}
