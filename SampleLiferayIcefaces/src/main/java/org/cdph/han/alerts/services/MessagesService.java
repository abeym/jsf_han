package org.cdph.han.alerts.services;

import javax.faces.application.FacesMessage;

/**
 * Interface defining the methods required for retrieving the messages in the request locale
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface MessagesService {

	/**
	 * Returns the required message with the provided key, in case of errors or the message is not found,
	 * the defaultMessage is returned
	 * @param messageKey String with the message key for searching
	 * @param defaultMessage String with the default message
	 * @return found message or default message
	 */
	FacesMessage getMessage (final String messageKey, final String defaultMessage, final FacesMessage.Severity severity);

	/**
	 * Returns the message associated to the given resource key
	 * @param messageKey String with the resource key
	 * @return message
	 */
	String getMessage (final String messageKey);

}
