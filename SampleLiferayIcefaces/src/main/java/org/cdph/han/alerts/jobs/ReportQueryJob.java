package org.cdph.han.alerts.jobs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.ReportSummaryTO;
import org.cdph.han.alerts.services.ReportService;
import org.cdph.han.util.asynch.dto.AsynchResponseTO;
import org.cdph.han.util.asynch.jms.ResponseMessageCreator;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.TriggerKey;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * This Job class executes the query of the notification report at mir3
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Component ("reportQueryJob")
public class ReportQueryJob extends QuartzJobBean {
	/** Class log */
	private static final Log log = LogFactory.getLog (ReportQueryJob.class);

	/* (non-Javadoc)
	 * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
	 */
	@Override
	protected void executeInternal (final JobExecutionContext jobContext)
			throws JobExecutionException {
		log.info ("Updating alert report.");
		final long alertId = jobContext.getJobDetail ().getJobDataMap ().getLong ("alertId");
		final int notificationReportId = jobContext.getJobDetail ().getJobDataMap ().getInt ("notificationReportId");
		try {
			final ApplicationContext appCtx =
				(ApplicationContext) jobContext.getScheduler ().getContext ().get ("applicationContext");
			final ReportService reportService = (ReportService) appCtx.getBean ("reportService");
			final boolean isFinished = reportService.updateReport (alertId, notificationReportId);
			final ReportSummaryTO report = reportService.findReport (alertId, false, true);
			if (isFinished) {
				jobContext.getScheduler ().unscheduleJob (TriggerKey.triggerKey( "QueryReport-" + alertId + ":" + notificationReportId,
						"ALERT_PUBLICATION_GROUP"));
			}
			final JmsTemplate jmsTemplate = (JmsTemplate) appCtx.getBean ("asynchResTemplate");
			final AsynchResponseTO response = new AsynchResponseTO ("TRACK-" + alertId);
			response.getResult ().put ("REPORT_SUMMARY", report);
			final ResponseMessageCreator creator = new ResponseMessageCreator (response, null);
			jmsTemplate.send (creator);
		} catch (Exception ex) {
			log.error ("Error updating notification report for alertId: " + alertId
					+ " notificationReportId: " + notificationReportId, ex);
		}
	}

}
