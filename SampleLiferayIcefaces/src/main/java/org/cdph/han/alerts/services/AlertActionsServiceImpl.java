package org.cdph.han.alerts.services;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.AlertRequiringActionTO;
import org.cdph.han.alerts.dto.AlertSearchTO;
import org.cdph.han.persistence.Alert;
import org.cdph.han.persistence.AlertReport;
import org.cdph.han.persistence.UserData;
import org.cdph.han.util.services.HanPropertiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mir3.ws.AuthorizationType;
import com.mir3.ws.ChooseNotificationResponseOptionRequestType;
import com.mir3.ws.ChosenResponseType;
import com.mir3.ws.ErrorType;
import com.mir3.ws.GetNotificationReportRecipientsType;
import com.mir3.ws.Mir3;
import com.mir3.ws.Mir3Service;
import com.mir3.ws.ReportQueryType;
import com.mir3.ws.ReportRecipientsQueryType;
import com.mir3.ws.ReportRecipientsResponseType;
import com.mir3.ws.RespondType;
import com.mir3.ws.ResponseType;

/**
 * Service to process alert actions
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Repository
@Service ("alertActionsService")
public class AlertActionsServiceImpl implements AlertActionsService {
	/** Class log */
	private static final Log log = LogFactory.getLog (AlertActionsServiceImpl.class);
	/** MIR3 API Version key */
	private static final String API_VERSION_KEY = "MIR3_API_VERSION";
	/** MIR3 administrator name key */
	private static final String MIR3_ADMIN_USER_KEY = "MIR3_ADMIN_USER";
	/** MIR3 administrator password key */
	private static final String MIR3_ADMIN_PASS_KEY = "MIR3_ADMIN_PASS";
	/** MIR3 device address to use in responses provided within the portal */
	private static final String MIR3_DEVICE_ADDRESS_KEY = "MIR3_DEVICE_ADDRESS";
	/** MIR3 device description to use in responses provided within the portal */
	private static final String MIR3_DEVICE_DESC_KEY = "MIR3_DEVICE_DESC";
	/** Mir3 webservice */
	private Mir3 service;
	/** Persistence context */
	private EntityManager entityManager;
	/** Service used to retrieve user data */
	//abey...
	//@Autowired
	private UserDataService userDataService;
	/** Service to retrieve properties from the database */
	//abey...
	//@Autowired
	private HanPropertiesService hanPropertiesService;

	/**
	 * Constructor that initializes the mir3 service for calls
	 */
	public AlertActionsServiceImpl () {
		Mir3Service port = new Mir3Service ();
		service = port.getMir3 ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertActionsService#alertsPossiblyRequiringAction()
	 */
	@Override
	//abey...
	//@Transactional (readOnly = true)
	public List<AlertSearchTO> alertsPossiblyRequiringAction () {
		final List<AlertSearchTO> foundAlerts = new ArrayList<AlertSearchTO> ();
		final UserData currentUser = userDataService.getCurrentUserData ();
		final Query q = entityManager.createNamedQuery ("Alert.FindByReqAction");
		q.setParameter ("userId", currentUser.getUserId ());
		final List<?> alerts = q.getResultList ();
		Alert alert;
		AlertRequiringActionTO alertTO;
		for (Object o : alerts) {
			alert = (Alert) o;
			alertTO = new AlertRequiringActionTO ();
			alertTO.setAlertId (alert.getId ());
			alertTO.setLevelOfAlert (alert.getPriority ().substring (1));
			alertTO.setPublicationDate (alert.getDatePublished ());
			alertTO.setTitle (alert.getName ());
			alertTO.setTopic (alert.getTopic ().getTitle ());
			foundAlerts.add (alertTO);
		}
		return foundAlerts;
	}

	/**
	 * Retrieves all the alerts that require action
	 */
	//abey...
		//@Transactional (readOnly = true)
	public List<AlertSearchTO> alertsRequiringAction (final long userId) {
		final List<AlertSearchTO> foundAlerts = new ArrayList<AlertSearchTO> ();
		final Query q = entityManager.createNamedQuery ("Alert.FindByReqAction");
		q.setParameter ("userId", userId);
		final List<?> alerts = q.getResultList ();
		final UserData currentUser = userDataService.getUser (userId);
		Alert alert;
		AlertRequiringActionTO alertTO;
		Integer[] reportIds;
		for (Object o : alerts) {
			alert = (Alert) o;
			reportIds = requiresAction (alert, currentUser);
			if (reportIds != null) {
				alertTO = new AlertRequiringActionTO ();
				alertTO.setAlertId (alert.getId ());
				alertTO.setLevelOfAlert (alert.getPriority ().substring (1));
				alertTO.setPublicationDate (alert.getDatePublished ());
				alertTO.setTitle (alert.getName ());
				alertTO.setTopic (alert.getTopic ().getTitle ());
				alertTO.setNotificationReportId (reportIds[0]);
				alertTO.setRecipientReportId (reportIds[1]);
				foundAlerts.add (alertTO);
			}
		}
		return foundAlerts;
	}

	/**
	 * Check the given alert for further user actions
	 * @param alert Alert to check for further action
	 * @param currentUser UserData of the current user
	 * @return if the alert requires action returns the notification report id and the recipient report id
	 */
	private Integer[] requiresAction (final Alert alert, final UserData currentUser) {
		Integer[] reportIds = null;
		if (alert.getAlertReports () != null && !alert.getAlertReports ().isEmpty ()) {
			/*final List<Integer> reportIdsReqAct = new ArrayList<Integer> ();
			final GetNotificationReportsType reportsRequest = new GetNotificationReportsType ();*/
			// Get authorization using the alert initiator
			final AuthorizationType authorization = new AuthorizationType ();
			authorization.setPassword (hanPropertiesService.getValue (MIR3_ADMIN_PASS_KEY));
			authorization.setUsername (hanPropertiesService.getValue (MIR3_ADMIN_USER_KEY));
			/*// Set the API version and Authorization
			reportsRequest.setApiVersion (hanPropertiesService.getValue (API_VERSION_KEY));
			reportsRequest.setAuthorization (authorization);
			// Retrieve all the notification report ids (just one in must cases) and set them in the query object
			for (AlertReport report : alert.getAlertReports ()) {
				reportsRequest.getNotificationReportId ().add (report.getNotificationReportId ());
			}
			// Query the mir3 service
			ResponseType responseType = service.getNotificationReportsOp (reportsRequest);
			// Check for errors
			if (responseType.getError () == null || responseType.getError ().isEmpty ()) {
				GetNotificationReportsResponseType response = responseType.getGetNotificationReportsResponse ();
				// Check for active notifications (requiring action)
				for (NotificationReportType report : response.getNotificationReport ()) {
					if (report.getTimeClosed () == null) {
						reportIdsReqAct.add (report.getNotificationReportId ());
					}
				}
			} else {
				for (ErrorType error : responseType.getError ()) {
					log.error ("Error querying mir3 services: " + error.getErrorMessage ()
							+ " Error Code: " + error.getErrorCode ());
				}
			}*/
			// Check if the user has not responded to the notification
			//if (!reportIdsReqAct.isEmpty ()) {
				GetNotificationReportRecipientsType recipientsReportRequest =
					new GetNotificationReportRecipientsType ();
				// Set API version and same authorization object
				recipientsReportRequest.setApiVersion (hanPropertiesService.getValue (API_VERSION_KEY));
				recipientsReportRequest.setAuthorization (authorization);
				// Don't include devices to shorten the message
				recipientsReportRequest.setIncludeDeviceContacts (false);
				// Set the max result to 10 should only be 1
				recipientsReportRequest.setMaxResults (10);
				// Query value to execute in the search
				final ReportRecipientsQueryType query = new ReportRecipientsQueryType ();
				query.setQueryType (ReportQueryType.NO_RESPONSE);
				query.getUsername ().add (currentUser.getEmail ());
				recipientsReportRequest.setQuery (query);
				ReportRecipientsResponseType response;
				//for (Integer notificationReportId : reportIdsReqAct) {
				for (AlertReport report : alert.getAlertReports ()) {
					// Change only the notification report id, all other parameters remain the same
					recipientsReportRequest.setNotificationReportId (report.getNotificationReportId ());
					ResponseType responseType = service.getNotificationReportRecipientsOp (recipientsReportRequest);
					if (responseType.getError () == null || responseType.getError ().isEmpty ()) {
						response = responseType.getNotificationReportRecipientsResponse ();
						if (response.getRecipients ().getRecipient () != null
								&& !response.getRecipients ().getRecipient ().isEmpty ()) {
							reportIds = new Integer[2];
							reportIds[0] = report.getNotificationReportId ();
							reportIds[1] = response.getRecipients ().getRecipient ().get (0).getRecipientReportId ();
							break;
						}
					} else {
						for (ErrorType error : responseType.getError ()) {
							log.error ("Error invoking mir3 service: " + error.getErrorMessage ()
									+ " Error code: " + error.getErrorCode ());
						}
					}
				}
			//}
		}
		return reportIds;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertActionsService#checkForRequiredAction(long)
	 */
	@Override
	public boolean checkForRequiredAction (long alertId) {
		final Query q = entityManager.createNamedQuery ("Alert.FindByIDReqAction");
		q.setParameter ("userId", userDataService.getCurrentUserData ().getUserId ());
		q.setParameter ("alertId", alertId);
		return !q.getResultList ().isEmpty ();
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertActionsService#checkForRequiredAction(long)
	 */
	//abey...
		//@Transactional (readOnly = true)
	public Integer[] checkForRequiredAction (final long alertId, final long userId) {
		return requiresAction (entityManager.find (Alert.class, alertId),
				userDataService.getUser (userId));
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.AlertActionsService#respondAction(int, int, int)
	 */
	public void respondAction (final int notificationReportId, final int recipientReportId,
			final int option) {
		// Create the request object
		final ChooseNotificationResponseOptionRequestType request = new ChooseNotificationResponseOptionRequestType ();
		// Set API version
		request.setApiVersion (hanPropertiesService.getValue (API_VERSION_KEY));
		// Create authorization object using admin credentials
		final AuthorizationType authorization = new AuthorizationType ();
		authorization.setPassword (hanPropertiesService.getValue (MIR3_ADMIN_PASS_KEY));
		authorization.setUsername (hanPropertiesService.getValue (MIR3_ADMIN_USER_KEY));
		request.setAuthorization (authorization);
		// Set the response
		final RespondType response = new RespondType ();
		response.setReportId (notificationReportId);
		response.setRecipientReportId (recipientReportId);
		response.setDeviceAddress (hanPropertiesService.getValue (MIR3_DEVICE_ADDRESS_KEY));
		response.setDeviceDescription (hanPropertiesService.getValue (MIR3_DEVICE_DESC_KEY));
		// Set the response
		final ChosenResponseType chosenResponse = new ChosenResponseType ();
		chosenResponse.setOption (option);
		response.getChosenResponse ().add (chosenResponse);
		// Add to request
		request.getRespond ().add (response);
		final ResponseType serviceResponse = service.chooseNotificationResponseOptionOp (request);
		if (serviceResponse.getError () != null && !serviceResponse.getError ().isEmpty ()) {
			for (ErrorType error : serviceResponse.getError ()) {
				log.error ("Error found: " + error.getErrorMessage () + " Error code: " + error.getErrorCode ());
			}
		}
	}

	/**
	 * @param entityManager the entityManager to set
	 */
	//abey...
	//@PersistenceContext
	public void setEntityManager (EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
