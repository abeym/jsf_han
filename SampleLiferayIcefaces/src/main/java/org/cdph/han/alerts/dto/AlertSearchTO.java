package org.cdph.han.alerts.dto;

import java.io.Serializable;

/**
 * Base class for all the variants in the alert search functionality
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public abstract class AlertSearchTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -4476427890754896821L;
	/** The alert id */
	private long alertId;
	/** The alert title */
	private String title;
	/** The alert topic */
	private String topic;
	/** Ident style, used for grouping */
	private String identStyle;

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals (final Object obj) {
		boolean equal = false;
		if (obj != null && obj instanceof AlertSearchTO) {
			final AlertSearchTO other = (AlertSearchTO) obj;
			equal = other.getAlertId () == alertId;
		}
		return equal;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode () {
		return Long.valueOf (alertId).hashCode ();
	}

	/**
	 * @return the alertId
	 */
	public long getAlertId () {
		return alertId;
	}

	/**
	 * @param alertId the alertId to set
	 */
	public void setAlertId (long alertId) {
		this.alertId = alertId;
	}

	/**
	 * @return the title
	 */
	public String getTitle () {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle (String title) {
		this.title = title;
	}

	/**
	 * @return the topic
	 */
	public String getTopic () {
		return topic;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic (String topic) {
		this.topic = topic;
	}

	/**
	 * @return the identStyle
	 */
	public String getIdentStyle () {
		return identStyle;
	}

	/**
	 * @param identStyle the identStyle to set
	 */
	public void setIdentStyle (String identStyle) {
		this.identStyle = identStyle;
	}

	/**
	 * @return the specialized
	 */
	public boolean isSpecialized () {
		return false;
	}

}
