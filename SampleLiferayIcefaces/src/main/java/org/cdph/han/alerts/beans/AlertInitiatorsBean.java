package org.cdph.han.alerts.beans;

import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.MessagesService;
import org.cdph.han.alerts.services.UserDataService;
import org.cdph.han.persistence.UserData;

/**
 * This baked bean provides access to the data of the users with the ability to initiate alerts
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertInitiatorsBean {
	/** Class logger */
	private static final Log log = LogFactory.getLog (AlertInitiatorsBean.class);
	/** Service to get access to the user data */
	private UserDataService userDataService;
	/** Service to get access to the localized messages */
	private MessagesService messagesService;
	/** Initiators */
	private SelectItem[] initiators;

	/**
	 * @param userDataService the userDataService to set
	 */
	public void setUserDataService (UserDataService userDataService) {
		this.userDataService = userDataService;
	}

	/**
	 * Returns a list with the users that can start an alert
	 * @return list of users
	 */
	public SelectItem[] getInitiators () {
		if (initiators == null) {
			List<SelectItem> initiators = new ArrayList<SelectItem> ();
			try {
				List<?> users = userDataService.getAlertInitiators ();
				for (Object o : users) {
					UserData userData = (UserData) o;
					if (userData.isActive ()) {
						initiators.add (new SelectItem (userData.getUserId (), userData.getContact ().getFirstName ()
								+ " " + userData.getContact ().getLastName ()));
					}
				}
			} catch (Exception ex) {
				log.error ("Error retrieving users.", ex);
				FacesContext.getCurrentInstance ().addMessage ("initiators",
						messagesService.getMessage ("alerts.compose.notification.initiators.error",
								"Couldn't retrieve initiators, try again later.", FacesMessage.SEVERITY_ERROR));
			}
			this.initiators = initiators.toArray (new SelectItem[initiators.size ()]);
		}
		return initiators;
	}

	/**
	 * @param messagesService the messagesService to set
	 */
	public void setMessagesService (MessagesService messagesService) {
		this.messagesService = messagesService;
	}

}
