package org.cdph.han.alerts.jobs;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.services.AlertArchiveService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

/**
 * Quartz Job used to archive an alert when the expiration date is reached
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Component ("archiveAlertJob")
public class ArchiveAlertJob extends QuartzJobBean {
	/** Class log */
	private static final Log log = LogFactory.getLog (ArchiveAlertJob.class);

	/* (non-Javadoc)
	 * @see org.springframework.scheduling.quartz.QuartzJobBean#executeInternal(org.quartz.JobExecutionContext)
	 */
	@Override
	protected void executeInternal (final JobExecutionContext jobContext) throws JobExecutionException {
		try {
			final ApplicationContext appCtx =
				(ApplicationContext) jobContext.getScheduler ().getContext ().get ("applicationContext");
			final AlertArchiveService alertArchiveService =
				(AlertArchiveService) appCtx.getBean ("alertArchiveService");
			alertArchiveService.archiveAlertFromJob (
					jobContext.getJobDetail ().getJobDataMap ().getLong ("alertId"));
		} catch (Exception ex) {
			log.error ("Error archiving alert.", ex);
			throw new JobExecutionException ("Error archiving alert.", ex, false);
		}
	}

}
