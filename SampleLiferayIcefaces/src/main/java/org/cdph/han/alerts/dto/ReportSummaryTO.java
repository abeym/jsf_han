package org.cdph.han.alerts.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Data transfer object encapsulating the summary information of a report from mir3
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ReportSummaryTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -5821275582854770323L;
	/** Name of the user that started the alert */
	private String initiatedBy;
	/** Date and time when the alert was started */
	private Date issued;
	/** Date and time when the alert was completed */
	private Date completed;
	/** Name of the alert */
	private String name;
	/** Notification method */
	private String type;
	/** ID of the report in mir3 */
	private long reportId;
	/** Message key for the expedited type */
	private String expeditedDelivery;
	/** Time used in phone calls */
	private int phoneTime;
	/** Message included in the notification */
	private String message;
	/** List of available responses */
	private List<ResponseTO> responseOptions;
	/** Total recipients included in the alert */
	private int totalRecipients;
	/** Total recipients contacted */
	private int totalContacted;
	/** Total recipients responded */
	private int totalResponded;
	/** Total calls made */
	private int totalCalls;
	/** Total emails sent */
	private int totalEmails;
	/** Total pages */
	private int totalPages;
	/** Total faxes */
	private int totalFaxes;
	/** Total sms */
	private int sms;
	/** Total hang ups */
	private int hangUp;
	/** Total no answer times */
	private int noAnswer;
	/** Total busy line times */
	private int busy;
	/** Total not at this location times */
	private int notAtThisLocation;
	/** Total invalid responses received */
	private int invalidResponse;
	/** Total times reached answering machine */
	private int answeringMachine;
	/** Total messages left */
	private int leftMessages;
	/** Total wrong address or tel # */
	private int wrongAddressTel;
	/** Other issues */
	private int other;
	/** List of recipients */
	private List<ReportRecipientTO> recipients;
	/** Date of the last report fetch */
	private Date lastFetch;
	/** Number of responses received via blackberry */
	private int blackBerry;

	/**
	 * Default constructor
	 */
	public ReportSummaryTO () {}

	/**
	 * Constructor using fields
	 * @param initiatedBy Name of the user starting the alert
	 * @param issued Date and time of the publish date
	 * @param completed Date and time the notification was closed
	 * @param name Name of the alert
	 * @param type Type of the notification method
	 * @param reportId ID of the report at mir3
	 * @param expeditedDelivery Expedited delivery method message key
	 * @param phoneTime Time used in phone calls
	 * @param message Message in the notification
	 * @param responseOptions Summary of the response options
	 * @param totalRecipients Total of recipients in the notification
	 * @param totalContacted Total recipients contacted
	 * @param totalResponded Total recipients that responded
	 */
	public ReportSummaryTO (final String initiatedBy, final Date issued,
			final Date completed, final String name, final String type, final int reportId,
			final String expeditedDelivery, final int phoneTime, final String message,
			final List<ResponseTO> responseOptions, final int totalRecipients,
			final int totalContacted, final int totalResponded) {
		this.initiatedBy = initiatedBy;
		this.issued = issued;
		this.completed = completed;
		this.name = name;
		this.type = type;
		this.reportId = reportId;
		this.expeditedDelivery = expeditedDelivery;
		this.phoneTime = phoneTime;
		this.message = message;
		this.responseOptions = responseOptions;
		this.totalRecipients = totalRecipients;
		this.totalContacted = totalContacted;
		this.totalResponded = totalResponded;
	}

	/**
	 * @return the initiatedBy
	 */
	public String getInitiatedBy () {
		return initiatedBy;
	}

	/**
	 * @param initiatedBy the initiatedBy to set
	 */
	public void setInitiatedBy (String initiatedBy) {
		this.initiatedBy = initiatedBy;
	}

	/**
	 * @return the issued
	 */
	public Date getIssued () {
		return issued;
	}

	/**
	 * @param issued the issued to set
	 */
	public void setIssued (Date issued) {
		this.issued = issued;
	}

	/**
	 * @return the completed
	 */
	public Date getCompleted () {
		return completed;
	}

	/**
	 * @param completed the completed to set
	 */
	public void setCompleted (Date completed) {
		this.completed = completed;
	}

	/**
	 * @return the name
	 */
	public String getName () {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName (String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType () {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType (String type) {
		this.type = type;
	}

	/**
	 * @return the reportId
	 */
	public long getReportId () {
		return reportId;
	}

	/**
	 * @param reportId the reportId to set
	 */
	public void setReportId (long reportId) {
		this.reportId = reportId;
	}

	/**
	 * @return the expeditedDelivery
	 */
	public String getExpeditedDelivery () {
		return expeditedDelivery;
	}

	/**
	 * @param expeditedDelivery the expeditedDelivery to set
	 */
	public void setExpeditedDelivery (String expeditedDelivery) {
		this.expeditedDelivery = expeditedDelivery;
	}

	/**
	 * @return the phoneTime
	 */
	public int getPhoneTime () {
		return phoneTime;
	}

	/**
	 * @param phoneTime the phoneTime to set
	 */
	public void setPhoneTime (int phoneTime) {
		this.phoneTime = phoneTime;
	}

	/**
	 * @return the message
	 */
	public String getMessage () {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage (String message) {
		this.message = message;
	}

	/**
	 * @return the responseOptions
	 */
	public List<ResponseTO> getResponseOptions () {
		return responseOptions;
	}

	/**
	 * @param responseOptions the responseOptions to set
	 */
	public void setResponseOptions (List<ResponseTO> responseOptions) {
		this.responseOptions = responseOptions;
	}

	/**
	 * @return the totalRecipients
	 */
	public int getTotalRecipients () {
		return totalRecipients;
	}

	/**
	 * @param totalRecipients the totalRecipients to set
	 */
	public void setTotalRecipients (int totalRecipients) {
		this.totalRecipients = totalRecipients;
	}

	/**
	 * @return the totalContacted
	 */
	public int getTotalContacted () {
		return totalContacted;
	}

	/**
	 * @param totalContacted the totalContacted to set
	 */
	public void setTotalContacted (int totalContacted) {
		this.totalContacted = totalContacted;
	}

	/**
	 * @return the totalResponded
	 */
	public int getTotalResponded () {
		return totalResponded;
	}

	/**
	 * @param totalResponded the totalResponded to set
	 */
	public void setTotalResponded (int totalResponded) {
		this.totalResponded = totalResponded;
	}

	/**
	 * @return the formatted phone time
	 */
	public String getFormattedPhoneTime () {
		final int minutes = phoneTime % 60;
		final int hours = phoneTime / 60;
		final StringBuilder builder = new StringBuilder ();
		if (hours < 10) {
			builder.append (0).append (hours);
		} else {
			builder.append (hours);
		}
		builder.append (':');
		if (minutes < 10) {
			builder.append (0).append (minutes);
		} else {
			builder.append (minutes);
		}
		return builder.toString ();
	}

	/**
	 * @return the totalCalls
	 */
	public int getTotalCalls () {
		return totalCalls;
	}

	/**
	 * @param totalCalls the totalCalls to set
	 */
	public void setTotalCalls (int totalCalls) {
		this.totalCalls = totalCalls;
	}

	/**
	 * @return the totalEmails
	 */
	public int getTotalEmails () {
		return totalEmails;
	}

	/**
	 * @param totalEmails the totalEmails to set
	 */
	public void setTotalEmails (int totalEmails) {
		this.totalEmails = totalEmails;
	}

	/**
	 * @return the totalPages
	 */
	public int getTotalPages () {
		return totalPages;
	}

	/**
	 * @param totalPages the totalPages to set
	 */
	public void setTotalPages (int totalPages) {
		this.totalPages = totalPages;
	}

	/**
	 * @return the totalFaxes
	 */
	public int getTotalFaxes () {
		return totalFaxes;
	}

	/**
	 * @param totalFaxes the totalFaxes to set
	 */
	public void setTotalFaxes (int totalFaxes) {
		this.totalFaxes = totalFaxes;
	}

	/**
	 * @return the sms
	 */
	public int getSms () {
		return sms;
	}

	/**
	 * @param sms the sms to set
	 */
	public void setSms (int sms) {
		this.sms = sms;
	}

	/**
	 * @return the hangUp
	 */
	public int getHangUp () {
		return hangUp;
	}

	/**
	 * @param hangUp the hangUp to set
	 */
	public void setHangUp (int hangUp) {
		this.hangUp = hangUp;
	}

	/**
	 * @return the noAnswer
	 */
	public int getNoAnswer () {
		return noAnswer;
	}

	/**
	 * @param noAnswer the noAnswer to set
	 */
	public void setNoAnswer (int noAnswer) {
		this.noAnswer = noAnswer;
	}

	/**
	 * @return the busy
	 */
	public int getBusy () {
		return busy;
	}

	/**
	 * @param busy the busy to set
	 */
	public void setBusy (int busy) {
		this.busy = busy;
	}

	/**
	 * @return the notAtThisLocation
	 */
	public int getNotAtThisLocation () {
		return notAtThisLocation;
	}

	/**
	 * @param notAtThisLocation the notAtThisLocation to set
	 */
	public void setNotAtThisLocation (int notAtThisLocation) {
		this.notAtThisLocation = notAtThisLocation;
	}

	/**
	 * @return the invalidResponse
	 */
	public int getInvalidResponse () {
		return invalidResponse;
	}

	/**
	 * @param invalidResponse the invalidResponse to set
	 */
	public void setInvalidResponse (int invalidResponse) {
		this.invalidResponse = invalidResponse;
	}

	/**
	 * @return the answeringMachine
	 */
	public int getAnsweringMachine () {
		return answeringMachine;
	}

	/**
	 * @param answeringMachine the answeringMachine to set
	 */
	public void setAnsweringMachine (int answeringMachine) {
		this.answeringMachine = answeringMachine;
	}

	/**
	 * @return the leftMessages
	 */
	public int getLeftMessages () {
		return leftMessages;
	}

	/**
	 * @param leftMessages the leftMessages to set
	 */
	public void setLeftMessages (int leftMessages) {
		this.leftMessages = leftMessages;
	}

	/**
	 * @return the wrongAddressTel
	 */
	public int getWrongAddressTel () {
		return wrongAddressTel;
	}

	/**
	 * @param wrongAddressTel the wrongAddressTel to set
	 */
	public void setWrongAddressTel (int wrongAddressTel) {
		this.wrongAddressTel = wrongAddressTel;
	}

	/**
	 * @return the other
	 */
	public int getOther () {
		return other;
	}

	/**
	 * @param other the other to set
	 */
	public void setOther (int other) {
		this.other = other;
	}

	/**
	 * @return the recipients
	 */
	public List<ReportRecipientTO> getRecipients () {
		return recipients;
	}

	/**
	 * @param recipients the recipients to set
	 */
	public void setRecipients (List<ReportRecipientTO> recipients) {
		this.recipients = recipients;
	}

	/**
	 * @return the status
	 */
	public String getStatus () {
		String status;
		if (completed != null) {
			status = "Completed";
		} else {
			status = "In Progress";
		}
		return status;
	}

	/**
	 * @return the lastFetch
	 */
	public Date getLastFetch () {
		return lastFetch;
	}

	/**
	 * @param lastFetch the lastFetch to set
	 */
	public void setLastFetch (Date lastFetch) {
		this.lastFetch = lastFetch;
	}

	/**
	 * @return the blackBerry
	 */
	public int getBlackBerry () {
		return blackBerry;
	}

	/**
	 * @param blackBerry the blackBerry to set
	 */
	public void setBlackBerry (int blackBerry) {
		this.blackBerry = blackBerry;
	}

}
