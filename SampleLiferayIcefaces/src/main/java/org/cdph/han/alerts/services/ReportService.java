package org.cdph.han.alerts.services;

import org.cdph.han.alerts.dto.ReportSummaryTO;

public interface ReportService {

	/**
	 * Retrieves the report of the given alert
	 * @param alertId Id of the alert to retrieve the report
	 * @param historic if the report is historic or is recent
	 * @return ReportSummaryTO object with the alert report, null in case no report is found
	 */
	ReportSummaryTO findReport (final long alertId, final boolean archived);

	/**
	 * Retrieves the report of the given alert
	 * @param alertId Id of the alert to retrieve the report
	 * @param historic if the report is historic or is recent
	 * @param fromWeb if the request is done from the web
	 * @return ReportSummaryTO object with the alert report, null in case no report is found
	 */
	ReportSummaryTO findReport (final long alertId, final boolean archived, final boolean fromWeb);

	/**
	 * Updates the given report using mir3 API if the report is not marked as finished in the DB
	 * @param alertId the id of the alert
	 * @param notificationReportId the id of the notification report
	 * @return if the notification is finished
	 */
	boolean updateReport (final long alertId, final int notificationReportId);

}
