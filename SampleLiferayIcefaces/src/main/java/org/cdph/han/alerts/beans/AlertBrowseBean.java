package org.cdph.han.alerts.beans;

import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.components.MenuUserObject.SearchType;
import org.cdph.han.alerts.dto.AlertSearchTO;
import org.cdph.han.alerts.exceptions.AlertTerminatedException;
import org.cdph.han.alerts.services.AbortAlertService;
import org.cdph.han.alerts.services.AlertArchiveService;
import org.cdph.han.alerts.services.AlertJPAService;

/**
 * This class controls the functionality to allow the user search and browse alerts
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertBrowseBean extends AbstractAlertSearchBean {
	/** Class serial version UID */
	private static final long serialVersionUID = -3087698499504904081L;
	/** Class log */
	private static Log log = LogFactory.getLog (AlertBrowseBean.class);
	/** Flag for showing the archive confirmation dialog */
	private boolean archiveVisible;
	/** Flag for showing the delete confirmation dialog */
	private boolean deleteVisible;
	/** Flag for showing the abort confirmation dialog */
	private boolean abortVisible;
	/** Alert ID to abort */
	private long alertId;
	/** Flag if the current selected alert is archived */
	private boolean archived;
	/** Service to archive alerts */
	private AlertArchiveService alertArchiveService;
	/** Previous executed search */
	private SearchType lastExecuted = SearchType.CURRENT_BY_PUBLICATION_DATE;
	/** Keep track of the executed searches for alert actions */
	private SearchType previouslyExecuted = SearchType.CURRENT_BY_PUBLICATION_DATE;
	/** Service for aborting alerts */
	private AbortAlertService abortAlertService;
	/** Search to execute if the found alerts are empty */
	private SearchType toExecute = SearchType.CURRENT_BY_PUBLICATION_DATE;

	/**
	 * Action listener that refreshes the last executed search
	 * @param event ActionEvent triggered by the user
	 */
	public void refreshSearch (final ActionEvent event) {
		performSearch (lastExecuted);
	}

	/**
	 * Action listener to execute the requested search
	 * @param event ActionEvent triggered by the user
	 */
	public void executeSearch (final ActionEvent event) {
		final String typeString = (String) FacesContext.getCurrentInstance ().getExternalContext (
				).getRequestParameterMap ().get ("selectedSearch");
		log.info ("Requested search: " + typeString);
		try {
			final SearchType type = typeString != null && !typeString.equals ("") ? SearchType.valueOf (typeString)
					: SearchType.CURRENT_BY_PUBLICATION_DATE;
			previouslyExecuted = lastExecuted;
			lastExecuted = type;
			performSearch (type);
		} catch (Exception ex) {
			log.error ("Error performing search: " + typeString, ex);
			FacesContext.getCurrentInstance ().addMessage (null,
					messagesService.getMessage ("alerts.search.error",
							"Error performing search, try again later.",
							FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * Executes the search requested in the SearchType parameter
	 * @param type SearchType representing the search to perform
	 */
	private void performSearch (final SearchType type) {
		if (SearchType.CURRENT_BY_PUBLICATION_DATE.equals (type)) {
			searchTitle = "alerts.search.current.date.title";
			foundAlerts = alertJPAService.searchCurrentByDate ();
		} else if (SearchType.CURRENT_BY_TOPIC.equals (type)) {
			searchTitle = "alerts.search.topic.title";
			foundAlertsGroups = alertJPAService.searchCurrentByTopic ();
			addAllGroups (foundAlertsGroups);
		} else if (SearchType.CURRENT_BY_LEVEL_OF_ALERT.equals (type)) {
			searchTitle = "alerts.search.current.level.title";
			foundAlertsGroups = alertJPAService.searchCurrentByAlertLevel ();
			addAllGroups (foundAlertsGroups);
		} else if (SearchType.DRAFT_BY_LAST_MODIFIED.equals (type)) {
			foundAlerts = alertJPAService.searchDrafts ();
		} else if (SearchType.ARCHIVED_BY_LEVEL_OF_ALERT.equals (type)) {
			searchTitle = "alerts.search.archived.level.title";
			foundAlertsGroups = alertJPAService.searchArchivedByAlertLevel ();
			addAllGroups (foundAlertsGroups);
		} else if (SearchType.ARCHIVED_BY_PUBLICATION_DATE.equals (type)) {
			searchTitle = "alerts.search.archived.date.title";
			foundAlerts = alertJPAService.searchArchivedByPublishDate ();
		} else if (SearchType.ARCHIVED_BY_TOPIC.equals (type)) {
			searchTitle = "alerts.search.archived.topic.title";
			foundAlertsGroups = alertJPAService.searchArchivedByTopic ();
			addAllGroups (foundAlertsGroups);
		} else if (SearchType.SCHEDULED_BY_SCHEDULED_DATE.equals (type)) {
			searchTitle = "alerts.search.scheduled.title";
			foundAlertsGroups = alertJPAService.searchScheduled ();
			addAllGroups (foundAlertsGroups);
		}
	}

	/**
	 * Action listener for toggling the visibility of the archive confirmation dialog
	 * @param event ActionEvent
	 */
	public void toggleArchive (final ActionEvent event) {
		archiveVisible = !archiveVisible;
		if (archiveVisible) {
			final String alertIdString = (String) FacesContext.getCurrentInstance ().getExternalContext (
					).getRequestParameterMap ().get ("alertId");
			alertId = Long.parseLong (alertIdString);
		}
	}

	/**
	 * Action listener for toggling the visibility of the delete confirmation dialog
	 * @param event ActionEvent
	 */
	public void toggleDelete (final ActionEvent event) {
		deleteVisible = !deleteVisible;
		if (deleteVisible) {
			final String alertIdString = (String) FacesContext.getCurrentInstance ().getExternalContext (
					).getRequestParameterMap ().get ("alertId");
			final String archivedString = (String) FacesContext.getCurrentInstance ().getExternalContext (
					).getRequestParameterMap ().get ("archived");
			alertId = Long.parseLong (alertIdString);
			archived = Boolean.parseBoolean (archivedString);
		}
	}

	/**
	 * Displays and hides the confirmation dialog for aborting an alert
	 * @param event ActionEvent
	 */
	public void toggleAbort (final ActionEvent event) {
		if (!abortVisible) {
			final String alertIdString = (String) FacesContext.getCurrentInstance ().getExternalContext (
					).getRequestParameterMap ().get ("alertId");
			if (alertIdString != null && !alertIdString.equals ("")) {
				alertId = Long.parseLong (alertIdString);
			}
		}
		abortVisible = !abortVisible;
	}

	/**
	 * Action listener for archiving an alert
	 * @param event ActionEvent
	 */
	public void archiveAlert (final ActionEvent event) {
		// Hide confirmation window
		archiveVisible = false;
		try {
			alertArchiveService.archiveAlert (alertId);
			FacesContext.getCurrentInstance ().addMessage (null, messagesService.getMessage (
					"alerts.action.archive.success",
					"The alert has been successfully moved to the archive.",
					FacesMessage.SEVERITY_INFO));
			// Execute search previous to the Detail search
			performSearch (previouslyExecuted);
		} catch (Exception ex) {
			log.error ("Error moving alert to archives.", ex);
			FacesContext.getCurrentInstance ().addMessage (null, messagesService.getMessage (
					"alerts.action.archive.error",
					"The alert couldn't be moved to the archive.",
					FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * Action listener for soft deleting an alert
	 * @param event ActionEvent
	 */
	public void deleteAlert (final ActionEvent event) {
		// Hide confirmation window
		deleteVisible = false;
		try {
			alertArchiveService.deleteAlert (alertId, archived);
			FacesContext.getCurrentInstance ().addMessage (null, messagesService.getMessage (
					"alerts.action.delete.success",
					"The alert has been successfully moved to the archive and flagged as deleted.",
					FacesMessage.SEVERITY_INFO));
			// Execute search previous to the Detail search
			performSearch (previouslyExecuted);
		} catch (Exception ex) {
			log.error ("Error moving alert to archives and flagging as deleted.", ex);
			FacesContext.getCurrentInstance ().addMessage (null, messagesService.getMessage (
					"alerts.action.delete.error",
					"The alert couldn't be moved to the archive and flagged as deleted.",
					FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * Action listener for aborting an alert
	 * @param event ActionEvent
	 */
	public void abortAlert (final ActionEvent event) {
		// Hide confirmation
		abortVisible = false;
		try {
			abortAlertService.abortNotification (alertId);
			FacesContext.getCurrentInstance ().addMessage (null, messagesService.getMessage (
					"alerts.action.abort.success",
					"The alert has been aborted successfully.",
					FacesMessage.SEVERITY_INFO));
			// Execute search previous to the Detail search
			performSearch (lastExecuted);
		} catch (AlertTerminatedException ex) {
			log.error ("Error terminating alert, alert already terminated.", ex);
			FacesContext.getCurrentInstance ().addMessage (null, messagesService.getMessage (
					"alerts.action.abort.already.terminated",
					"The alert is already finished.",
					FacesMessage.SEVERITY_ERROR));
		} catch (Exception ex) {
			log.error ("Error terminating alert.", ex);
			FacesContext.getCurrentInstance ().addMessage (null, messagesService.getMessage (
					"alerts.action.abort.error",
					"The alert couldn't be aborted, try again later.",
					FacesMessage.SEVERITY_ERROR));
		}
	}

	/**
	 * @return the archiveVisible
	 */
	public boolean isArchiveVisible () {
		return archiveVisible;
	}

	/**
	 * @param archiveVisible the archiveVisible to set
	 */
	public void setArchiveVisible (boolean archiveVisible) {
		this.archiveVisible = archiveVisible;
	}

	/**
	 * @return the deleteVisible
	 */
	public boolean isDeleteVisible () {
		return deleteVisible;
	}

	/**
	 * @param deleteVisible the deleteVisible to set
	 */
	public void setDeleteVisible (boolean deleteVisible) {
		this.deleteVisible = deleteVisible;
	}

	/**
	 * @return the abortVisible
	 */
	public boolean isAbortVisible () {
		return abortVisible;
	}

	/**
	 * @param abortVisible the abortVisible to set
	 */
	public void setAbortVisible (boolean abortVisible) {
		this.abortVisible = abortVisible;
	}

	/**
	 * @return the alertId
	 */
	public long getAlertId () {
		return alertId;
	}

	/**
	 * @param alertId the alertId to set
	 */
	public void setAlertId (long alertId) {
		this.alertId = alertId;
	}

	/**
	 * @param alertArchiveService the alertArchiveService to set
	 */
	public void setAlertArchiveService (AlertArchiveService alertArchiveService) {
		this.alertArchiveService = alertArchiveService;
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.beans.AbstractAlertSearchBean#setAlertJPAService(org.cdph.han.alerts.services.AlertJPAService)
	 */
	@Override
	public void setAlertJPAService (AlertJPAService alertJPAService) {
		super.setAlertJPAService (alertJPAService);
		// Set the found alerts
		if (foundAlerts == null) {
			foundAlerts = alertJPAService.searchCurrentByDate ();
		}
	}

	/**
	 * @param abortAlertService the abortAlertService to set
	 */
	public void setAbortAlertService (AbortAlertService abortAlertService) {
		this.abortAlertService = abortAlertService;
	}

	/**
	 * @return the toExecute
	 */
	public String getToExecute () {
		return toExecute.toString ();
	}

	/**
	 * @param toExecute the toExecute to set
	 */
	public void setToExecute (String toExecute) {
		this.toExecute = SearchType.valueOf (toExecute);
		if (SearchType.SCHEDULED_BY_SCHEDULED_DATE.equals (this.toExecute)) {
			foundAlerts = null;
			searchTitle = "alerts.search.scheduled.title";
		}
	}

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.beans.AbstractAlertSearchBean#getFoundAlerts()
	 */
	@Override
	public List<AlertSearchTO> getFoundAlerts () {
		if (foundAlerts == null) {
			performSearch (toExecute);
		}
		return super.getFoundAlerts ();
	}

}
