package org.cdph.han.alerts.services;

import org.cdph.han.alerts.dto.AlertTO;
import org.cdph.han.alerts.exceptions.PublishAlertException;

/**
 * Definition of the minimum methods required to manipulate alerts
 * @author Horaci Oswaldo Ferro
 * @version 1.0
 */
public interface NotificationService {

	/**
	 * Send an alert using mir3 webservices
	 * @param alert AlertTO to send
	 * @return id of the alert at mir3 site
	 * @throws PublishAlertException in case of errors while sending the alert
	 */
	int sendAlert (final AlertTO alert) throws PublishAlertException;

}
