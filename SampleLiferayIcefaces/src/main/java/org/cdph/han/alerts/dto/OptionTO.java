package org.cdph.han.alerts.dto;

import java.io.Serializable;

/**
 * Transfer Object for the notification options
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class OptionTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = 4754736195242621661L;
	/** Text of the response option */
	private String optionText;
	/** Action of the response option */
	private int action;
	/** The call bridge number */
	private String callBridge;

	/**
	 * @return the optionText
	 */
	public String getOptionText () {
		return optionText;
	}

	/**
	 * @param optionText the optionText to set
	 */
	public void setOptionText (String optionText) {
		this.optionText = optionText;
	}

	/**
	 * @return the action
	 */
	public int getAction () {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction (int action) {
		this.action = action;
	}

	/**
	 * @return the callBridge
	 */
	public String getCallBridge () {
		return callBridge;
	}

	/**
	 * @param callBridge the callBridge to set
	 */
	public void setCallBridge (String callBridge) {
		this.callBridge = callBridge;
	}

}
