package org.cdph.han.alerts.beans;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.portlet.PortletSession;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.alerts.dto.AlertGroupingTO;
import org.cdph.han.alerts.dto.AlertSearchTO;
import org.cdph.han.alerts.services.AlertJPAService;
import org.cdph.han.alerts.services.MessagesService;

/**
 * Base class used for all search beans containing common methods and attributes
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public abstract class AbstractAlertSearchBean implements Serializable {
	private static final Log log = LogFactory.getLog (AbstractAlertSearchBean.class);
	/** Class serial version ID */
	private static final long serialVersionUID = -3613608627305885293L;
	/** List of results */
	protected List<AlertSearchTO> foundAlerts;
	/** Map containing all the results of a given search */
	protected Map<? extends Object, List<AlertSearchTO>> foundAlertsGroups;
	/** Service for searching alerts */
	protected AlertJPAService alertJPAService;
	/** Service for retrieving messages */
	protected MessagesService messagesService;
	/** The current grouping */
	protected AlertGroupingTO[] grouping;
	/** Title of the search page */
	protected String searchTitle = "alerts.search.current.date.title";

	/**
	 * Expands or collapses a group of search results
	 * @param event ActionEvent triggered by the user
	 */
	public void toggleGroup (final ActionEvent event) {
		final String groupTitle = (String) FacesContext.getCurrentInstance (
				).getExternalContext ().getRequestParameterMap ().get ("groupTitle");
		log.info ("Group to toggle: " + groupTitle);
		log.info ("Groups: " + grouping != null ? grouping.length : 0);
		Object key;
		for (AlertGroupingTO group : grouping) {
			if (group.getTitle ().equals (groupTitle)) {
				key = group.getKey ();
				if (group.isExpanded ()) {
					foundAlerts.removeAll (foundAlertsGroups.get (key));
					group.setExpanded (false);
				} else {
					foundAlerts.addAll (foundAlerts.indexOf (group) + 1, foundAlertsGroups.get (key));
					group.setExpanded (true);
				}
				break;
			}
		}
	}

	/**
	 * Adds all the groups of alerts to the list created
	 * @param grouping an array that will contain the grouping
	 * @param foundAlertsGroups the alerts found that are grouped
	 * @return a list with all the alerts
	 */
	protected void addAllGroups (final Map<? extends Object, List<AlertSearchTO>> foundAlertsGroups) {
		foundAlerts = new ArrayList<AlertSearchTO> ();
		grouping = new AlertGroupingTO[foundAlertsGroups.keySet ().size ()];
		int index = 0;
		for (Object key : foundAlertsGroups.keySet ()) {
			AlertGroupingTO group = new AlertGroupingTO ();
			if (key instanceof String) {
				group.setTitle ((String) key);
			} else if (key instanceof Date) {
				SimpleDateFormat sdf = new SimpleDateFormat ("MM/dd/yyyy");
				group.setTitle (sdf.format ((Date) key));
			}
			group.setExpanded (true);
			group.setKey (key);
			foundAlerts.add (group);
			grouping[index++] = group;
			foundAlerts.addAll (foundAlertsGroups.get (key));
		}
	}

	/**
	 * Retrieves the session id to use in the re render requests
	 * @return session ID
	 */
	protected String getSessionId () {
		String sessionId;
		Object o = FacesContext.getCurrentInstance ().getExternalContext ().getSession (true);
		if (o instanceof HttpSession) {
			sessionId = ((HttpSession) o).getId ();
		} else if (o instanceof PortletSession) {
			sessionId = ((PortletSession) o).getId ();
		} else {
			sessionId = null;
		}
		return sessionId;
	}

	/**
	 * @return the foundAlerts
	 */
	public List<AlertSearchTO> getFoundAlerts () {
		return foundAlerts;
	}

	/**
	 * @param foundAlerts the foundAlerts to set
	 */
	public void setFoundAlerts (List<AlertSearchTO> foundAlerts) {
		this.foundAlerts = foundAlerts;
	}

	/**
	 * @return the foundAlertsGroups
	 */
	public Map<? extends Object, List<AlertSearchTO>> getFoundAlertsGroups () {
		return foundAlertsGroups;
	}

	/**
	 * @param foundAlertsGroups the foundAlertsGroups to set
	 */
	public void setFoundAlertsGroups (Map<? extends Object, List<AlertSearchTO>> foundAlertsGroups) {
		this.foundAlertsGroups = foundAlertsGroups;
	}

	/**
	 * @param alertJPAService the alertJPAService to set
	 */
	public void setAlertJPAService (AlertJPAService alertJPAService) {
		this.alertJPAService = alertJPAService;
	}

	/**
	 * @param messagesService the messagesService to set
	 */
	public void setMessagesService (MessagesService messagesService) {
		this.messagesService = messagesService;
	}

	/**
	 * @return the searchTitle
	 */
	public String getSearchTitle () {
		return searchTitle;
	}

	/**
	 * @param searchTitle the searchTitle to set
	 */
	public void setSearchTitle (String searchTitle) {
		this.searchTitle = searchTitle;
	}

}