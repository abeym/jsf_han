package org.cdph.han.alerts.dto;

import java.io.Serializable;
import java.util.Date;

/**
 * This data transfer object class represents a device used to contact a user in the mir3 notification
 * system
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ReportDeviceTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -3794966345747809248L;
	/** Email address, fax or phone number */
	private String address;
	/** Result of contacting the device */
	private String contactResult;
	/** Time the message was sent */
	private Date timeSent;
	/** Time when the response was received */
	private Date timeResponded;
	/** Duration of the comunication */
	private int duration;
	/** Description of the device */
	private String description;
	/** Contact Id to identify each contact attempt */
	private int contactId;
	/** Selected response if any */
	private Integer response;

	/**
	 * @return the address
	 */
	public String getAddress () {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress (String address) {
		this.address = address;
	}

	/**
	 * @return the contactResult
	 */
	public String getContactResult () {
		return contactResult;
	}

	/**
	 * @param contactResult the contactResult to set
	 */
	public void setContactResult (String contactResult) {
		this.contactResult = contactResult;
	}

	/**
	 * @return the timeSent
	 */
	public Date getTimeSent () {
		return timeSent;
	}

	/**
	 * @param timeSent the timeSent to set
	 */
	public void setTimeSent (Date timeSent) {
		this.timeSent = timeSent;
	}

	/**
	 * @return the timeResponded
	 */
	public Date getTimeResponded () {
		return timeResponded;
	}

	/**
	 * @param timeResponded the timeResponded to set
	 */
	public void setTimeResponded (Date timeResponded) {
		this.timeResponded = timeResponded;
	}

	/**
	 * @return the duration
	 */
	public int getDuration () {
		return duration;
	}

	/**
	 * @param duration the duration to set
	 */
	public void setDuration (int duration) {
		this.duration = duration;
	}

	/**
	 * @return the description
	 */
	public String getDescription () {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription (String description) {
		this.description = description;
	}

	/**
	 * @return the contactId
	 */
	public int getContactId () {
		return contactId;
	}

	/**
	 * @param contactId the contactId to set
	 */
	public void setContactId (int contactId) {
		this.contactId = contactId;
	}

	/**
	 * @return the response
	 */
	public Integer getResponse () {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse (Integer response) {
		this.response = response;
	}

}
