package org.cdph.han.alerts.services;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.cdph.han.alerts.dto.AlertSearchTO;
import org.cdph.han.alerts.dto.AlertTO;
import org.cdph.han.alerts.dto.AttachmentTO;
import org.cdph.han.alerts.exceptions.SaveAlertException;
import org.cdph.han.alerts.exceptions.PublishAlertException;

/**
 * Interface to allow access to the save, update and retrieve functionality for the alerts.
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AlertJPAService {

	/**
	 * Insert or update an alert in the HAN database
	 * @param alert AlertTO containing all the HAN Alert information
	 * @return id of the alert
	 * @throws SaveAlertException in case of errors while saving the alert
	 */
	long saveAlert (final AlertTO alertTO) throws SaveAlertException;

	/**
	 * Retrieves an alert using the alert id
	 * @param alertId long with the alert id to retrieve
	 * @param archived if the alert to load is archived
	 * @return AlertTO with the found alert, if not found, null
	 */
	AlertTO retrieveAlert (final long alertId, boolean archived) throws SaveAlertException;

	/**
	 * Loads an alert using the alert id, this method does not retrieve the attachments
	 * @param alertId long with the alert id to load
	 * @param archived if the alert to load is archived
	 * @return AlertTO with the found alert, otherwise null
	 */
	AlertTO loadAlert (final long alertId, boolean archived);

	/**
	 * Retrieves all the alerts that are not published (drafts)
	 * @return List with AlertDrafTO objects for each alert draft
	 */
	List<AlertSearchTO> searchDrafts ();

	/**
	 * Retrieves all the alerts that are current sorted by publication date
	 * @return List with AlertPublishedTO objects for each found alert
	 */
	List<AlertSearchTO> searchCurrentByDate ();

	/**
	 * Retrieves all the alerts that are current grouped by topic
	 * @return Map with the topic name as key and a list of found alerts
	 */
	Map<String, List <AlertSearchTO>> searchCurrentByTopic ();

	/**
	 * Retrieves all the alerts that are current grouped by level of alert
	 * @return Map with the level of alert as key and a list of found alerts
	 */
	Map<String, List<AlertSearchTO>> searchCurrentByAlertLevel ();

	/**
	 * Retrieves all the alerts that are archived sorted by publication date
	 * @return List with AlertPublishedTO objects for each found alert
	 */
	List<AlertSearchTO> searchArchivedByPublishDate ();

	/**
	 * Retrieves all the alerts that are archived grouped by topic
	 * @return Map with the topic name as key and a list of found alerts
	 */
	Map<String, List <AlertSearchTO>> searchArchivedByTopic ();

	/**
	 * Retrieves all the alerts that are archived grouped by level of alert
	 * @return Map with the level of alert as key and a list of found alerts
	 */
	Map<String, List<AlertSearchTO>> searchArchivedByAlertLevel ();

	/**
	 * Retrieves all the alerts that are scheduled but not published
	 * @return Map with the date scheduled as key and a list of found alerts
	 */
	Map<Date, List<AlertSearchTO>> searchScheduled ();

	/**
	 * Publishes an alert and send a notification
	 * @param alert AlertTO object with the information of the alert to publish
	 * @throws PublishAlertException in case of errors while publishing the alert
	 * @return the notification report id assigned to the notification
	 */
	int publishAlert (final AlertTO alert) throws PublishAlertException;

	/**
	 * Publish scheduled alert
	 * @param alertId with the id of the alert to publish
	 * @throws PublishAlertException in case of errors while publishing the alert
	 * @return the notification report id assigned to the notification
	 */
	int publishAlert (final long alertId) throws PublishAlertException;

	/**
	 * Reads an attachment validating that the user trying to download it is in the recipient list or have the
	 * appropriate role.
	 * @param attachmentId with the id of the attachment to obtain
	 * @param userId with the id of the user requesting the attachment
	 * @param archived alert flag
	 */
	AttachmentTO readAttachment (final long attachmentId, final long userId, final boolean archived);

	/**
	 * Retrieves all the alerts for tracking (current and archived)
	 * @return Map with the level of alert as key and a list of found alerts
	 */
	Map<String, List<AlertSearchTO>> searchAlertsForTracking ();

}
