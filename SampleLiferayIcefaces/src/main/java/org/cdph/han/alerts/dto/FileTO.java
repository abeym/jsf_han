package org.cdph.han.alerts.dto;

import java.io.File;
import java.io.Serializable;

import javax.faces.context.FacesContext;

import org.cdph.han.alerts.services.AlertJPAService;

import com.icesoft.faces.component.inputfile.FileInfo;
import com.icesoft.faces.context.ByteArrayResource;
import com.icesoft.faces.context.Resource;
/**
 * Data transfer object for managing the uploaded files
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class FileTO implements Serializable {
	/** Class serial version UID */
	private static final long serialVersionUID = -978942690744097170L;
	/** File info */
	private FileInfo fileInfo;
	/** Actual file */
	private File file;
	/** Size in megabytes */
	public static final long MEGABYTE_LENGTH_BYTES = 1048000l;
	/** Size in kilobytes */
	public static final long KILOBYTE_LENGTH_BYTES = 1024l;
	/** Attachment id */
	private long id;
	/** Archived alert */
	private boolean archived;
	/** Service to access the Alert DB */
	private AlertJPAService alertJPAService;

	/**
	 * Returns the size of the file formatted in KB or MB
	 * @return String with the formatted size
	 */
	public String getFormattedSize () {
		String formattedSize = null;
		if (file != null) {
			if (file.length () >= MEGABYTE_LENGTH_BYTES) {
				formattedSize = file.length () / MEGABYTE_LENGTH_BYTES + " MB";
			} else if (file.length () >= KILOBYTE_LENGTH_BYTES) {
				formattedSize = file.length () / KILOBYTE_LENGTH_BYTES + " KB";
			} else {
				formattedSize = file.length () + " B";
			}
		} else {
			if (fileInfo.getSize () >= MEGABYTE_LENGTH_BYTES) {
				formattedSize = fileInfo.getSize () / MEGABYTE_LENGTH_BYTES + " MB";
			} else if (fileInfo.getSize () >= KILOBYTE_LENGTH_BYTES) {
				formattedSize = fileInfo.getSize () / KILOBYTE_LENGTH_BYTES + " KB";
			} else {
				formattedSize = fileInfo.getSize () + " B";
			}
		}
		return formattedSize;
	}

	/**
	 * @return the fileInfo
	 */
	public FileInfo getFileInfo () {
		return fileInfo;
	}

	/**
	 * @param fileInfo the fileInfo to set
	 */
	public void setFileInfo (FileInfo fileInfo) {
		this.fileInfo = fileInfo;
	}

	/**
	 * @return the file
	 */
	public File getFile () {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile (File file) {
		this.file = file;
	}

	/**
	 * @return the id
	 */
	public long getId () {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId (long id) {
		this.id = id;
	}

	/**
	 * Returns the file as a resource
	 * @return Resource
	 */
	public Resource getFileContent () {
		Resource resource = null;
		resource = new ByteArrayResource (alertJPAService.readAttachment (id,
				Long.parseLong (FacesContext.getCurrentInstance ().getExternalContext (
						).getUserPrincipal ().getName ()), archived).getAttachment ());
		return resource;
	}

	/**
	 * @param alertJPAService the alertJPAService to set
	 */
	public void setAlertJPAService (AlertJPAService alertJPAService) {
		this.alertJPAService = alertJPAService;
	}

	/**
	 * @return the archived
	 */
	public boolean isArchived () {
		return archived;
	}

	/**
	 * @param archived the archived to set
	 */
	public void setArchived (boolean archived) {
		this.archived = archived;
	}

}
