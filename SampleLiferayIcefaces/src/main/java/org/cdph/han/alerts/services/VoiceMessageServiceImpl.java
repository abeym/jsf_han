package org.cdph.han.alerts.services;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.cdph.han.persistence.UserData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mir3.ws.AuthorizationType;
import com.mir3.ws.ErrorType;
import com.mir3.ws.Mir3;
import com.mir3.ws.Mir3Service;
import com.mir3.ws.ResponseType;
import com.mir3.ws.SearchVoiceFilesResponseType;
import com.mir3.ws.SearchVoiceFilesType;
import com.mir3.ws.VoiceFileOneSearchType;
import com.mir3.ws.VoiceFileType;

/**
 * Implementation that retrieves all pre recorded file names from mir3 site
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
@Service ("voiceMessageService")
public class VoiceMessageServiceImpl implements VoiceMessageService {
	/** Class log */
	private static Log log = LogFactory.getLog (VoiceMessageServiceImpl.class);
	/** User data service */
	//abey ...
	//@Autowired
	private UserDataService userDataService;
	/** MIR3 API version used */
	private static final String API_VERSION = "2.14";

	/* (non-Javadoc)
	 * @see org.cdph.han.alerts.services.VoiceMessageService#recordedFiles()
	 */
	public String[] recordedFiles () {
		final Mir3Service mir3Service = new Mir3Service ();
		final Mir3 service = mir3Service.getMir3 ();
		final ResponseType response =service.searchVoiceFilesOp (createSearch ());
		final List<String> titles = new ArrayList<String> ();
		if (response.getError () == null || response.getError ().isEmpty ()) {
			SearchVoiceFilesResponseType filesResponse = response.getSearchVoiceFilesResponse ();
			for (VoiceFileType file : filesResponse.getVoiceFile ()) {
				titles.add (file.getTitle ());
			}
		} else {
			for (ErrorType error : response.getError ()) {
				log.error ("Error found: " + error.getErrorMessage () + " Error Code: " + error.getErrorCode ());
			}
		}
		return titles.toArray (new String[titles.size ()]);
	}

	/**
	 * Creates a SearchVoiceFilesType object to retrieve all the prerecorded messages
	 * @return SearchVoiceFilesType populated
	 */
	private SearchVoiceFilesType createSearch () {
		final SearchVoiceFilesType search = new SearchVoiceFilesType ();
		search.setApiVersion (API_VERSION);
		search.setAuthorization (createAuthorization ());
		search.setDivision ("/");
		search.setIncludeDetail (true);
		search.setMaxResults (100);
		search.setQuery (createQuery ());
		search.setStartIndex (1);
		return search;
	}

	/**
	 * Creates the query object
	 * @return VoiceFileOneSearchType object
	 */
	private VoiceFileOneSearchType createQuery () {
		final VoiceFileOneSearchType query = new VoiceFileOneSearchType ();
		query.setLocale ("en_US");
		return query;
	}

	/**
	 * Retrieves de current user information to create the AuthorizationType object
	 * @return AuthorizationType object populated
	 */
	private AuthorizationType createAuthorization () {
		final AuthorizationType authorization = new AuthorizationType ();
		final UserData userData = userDataService.getCurrentUserData ();
		authorization.setPassword (userData.getTelephonyPassword ());
		authorization.setUsername (userData.getEmail ());
		return authorization;
	}

}
