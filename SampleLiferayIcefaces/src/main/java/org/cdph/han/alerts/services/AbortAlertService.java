package org.cdph.han.alerts.services;

import org.cdph.han.alerts.exceptions.AlertTerminatedException;

/**
 * Service for aborting notifications in progress
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AbortAlertService {

	/**
	 * The implementation of this method must call mir3 API services to try to abort the notification
	 * @param alertId Id of the alert to wich the notification belongs
	 * @return if the notification was aborted
	 * @throws AlertTerminatedException in case the alert is already terminated
	 */
	boolean abortNotification (final long alertId) throws AlertTerminatedException;

}
