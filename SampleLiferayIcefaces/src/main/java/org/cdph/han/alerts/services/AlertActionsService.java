package org.cdph.han.alerts.services;

import java.util.List;

import org.cdph.han.alerts.dto.AlertSearchTO;

/**
 * Contract for the service to perform actions on alerts that require answers
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public interface AlertActionsService {

	/**
	 * The implementation of this method should return all the alerts that may require action from the user with out
	 * using mir3 webservices
	 * @return List of AlertSearchTO objects that may require action
	 */
	List<AlertSearchTO> alertsPossiblyRequiringAction ();

	/**
	 * Returns a list of alerts that require action
	 * @return List of AlertSearchTO objects requiring action
	 */
	List<AlertSearchTO> alertsRequiringAction (final long userId);

	/**
	 * This method checks if the alert is not finished and may require further action by the user, to improve the
	 * performance this method only checks locally
	 * @param alertId long with the id of the alert
	 * @return notificationReportId and recipientReportId if the alert requires further action
	 */
	boolean checkForRequiredAction (final long alertId);

	/**
	 * Checks if the alert requires further action by the user, if so, returns the user recipient report id, this
	 * method checks against mir3 repository
	 * @param alertId long with the id of the alert
	 * @return notificationReportId and recipientReportId if the alert requires further action
	 */
	Integer[] checkForRequiredAction (final long alertId, final long userId);

	/**
	 * Respond a notification with the selected response option
	 * @param notificationReportId int with the id of the notification report
	 * @param recipientReportId int with the id of the recipient report
	 * @param option int with the selected response option
	 */
	void respondAction (final int notificationReportId, final int recipientReportId, final int option);

}
