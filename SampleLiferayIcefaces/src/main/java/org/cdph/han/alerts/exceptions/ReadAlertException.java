package org.cdph.han.alerts.exceptions;

/**
 * Exception rised when encountering erros while sending the notification to mir3
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class ReadAlertException extends Exception {
	/** Class serial version UID */
	private static final long serialVersionUID = -3796561057651987975L;

	public ReadAlertException () {
	}

	public ReadAlertException (final String arg0) {
		super (arg0);
	}

	public ReadAlertException (final Throwable arg0) {
		super (arg0);
	}

	public ReadAlertException (final String arg0, final Throwable arg1) {
		super (arg0, arg1);
	}

}
