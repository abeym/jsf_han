package org.cdph.han.alerts.dto;

import java.util.Date;

/**
 * Data transfer object representing an scheduled alert in the HAN System
 * @author Horacio Oswaldo Ferro
 * @version 1.0
 */
public class AlertScheduledTO extends AlertPublishedTO {
	/** Class serial version UID */
	private static final long serialVersionUID = 8674811346079194120L;
	/** Name of the Alert author */
	private String author;
	/** Date scheduled to publish the alert */
	private Date scheduledDate;
	/** Date created */
	private Date creationDate;

	/**
	 * @return the author
	 */
	public String getAuthor () {
		return author;
	}

	/**
	 * @param author the author to set
	 */
	public void setAuthor (String author) {
		this.author = author;
	}

	/**
	 * @return the scheduledDate
	 */
	public Date getScheduledDate () {
		return scheduledDate;
	}

	/**
	 * @param scheduledDate the scheduledDate to set
	 */
	public void setScheduledDate (Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}

	/**
	 * @return the creationDate
	 */
	public Date getCreationDate () {
		return creationDate;
	}

	/**
	 * @param creationDate the creationDate to set
	 */
	public void setCreationDate (Date creationDate) {
		this.creationDate = creationDate;
	}

}
