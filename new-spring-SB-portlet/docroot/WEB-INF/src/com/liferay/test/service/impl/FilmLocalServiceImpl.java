/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.test.service.impl;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.test.model.Film;
import com.liferay.test.service.base.FilmLocalServiceBaseImpl;

import java.util.List;

/**
 * The implementation of the film local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link com.liferay.test.service.FilmLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author shilpa
 * @see com.liferay.test.service.base.FilmLocalServiceBaseImpl
 * @see com.liferay.test.service.FilmLocalServiceUtil
 */
public class FilmLocalServiceImpl extends FilmLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link com.liferay.test.service.FilmLocalServiceUtil} to access the film local service.
	 */
	
	public Film create(long userId, long groupId, String director, String cast)
			throws SystemException {

		long filmId = counterLocalService.increment();

		Film film = filmPersistence.create(filmId);

		film.setUserId(userId);
		film.setGroupId(groupId);
		film.setDirector(director);
		film.setCast(cast);
		// film.setYear(year);

		// film.setRating(rating);

		// film.setLanguage(language);
		// film.setComments(comments);
		// film.setViews(views);

		filmPersistence.update(film, false);

		return film;
	}

	public Film create(Film film) throws SystemException {

		long filmId = counterLocalService.increment();

		// Film film = filmPersistence.create(filmId);
		film.setFilmId(filmId);

		filmPersistence.update(film, false);
		return film;
	}

	public List<Film> getFilmData(long userId) throws Exception {
		return filmPersistence.findByUserId(userId);
	}

	public List<Film> getCompanyFilm(long groupId) throws Exception {
		return filmPersistence.findByGroupId(groupId);
	}

	public int getCompanyFilmCount(long groupId) throws Exception {
		return filmPersistence.countByGroupId(groupId);
	}

	public void deleteFilmProfile(long filmId) throws Exception {

		filmLocalService.deleteFilm(filmId);
	}
	
	
}