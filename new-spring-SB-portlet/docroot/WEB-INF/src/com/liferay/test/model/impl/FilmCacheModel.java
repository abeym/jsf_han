/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.test.model.impl;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import com.liferay.test.model.Film;

import java.io.Serializable;

/**
 * The cache model class for representing Film in entity cache.
 *
 * @author shilpa
 * @see Film
 * @generated
 */
public class FilmCacheModel implements CacheModel<Film>, Serializable {
	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{filmId=");
		sb.append(filmId);
		sb.append(", filmTitle=");
		sb.append(filmTitle);
		sb.append(", director=");
		sb.append(director);
		sb.append(", cast=");
		sb.append(cast);
		sb.append(", userId=");
		sb.append(userId);
		sb.append(", groupId=");
		sb.append(groupId);
		sb.append("}");

		return sb.toString();
	}

	public Film toEntityModel() {
		FilmImpl filmImpl = new FilmImpl();

		filmImpl.setFilmId(filmId);

		if (filmTitle == null) {
			filmImpl.setFilmTitle(StringPool.BLANK);
		}
		else {
			filmImpl.setFilmTitle(filmTitle);
		}

		if (director == null) {
			filmImpl.setDirector(StringPool.BLANK);
		}
		else {
			filmImpl.setDirector(director);
		}

		if (cast == null) {
			filmImpl.setCast(StringPool.BLANK);
		}
		else {
			filmImpl.setCast(cast);
		}

		filmImpl.setUserId(userId);
		filmImpl.setGroupId(groupId);

		filmImpl.resetOriginalValues();

		return filmImpl;
	}

	public long filmId;
	public String filmTitle;
	public String director;
	public String cast;
	public long userId;
	public long groupId;
}