package com.liferay;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderResponse;

import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.portlet.bind.annotation.ActionMapping;
import org.springframework.web.portlet.bind.annotation.RenderMapping;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.test.model.Film;
import com.liferay.test.model.impl.FilmImpl;
import com.liferay.test.service.FilmLocalServiceUtil;
import com.liferay.FilmProfileController;

@Controller("filmController")
@RequestMapping(value = "VIEW")

public class FilmProfileController {
	
	private static Log log = LogFactoryUtil.getLog(FilmProfileController.class);
	
	@RenderMapping
	public String showFilms(RenderResponse response) {
		log.info("inside show films......");
		System.out.println("inside show films...... syso");
		return "home";
	}

	@ModelAttribute
	public Film newRequest(@RequestParam(required = false) Long filmId) {
		try {
			Film film = new FilmImpl();
			if (filmId != null) {
				film = FilmLocalServiceUtil.getFilm(filmId);
			}
			return film;
		} catch (Exception e) {
			return new FilmImpl();
		}
	}

	@RenderMapping(params = "myaction=addFilmForm")
	public String showAddFilmForm(RenderResponse response) {
		return "addFilmForm";
	}

	@RenderMapping(params = "myaction=editFilmForm")
	public String showEditBookForm() {
		return "editFilmForm";
	}

	@ActionMapping(params = "myaction=deleteFilmForm")
	public void deleteFilmForm(@ModelAttribute Film film,
			ActionResponse response) {
		log.info("Inside delete film ...............");
		try {
			FilmLocalServiceUtil.deleteFilmProfile(film.getFilmId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();  
		}

	}

	Film filmInstance = null;
	@ActionMapping(params = "myaction=addFilm")
	public void addFilm(@ModelAttribute Film film, BindingResult bindingResult,ActionRequest request,
			ActionResponse response, SessionStatus sessionStatus) {
		log.info("Inside add film ...............");
		try {
			//long id = FilmLocalServiceUtil.increment();
			FilmLocalServiceUtil.create(film);
			/*
			 * int filmIdPort = request.getParameter("filmId"); String String
			 * String int int
			 */
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.setRenderParameter("myaction", "books");
		sessionStatus.setComplete();
	}

}
