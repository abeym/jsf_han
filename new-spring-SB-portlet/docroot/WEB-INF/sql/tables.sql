create table New_Film (
	filmId LONG not null primary key,
	filmTitle VARCHAR(75) null,
	director VARCHAR(75) null,
	cast VARCHAR(75) null,
	userId LONG,
	groupId LONG
);