/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.test.model;

import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Film}.
 * </p>
 *
 * @author    shilpa
 * @see       Film
 * @generated
 */
public class FilmWrapper implements Film, ModelWrapper<Film> {
	public FilmWrapper(Film film) {
		_film = film;
	}

	public Class<?> getModelClass() {
		return Film.class;
	}

	public String getModelClassName() {
		return Film.class.getName();
	}

	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("filmId", getFilmId());
		attributes.put("filmTitle", getFilmTitle());
		attributes.put("director", getDirector());
		attributes.put("cast", getCast());
		attributes.put("userId", getUserId());
		attributes.put("groupId", getGroupId());

		return attributes;
	}

	public void setModelAttributes(Map<String, Object> attributes) {
		Long filmId = (Long)attributes.get("filmId");

		if (filmId != null) {
			setFilmId(filmId);
		}

		String filmTitle = (String)attributes.get("filmTitle");

		if (filmTitle != null) {
			setFilmTitle(filmTitle);
		}

		String director = (String)attributes.get("director");

		if (director != null) {
			setDirector(director);
		}

		String cast = (String)attributes.get("cast");

		if (cast != null) {
			setCast(cast);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}
	}

	/**
	* Returns the primary key of this film.
	*
	* @return the primary key of this film
	*/
	public long getPrimaryKey() {
		return _film.getPrimaryKey();
	}

	/**
	* Sets the primary key of this film.
	*
	* @param primaryKey the primary key of this film
	*/
	public void setPrimaryKey(long primaryKey) {
		_film.setPrimaryKey(primaryKey);
	}

	/**
	* Returns the film ID of this film.
	*
	* @return the film ID of this film
	*/
	public long getFilmId() {
		return _film.getFilmId();
	}

	/**
	* Sets the film ID of this film.
	*
	* @param filmId the film ID of this film
	*/
	public void setFilmId(long filmId) {
		_film.setFilmId(filmId);
	}

	/**
	* Returns the film title of this film.
	*
	* @return the film title of this film
	*/
	public java.lang.String getFilmTitle() {
		return _film.getFilmTitle();
	}

	/**
	* Sets the film title of this film.
	*
	* @param filmTitle the film title of this film
	*/
	public void setFilmTitle(java.lang.String filmTitle) {
		_film.setFilmTitle(filmTitle);
	}

	/**
	* Returns the director of this film.
	*
	* @return the director of this film
	*/
	public java.lang.String getDirector() {
		return _film.getDirector();
	}

	/**
	* Sets the director of this film.
	*
	* @param director the director of this film
	*/
	public void setDirector(java.lang.String director) {
		_film.setDirector(director);
	}

	/**
	* Returns the cast of this film.
	*
	* @return the cast of this film
	*/
	public java.lang.String getCast() {
		return _film.getCast();
	}

	/**
	* Sets the cast of this film.
	*
	* @param cast the cast of this film
	*/
	public void setCast(java.lang.String cast) {
		_film.setCast(cast);
	}

	/**
	* Returns the user ID of this film.
	*
	* @return the user ID of this film
	*/
	public long getUserId() {
		return _film.getUserId();
	}

	/**
	* Sets the user ID of this film.
	*
	* @param userId the user ID of this film
	*/
	public void setUserId(long userId) {
		_film.setUserId(userId);
	}

	/**
	* Returns the user uuid of this film.
	*
	* @return the user uuid of this film
	* @throws SystemException if a system exception occurred
	*/
	public java.lang.String getUserUuid()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _film.getUserUuid();
	}

	/**
	* Sets the user uuid of this film.
	*
	* @param userUuid the user uuid of this film
	*/
	public void setUserUuid(java.lang.String userUuid) {
		_film.setUserUuid(userUuid);
	}

	/**
	* Returns the group ID of this film.
	*
	* @return the group ID of this film
	*/
	public long getGroupId() {
		return _film.getGroupId();
	}

	/**
	* Sets the group ID of this film.
	*
	* @param groupId the group ID of this film
	*/
	public void setGroupId(long groupId) {
		_film.setGroupId(groupId);
	}

	public boolean isNew() {
		return _film.isNew();
	}

	public void setNew(boolean n) {
		_film.setNew(n);
	}

	public boolean isCachedModel() {
		return _film.isCachedModel();
	}

	public void setCachedModel(boolean cachedModel) {
		_film.setCachedModel(cachedModel);
	}

	public boolean isEscapedModel() {
		return _film.isEscapedModel();
	}

	public java.io.Serializable getPrimaryKeyObj() {
		return _film.getPrimaryKeyObj();
	}

	public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
		_film.setPrimaryKeyObj(primaryKeyObj);
	}

	public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
		return _film.getExpandoBridge();
	}

	public void setExpandoBridgeAttributes(
		com.liferay.portal.service.ServiceContext serviceContext) {
		_film.setExpandoBridgeAttributes(serviceContext);
	}

	@Override
	public java.lang.Object clone() {
		return new FilmWrapper((Film)_film.clone());
	}

	public int compareTo(com.liferay.test.model.Film film) {
		return _film.compareTo(film);
	}

	@Override
	public int hashCode() {
		return _film.hashCode();
	}

	public com.liferay.portal.model.CacheModel<com.liferay.test.model.Film> toCacheModel() {
		return _film.toCacheModel();
	}

	public com.liferay.test.model.Film toEscapedModel() {
		return new FilmWrapper(_film.toEscapedModel());
	}

	@Override
	public java.lang.String toString() {
		return _film.toString();
	}

	public java.lang.String toXmlString() {
		return _film.toXmlString();
	}

	public void persist()
		throws com.liferay.portal.kernel.exception.SystemException {
		_film.persist();
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedModel}
	 */
	public Film getWrappedFilm() {
		return _film;
	}

	public Film getWrappedModel() {
		return _film;
	}

	public void resetOriginalValues() {
		_film.resetOriginalValues();
	}

	private Film _film;
}