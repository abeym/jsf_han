/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.test.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link com.liferay.test.service.http.FilmServiceSoap}.
 *
 * @author    shilpa
 * @see       com.liferay.test.service.http.FilmServiceSoap
 * @generated
 */
public class FilmSoap implements Serializable {
	public static FilmSoap toSoapModel(Film model) {
		FilmSoap soapModel = new FilmSoap();

		soapModel.setFilmId(model.getFilmId());
		soapModel.setFilmTitle(model.getFilmTitle());
		soapModel.setDirector(model.getDirector());
		soapModel.setCast(model.getCast());
		soapModel.setUserId(model.getUserId());
		soapModel.setGroupId(model.getGroupId());

		return soapModel;
	}

	public static FilmSoap[] toSoapModels(Film[] models) {
		FilmSoap[] soapModels = new FilmSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static FilmSoap[][] toSoapModels(Film[][] models) {
		FilmSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new FilmSoap[models.length][models[0].length];
		}
		else {
			soapModels = new FilmSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static FilmSoap[] toSoapModels(List<Film> models) {
		List<FilmSoap> soapModels = new ArrayList<FilmSoap>(models.size());

		for (Film model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new FilmSoap[soapModels.size()]);
	}

	public FilmSoap() {
	}

	public long getPrimaryKey() {
		return _filmId;
	}

	public void setPrimaryKey(long pk) {
		setFilmId(pk);
	}

	public long getFilmId() {
		return _filmId;
	}

	public void setFilmId(long filmId) {
		_filmId = filmId;
	}

	public String getFilmTitle() {
		return _filmTitle;
	}

	public void setFilmTitle(String filmTitle) {
		_filmTitle = filmTitle;
	}

	public String getDirector() {
		return _director;
	}

	public void setDirector(String director) {
		_director = director;
	}

	public String getCast() {
		return _cast;
	}

	public void setCast(String cast) {
		_cast = cast;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	private long _filmId;
	private String _filmTitle;
	private String _director;
	private String _cast;
	private long _userId;
	private long _groupId;
}