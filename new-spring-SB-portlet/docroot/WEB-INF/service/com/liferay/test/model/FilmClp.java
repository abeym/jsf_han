/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.test.model;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import com.liferay.test.service.FilmLocalServiceUtil;

import java.io.Serializable;

import java.lang.reflect.Proxy;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shilpa
 */
public class FilmClp extends BaseModelImpl<Film> implements Film {
	public FilmClp() {
	}

	public Class<?> getModelClass() {
		return Film.class;
	}

	public String getModelClassName() {
		return Film.class.getName();
	}

	public long getPrimaryKey() {
		return _filmId;
	}

	public void setPrimaryKey(long primaryKey) {
		setFilmId(primaryKey);
	}

	public Serializable getPrimaryKeyObj() {
		return new Long(_filmId);
	}

	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		setPrimaryKey(((Long)primaryKeyObj).longValue());
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("filmId", getFilmId());
		attributes.put("filmTitle", getFilmTitle());
		attributes.put("director", getDirector());
		attributes.put("cast", getCast());
		attributes.put("userId", getUserId());
		attributes.put("groupId", getGroupId());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Long filmId = (Long)attributes.get("filmId");

		if (filmId != null) {
			setFilmId(filmId);
		}

		String filmTitle = (String)attributes.get("filmTitle");

		if (filmTitle != null) {
			setFilmTitle(filmTitle);
		}

		String director = (String)attributes.get("director");

		if (director != null) {
			setDirector(director);
		}

		String cast = (String)attributes.get("cast");

		if (cast != null) {
			setCast(cast);
		}

		Long userId = (Long)attributes.get("userId");

		if (userId != null) {
			setUserId(userId);
		}

		Long groupId = (Long)attributes.get("groupId");

		if (groupId != null) {
			setGroupId(groupId);
		}
	}

	public long getFilmId() {
		return _filmId;
	}

	public void setFilmId(long filmId) {
		_filmId = filmId;
	}

	public String getFilmTitle() {
		return _filmTitle;
	}

	public void setFilmTitle(String filmTitle) {
		_filmTitle = filmTitle;
	}

	public String getDirector() {
		return _director;
	}

	public void setDirector(String director) {
		_director = director;
	}

	public String getCast() {
		return _cast;
	}

	public void setCast(String cast) {
		_cast = cast;
	}

	public long getUserId() {
		return _userId;
	}

	public void setUserId(long userId) {
		_userId = userId;
	}

	public String getUserUuid() throws SystemException {
		return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
	}

	public void setUserUuid(String userUuid) {
		_userUuid = userUuid;
	}

	public long getGroupId() {
		return _groupId;
	}

	public void setGroupId(long groupId) {
		_groupId = groupId;
	}

	public BaseModel<?> getFilmRemoteModel() {
		return _filmRemoteModel;
	}

	public void setFilmRemoteModel(BaseModel<?> filmRemoteModel) {
		_filmRemoteModel = filmRemoteModel;
	}

	public void persist() throws SystemException {
		if (this.isNew()) {
			FilmLocalServiceUtil.addFilm(this);
		}
		else {
			FilmLocalServiceUtil.updateFilm(this);
		}
	}

	@Override
	public Film toEscapedModel() {
		return (Film)Proxy.newProxyInstance(Film.class.getClassLoader(),
			new Class[] { Film.class }, new AutoEscapeBeanHandler(this));
	}

	@Override
	public Object clone() {
		FilmClp clone = new FilmClp();

		clone.setFilmId(getFilmId());
		clone.setFilmTitle(getFilmTitle());
		clone.setDirector(getDirector());
		clone.setCast(getCast());
		clone.setUserId(getUserId());
		clone.setGroupId(getGroupId());

		return clone;
	}

	public int compareTo(Film film) {
		int value = 0;

		value = getDirector().compareTo(film.getDirector());

		if (value != 0) {
			return value;
		}

		return 0;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}

		FilmClp film = null;

		try {
			film = (FilmClp)obj;
		}
		catch (ClassCastException cce) {
			return false;
		}

		long primaryKey = film.getPrimaryKey();

		if (getPrimaryKey() == primaryKey) {
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public int hashCode() {
		return (int)getPrimaryKey();
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(13);

		sb.append("{filmId=");
		sb.append(getFilmId());
		sb.append(", filmTitle=");
		sb.append(getFilmTitle());
		sb.append(", director=");
		sb.append(getDirector());
		sb.append(", cast=");
		sb.append(getCast());
		sb.append(", userId=");
		sb.append(getUserId());
		sb.append(", groupId=");
		sb.append(getGroupId());
		sb.append("}");

		return sb.toString();
	}

	public String toXmlString() {
		StringBundler sb = new StringBundler(22);

		sb.append("<model><model-name>");
		sb.append("com.liferay.test.model.Film");
		sb.append("</model-name>");

		sb.append(
			"<column><column-name>filmId</column-name><column-value><![CDATA[");
		sb.append(getFilmId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>filmTitle</column-name><column-value><![CDATA[");
		sb.append(getFilmTitle());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>director</column-name><column-value><![CDATA[");
		sb.append(getDirector());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>cast</column-name><column-value><![CDATA[");
		sb.append(getCast());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>userId</column-name><column-value><![CDATA[");
		sb.append(getUserId());
		sb.append("]]></column-value></column>");
		sb.append(
			"<column><column-name>groupId</column-name><column-value><![CDATA[");
		sb.append(getGroupId());
		sb.append("]]></column-value></column>");

		sb.append("</model>");

		return sb.toString();
	}

	private long _filmId;
	private String _filmTitle;
	private String _director;
	private String _cast;
	private long _userId;
	private String _userUuid;
	private long _groupId;
	private BaseModel<?> _filmRemoteModel;
}