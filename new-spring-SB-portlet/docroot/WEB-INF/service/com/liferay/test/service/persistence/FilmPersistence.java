/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.test.service.persistence;

import com.liferay.portal.service.persistence.BasePersistence;

import com.liferay.test.model.Film;

/**
 * The persistence interface for the film service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shilpa
 * @see FilmPersistenceImpl
 * @see FilmUtil
 * @generated
 */
public interface FilmPersistence extends BasePersistence<Film> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. Always use {@link FilmUtil} to access the film persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
	 */

	/**
	* Caches the film in the entity cache if it is enabled.
	*
	* @param film the film
	*/
	public void cacheResult(com.liferay.test.model.Film film);

	/**
	* Caches the films in the entity cache if it is enabled.
	*
	* @param films the films
	*/
	public void cacheResult(java.util.List<com.liferay.test.model.Film> films);

	/**
	* Creates a new film with the primary key. Does not add the film to the database.
	*
	* @param filmId the primary key for the new film
	* @return the new film
	*/
	public com.liferay.test.model.Film create(long filmId);

	/**
	* Removes the film with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param filmId the primary key of the film
	* @return the film that was removed
	* @throws com.liferay.test.NoSuchFilmException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film remove(long filmId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException;

	public com.liferay.test.model.Film updateImpl(
		com.liferay.test.model.Film film, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the film with the primary key or throws a {@link com.liferay.test.NoSuchFilmException} if it could not be found.
	*
	* @param filmId the primary key of the film
	* @return the film
	* @throws com.liferay.test.NoSuchFilmException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film findByPrimaryKey(long filmId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException;

	/**
	* Returns the film with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param filmId the primary key of the film
	* @return the film, or <code>null</code> if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film fetchByPrimaryKey(long filmId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns all the films where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the films where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @return the range of matching films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the films where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first film in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching film
	* @throws com.liferay.test.NoSuchFilmException if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException;

	/**
	* Returns the first film in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching film, or <code>null</code> if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last film in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching film
	* @throws com.liferay.test.NoSuchFilmException if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException;

	/**
	* Returns the last film in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching film, or <code>null</code> if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the films before and after the current film in the ordered set where userId = &#63;.
	*
	* @param filmId the primary key of the current film
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next film
	* @throws com.liferay.test.NoSuchFilmException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film[] findByUserId_PrevAndNext(long filmId,
		long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException;

	/**
	* Returns all the films where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the films where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @return the range of matching films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the films where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the first film in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching film
	* @throws com.liferay.test.NoSuchFilmException if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film findByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException;

	/**
	* Returns the first film in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching film, or <code>null</code> if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film fetchByGroupId_First(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the last film in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching film
	* @throws com.liferay.test.NoSuchFilmException if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException;

	/**
	* Returns the last film in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching film, or <code>null</code> if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film fetchByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the films before and after the current film in the ordered set where groupId = &#63;.
	*
	* @param filmId the primary key of the current film
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next film
	* @throws com.liferay.test.NoSuchFilmException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film[] findByGroupId_PrevAndNext(
		long filmId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException;

	/**
	* Returns all the films.
	*
	* @return the films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns a range of all the films.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @return the range of films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findAll(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns an ordered range of all the films.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> findAll(int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the films where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the films where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public void removeByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Removes all the films from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of films where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching films
	* @throws SystemException if a system exception occurred
	*/
	public int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of films where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching films
	* @throws SystemException if a system exception occurred
	*/
	public int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException;

	/**
	* Returns the number of films.
	*
	* @return the number of films
	* @throws SystemException if a system exception occurred
	*/
	public int countAll()
		throws com.liferay.portal.kernel.exception.SystemException;
}