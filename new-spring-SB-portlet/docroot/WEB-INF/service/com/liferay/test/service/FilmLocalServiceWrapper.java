/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.test.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * <p>
 * This class is a wrapper for {@link FilmLocalService}.
 * </p>
 *
 * @author    shilpa
 * @see       FilmLocalService
 * @generated
 */
public class FilmLocalServiceWrapper implements FilmLocalService,
	ServiceWrapper<FilmLocalService> {
	public FilmLocalServiceWrapper(FilmLocalService filmLocalService) {
		_filmLocalService = filmLocalService;
	}

	/**
	* Adds the film to the database. Also notifies the appropriate model listeners.
	*
	* @param film the film
	* @return the film that was added
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film addFilm(com.liferay.test.model.Film film)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.addFilm(film);
	}

	/**
	* Creates a new film with the primary key. Does not add the film to the database.
	*
	* @param filmId the primary key for the new film
	* @return the new film
	*/
	public com.liferay.test.model.Film createFilm(long filmId) {
		return _filmLocalService.createFilm(filmId);
	}

	/**
	* Deletes the film with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param filmId the primary key of the film
	* @return the film that was removed
	* @throws PortalException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film deleteFilm(long filmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.deleteFilm(filmId);
	}

	/**
	* Deletes the film from the database. Also notifies the appropriate model listeners.
	*
	* @param film the film
	* @return the film that was removed
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film deleteFilm(
		com.liferay.test.model.Film film)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.deleteFilm(film);
	}

	public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
		return _filmLocalService.dynamicQuery();
	}

	/**
	* Performs a dynamic query on the database and returns the matching rows.
	*
	* @param dynamicQuery the dynamic query
	* @return the matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.dynamicQuery(dynamicQuery);
	}

	/**
	* Performs a dynamic query on the database and returns a range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @return the range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.dynamicQuery(dynamicQuery, start, end);
	}

	/**
	* Performs a dynamic query on the database and returns an ordered range of the matching rows.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param dynamicQuery the dynamic query
	* @param start the lower bound of the range of model instances
	* @param end the upper bound of the range of model instances (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching rows
	* @throws SystemException if a system exception occurred
	*/
	@SuppressWarnings("rawtypes")
	public java.util.List dynamicQuery(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
		int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.dynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	* Returns the number of rows that match the dynamic query.
	*
	* @param dynamicQuery the dynamic query
	* @return the number of rows that match the dynamic query
	* @throws SystemException if a system exception occurred
	*/
	public long dynamicQueryCount(
		com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.dynamicQueryCount(dynamicQuery);
	}

	public com.liferay.test.model.Film fetchFilm(long filmId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.fetchFilm(filmId);
	}

	/**
	* Returns the film with the primary key.
	*
	* @param filmId the primary key of the film
	* @return the film
	* @throws PortalException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film getFilm(long filmId)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.getFilm(filmId);
	}

	public com.liferay.portal.model.PersistedModel getPersistedModel(
		java.io.Serializable primaryKeyObj)
		throws com.liferay.portal.kernel.exception.PortalException,
			com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.getPersistedModel(primaryKeyObj);
	}

	/**
	* Returns a range of all the films.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @return the range of films
	* @throws SystemException if a system exception occurred
	*/
	public java.util.List<com.liferay.test.model.Film> getFilms(int start,
		int end) throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.getFilms(start, end);
	}

	/**
	* Returns the number of films.
	*
	* @return the number of films
	* @throws SystemException if a system exception occurred
	*/
	public int getFilmsCount()
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.getFilmsCount();
	}

	/**
	* Updates the film in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param film the film
	* @return the film that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film updateFilm(
		com.liferay.test.model.Film film)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.updateFilm(film);
	}

	/**
	* Updates the film in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
	*
	* @param film the film
	* @param merge whether to merge the film with the current session. See {@link com.liferay.portal.service.persistence.BatchSession#update(com.liferay.portal.kernel.dao.orm.Session, com.liferay.portal.model.BaseModel, boolean)} for an explanation.
	* @return the film that was updated
	* @throws SystemException if a system exception occurred
	*/
	public com.liferay.test.model.Film updateFilm(
		com.liferay.test.model.Film film, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.updateFilm(film, merge);
	}

	/**
	* Returns the Spring bean ID for this bean.
	*
	* @return the Spring bean ID for this bean
	*/
	public java.lang.String getBeanIdentifier() {
		return _filmLocalService.getBeanIdentifier();
	}

	/**
	* Sets the Spring bean ID for this bean.
	*
	* @param beanIdentifier the Spring bean ID for this bean
	*/
	public void setBeanIdentifier(java.lang.String beanIdentifier) {
		_filmLocalService.setBeanIdentifier(beanIdentifier);
	}

	public java.lang.Object invokeMethod(java.lang.String name,
		java.lang.String[] parameterTypes, java.lang.Object[] arguments)
		throws java.lang.Throwable {
		return _filmLocalService.invokeMethod(name, parameterTypes, arguments);
	}

	public com.liferay.test.model.Film create(long userId, long groupId,
		java.lang.String director, java.lang.String cast)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.create(userId, groupId, director, cast);
	}

	public com.liferay.test.model.Film create(com.liferay.test.model.Film film)
		throws com.liferay.portal.kernel.exception.SystemException {
		return _filmLocalService.create(film);
	}

	public java.util.List<com.liferay.test.model.Film> getFilmData(long userId)
		throws java.lang.Exception {
		return _filmLocalService.getFilmData(userId);
	}

	public java.util.List<com.liferay.test.model.Film> getCompanyFilm(
		long groupId) throws java.lang.Exception {
		return _filmLocalService.getCompanyFilm(groupId);
	}

	public int getCompanyFilmCount(long groupId) throws java.lang.Exception {
		return _filmLocalService.getCompanyFilmCount(groupId);
	}

	public void deleteFilmProfile(long filmId) throws java.lang.Exception {
		_filmLocalService.deleteFilmProfile(filmId);
	}

	/**
	 * @deprecated Renamed to {@link #getWrappedService}
	 */
	public FilmLocalService getWrappedFilmLocalService() {
		return _filmLocalService;
	}

	/**
	 * @deprecated Renamed to {@link #setWrappedService}
	 */
	public void setWrappedFilmLocalService(FilmLocalService filmLocalService) {
		_filmLocalService = filmLocalService;
	}

	public FilmLocalService getWrappedService() {
		return _filmLocalService;
	}

	public void setWrappedService(FilmLocalService filmLocalService) {
		_filmLocalService = filmLocalService;
	}

	private FilmLocalService _filmLocalService;
}