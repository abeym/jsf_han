/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package com.liferay.test.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import com.liferay.test.model.Film;

import java.util.List;

/**
 * The persistence utility for the film service. This utility wraps {@link FilmPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author shilpa
 * @see FilmPersistence
 * @see FilmPersistenceImpl
 * @generated
 */
public class FilmUtil {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
	 */
	public static void clearCache() {
		getPersistence().clearCache();
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
	 */
	public static void clearCache(Film film) {
		getPersistence().clearCache(film);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
	 */
	public long countWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().countWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
	 */
	public static List<Film> findWithDynamicQuery(DynamicQuery dynamicQuery)
		throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
	 */
	public static List<Film> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end) throws SystemException {
		return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
	 */
	public static List<Film> findWithDynamicQuery(DynamicQuery dynamicQuery,
		int start, int end, OrderByComparator orderByComparator)
		throws SystemException {
		return getPersistence()
				   .findWithDynamicQuery(dynamicQuery, start, end,
			orderByComparator);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean)
	 */
	public static Film update(Film film, boolean merge)
		throws SystemException {
		return getPersistence().update(film, merge);
	}

	/**
	 * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, boolean, ServiceContext)
	 */
	public static Film update(Film film, boolean merge,
		ServiceContext serviceContext) throws SystemException {
		return getPersistence().update(film, merge, serviceContext);
	}

	/**
	* Caches the film in the entity cache if it is enabled.
	*
	* @param film the film
	*/
	public static void cacheResult(com.liferay.test.model.Film film) {
		getPersistence().cacheResult(film);
	}

	/**
	* Caches the films in the entity cache if it is enabled.
	*
	* @param films the films
	*/
	public static void cacheResult(
		java.util.List<com.liferay.test.model.Film> films) {
		getPersistence().cacheResult(films);
	}

	/**
	* Creates a new film with the primary key. Does not add the film to the database.
	*
	* @param filmId the primary key for the new film
	* @return the new film
	*/
	public static com.liferay.test.model.Film create(long filmId) {
		return getPersistence().create(filmId);
	}

	/**
	* Removes the film with the primary key from the database. Also notifies the appropriate model listeners.
	*
	* @param filmId the primary key of the film
	* @return the film that was removed
	* @throws com.liferay.test.NoSuchFilmException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film remove(long filmId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException {
		return getPersistence().remove(filmId);
	}

	public static com.liferay.test.model.Film updateImpl(
		com.liferay.test.model.Film film, boolean merge)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().updateImpl(film, merge);
	}

	/**
	* Returns the film with the primary key or throws a {@link com.liferay.test.NoSuchFilmException} if it could not be found.
	*
	* @param filmId the primary key of the film
	* @return the film
	* @throws com.liferay.test.NoSuchFilmException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film findByPrimaryKey(long filmId)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException {
		return getPersistence().findByPrimaryKey(filmId);
	}

	/**
	* Returns the film with the primary key or returns <code>null</code> if it could not be found.
	*
	* @param filmId the primary key of the film
	* @return the film, or <code>null</code> if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film fetchByPrimaryKey(long filmId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByPrimaryKey(filmId);
	}

	/**
	* Returns all the films where userId = &#63;.
	*
	* @param userId the user ID
	* @return the matching films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findByUserId(
		long userId) throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserId(userId);
	}

	/**
	* Returns a range of all the films where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @return the range of matching films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findByUserId(
		long userId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByUserId(userId, start, end);
	}

	/**
	* Returns an ordered range of all the films where userId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param userId the user ID
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findByUserId(
		long userId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByUserId(userId, start, end, orderByComparator);
	}

	/**
	* Returns the first film in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching film
	* @throws com.liferay.test.NoSuchFilmException if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film findByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException {
		return getPersistence().findByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the first film in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching film, or <code>null</code> if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film fetchByUserId_First(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserId_First(userId, orderByComparator);
	}

	/**
	* Returns the last film in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching film
	* @throws com.liferay.test.NoSuchFilmException if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film findByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException {
		return getPersistence().findByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the last film in the ordered set where userId = &#63;.
	*
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching film, or <code>null</code> if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film fetchByUserId_Last(long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByUserId_Last(userId, orderByComparator);
	}

	/**
	* Returns the films before and after the current film in the ordered set where userId = &#63;.
	*
	* @param filmId the primary key of the current film
	* @param userId the user ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next film
	* @throws com.liferay.test.NoSuchFilmException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film[] findByUserId_PrevAndNext(
		long filmId, long userId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException {
		return getPersistence()
				   .findByUserId_PrevAndNext(filmId, userId, orderByComparator);
	}

	/**
	* Returns all the films where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the matching films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findByGroupId(
		long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByGroupId(groupId);
	}

	/**
	* Returns a range of all the films where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @return the range of matching films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findByGroupId(
		long groupId, int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findByGroupId(groupId, start, end);
	}

	/**
	* Returns an ordered range of all the films where groupId = &#63;.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param groupId the group ID
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of matching films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findByGroupId(
		long groupId, int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence()
				   .findByGroupId(groupId, start, end, orderByComparator);
	}

	/**
	* Returns the first film in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching film
	* @throws com.liferay.test.NoSuchFilmException if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film findByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException {
		return getPersistence().findByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the first film in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the first matching film, or <code>null</code> if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film fetchByGroupId_First(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGroupId_First(groupId, orderByComparator);
	}

	/**
	* Returns the last film in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching film
	* @throws com.liferay.test.NoSuchFilmException if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film findByGroupId_Last(long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException {
		return getPersistence().findByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the last film in the ordered set where groupId = &#63;.
	*
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the last matching film, or <code>null</code> if a matching film could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film fetchByGroupId_Last(
		long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().fetchByGroupId_Last(groupId, orderByComparator);
	}

	/**
	* Returns the films before and after the current film in the ordered set where groupId = &#63;.
	*
	* @param filmId the primary key of the current film
	* @param groupId the group ID
	* @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	* @return the previous, current, and next film
	* @throws com.liferay.test.NoSuchFilmException if a film with the primary key could not be found
	* @throws SystemException if a system exception occurred
	*/
	public static com.liferay.test.model.Film[] findByGroupId_PrevAndNext(
		long filmId, long groupId,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException,
			com.liferay.test.NoSuchFilmException {
		return getPersistence()
				   .findByGroupId_PrevAndNext(filmId, groupId, orderByComparator);
	}

	/**
	* Returns all the films.
	*
	* @return the films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll();
	}

	/**
	* Returns a range of all the films.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @return the range of films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findAll(
		int start, int end)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end);
	}

	/**
	* Returns an ordered range of all the films.
	*
	* <p>
	* Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set.
	* </p>
	*
	* @param start the lower bound of the range of films
	* @param end the upper bound of the range of films (not inclusive)
	* @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	* @return the ordered range of films
	* @throws SystemException if a system exception occurred
	*/
	public static java.util.List<com.liferay.test.model.Film> findAll(
		int start, int end,
		com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().findAll(start, end, orderByComparator);
	}

	/**
	* Removes all the films where userId = &#63; from the database.
	*
	* @param userId the user ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByUserId(userId);
	}

	/**
	* Removes all the films where groupId = &#63; from the database.
	*
	* @param groupId the group ID
	* @throws SystemException if a system exception occurred
	*/
	public static void removeByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeByGroupId(groupId);
	}

	/**
	* Removes all the films from the database.
	*
	* @throws SystemException if a system exception occurred
	*/
	public static void removeAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		getPersistence().removeAll();
	}

	/**
	* Returns the number of films where userId = &#63;.
	*
	* @param userId the user ID
	* @return the number of matching films
	* @throws SystemException if a system exception occurred
	*/
	public static int countByUserId(long userId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByUserId(userId);
	}

	/**
	* Returns the number of films where groupId = &#63;.
	*
	* @param groupId the group ID
	* @return the number of matching films
	* @throws SystemException if a system exception occurred
	*/
	public static int countByGroupId(long groupId)
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countByGroupId(groupId);
	}

	/**
	* Returns the number of films.
	*
	* @return the number of films
	* @throws SystemException if a system exception occurred
	*/
	public static int countAll()
		throws com.liferay.portal.kernel.exception.SystemException {
		return getPersistence().countAll();
	}

	public static FilmPersistence getPersistence() {
		if (_persistence == null) {
			_persistence = (FilmPersistence)PortletBeanLocatorUtil.locate(com.liferay.test.service.ClpSerializer.getServletContextName(),
					FilmPersistence.class.getName());

			ReferenceRegistry.registerReference(FilmUtil.class, "_persistence");
		}

		return _persistence;
	}

	/**
	 * @deprecated
	 */
	public void setPersistence(FilmPersistence persistence) {
	}

	private static FilmPersistence _persistence;
}